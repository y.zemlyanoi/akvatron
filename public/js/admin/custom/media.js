(function(window, document, $) {
    'use strict';

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(".delete-images").on('click', function (e) {
        $( "input[name='images[]']:checked" ).each(function () {
            let id = $(this).val();
            let url = $('body').data('base')+'/api/image/'+id+'/delete';
            $.ajax({
                type:'POST',
                url:url,
                datatype: 'json'
            }).done(function(data) {
                if(data.success) {
                    $("#image-block-"+id).remove();
                    toastr.success(data.message);
                }
            }).fail(function(jqXHR, ajaxOptions, thrownError){
                toastr.error(jqXHR.responseJSON.message);
            });
        });
    });
    //
    $('#images-uploader').on('submit', function (e) {
        e.preventDefault();
        let url = $(this).attr('action');
        let formData = new FormData(this);
        $.ajax({
            type:'POST',
            url:url,
            data:formData,
            processData: false,
            contentType: false,
            cache : false
        }).done(function(data) {
            if(data.success) {
                $(".app-file-files").append(data.htmlBody);
                toastr.success(data.message);
            }
        }).fail(function(jqXHR, ajaxOptions, thrownError){
            toastr.error(jqXHR.responseJSON.message);
        });
    });
})(window, document, jQuery);
