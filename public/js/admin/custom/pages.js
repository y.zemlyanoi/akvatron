(function(window, document, $) {
    'use strict';

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("button.add-image").on('click', function (e){
        e.preventDefault();
        let image_id = $('#image-choose').find(':selected').val();
        let pageId = $( "input[name='id']" ).val();
        let url = $('body').data('base');
        url = url+'/api/image/'+image_id+'/attach';
        $.ajax({
            type:'POST',
            url:url,
            data: {'page_id':pageId},
            datatype: 'json'
        }).done(function(data) {
            if(data.success) {
               $("#images-list").append(data.htmlBody);
               toastr.success(data.message);
            }
        }).fail(function(jqXHR, ajaxOptions, thrownError){
            toastr.error(jqXHR.responseJSON.message);
        });
    });

    $("a.delete-image").on('click', function (e){
        e.preventDefault();
        let url = $(this).attr('href');
        let pageId = $( "input[name='id']" ).val();
        let id = $(this).data('id');
        $.ajax({
            type:'POST',
            url:url,
            data: {'page_id':pageId},
            datatype: 'json'
        }).done(function(data){
            if(data.success) {
                $("#image-"+id).remove();
                toastr.success(data.message);
            }
        });
    });
})(window, document, jQuery);
