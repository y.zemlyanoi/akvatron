(function(window, document, $) {
    'use strict';

    const dateRangePickers = [
        'completed_calendar_payouts',
        'unconfirmed_calendar_payouts',
        'canceled_calendar_payouts',
        'calendar_users',
        'calendar_financial_statistics'
    ];
    dateRangePickers.forEach(function(item) {
        // Always Show Calendar on Ranges
        $("#"+item).daterangepicker({
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            alwaysShowCalendars: true,
            startDate: moment().subtract(1, 'year'),
            endDate: moment(),
        });
    });
})(window, document, jQuery);
