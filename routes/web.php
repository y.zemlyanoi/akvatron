<?php
use App\Http\Controllers\LanguageController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// dashboard Routes
Route::get('/demo','DashboardController@dashboardEcommerce');
Route::get('/demo/dashboard-ecommerce','DashboardController@dashboardEcommerce');
Route::get('/demo/dashboard-analytics','DashboardController@dashboardAnalytics');

//Application Routes
Route::get('/app-email','ApplicationController@emailApplication');
Route::get('/app-chat','ApplicationController@chatApplication');
Route::get('/app-todo','ApplicationController@todoApplication');
Route::get('/app-calendar','ApplicationController@calendarApplication');
Route::get('/app-kanban','ApplicationController@kanbanApplication');
Route::get('/app-invoice-view','ApplicationController@invoiceApplication');
Route::get('/app-invoice-list','ApplicationController@invoiceListApplication');
Route::get('/app-invoice-edit','ApplicationController@invoiceEditApplication');
Route::get('/app-invoice-add','ApplicationController@invoiceAddApplication');
Route::get('/app-file-manager','ApplicationController@fileManagerApplication');

// Content Page Routes
Route::get('/content-grid','ContentController@gridContent');
Route::get('/content-typography','ContentController@typographyContent');
Route::get('/content-text-utilities','ContentController@textUtilitiesContent');
Route::get('/content-syntax-highlighter','ContentController@contentSyntaxHighlighter');
Route::get('/content-helper-classes','ContentController@contentHelperClasses');
Route::get('/colors','ContentController@colorContent');
// icons
Route::get('/icons-livicons','IconsController@liveIcons');
Route::get('/icons-boxicons','IconsController@boxIcons');
// card
Route::get('/card-basic','CardController@basicCard');
Route::get('/card-actions','CardController@actionCard');
Route::get('/widgets','CardController@widgets');
// component route
Route::get('/component-alerts','ComponentController@alertComponenet');
Route::get('/component-buttons-basic','ComponentController@buttonComponenet');
Route::get('/component-breadcrumbs','ComponentController@breadcrumbsComponenet');
Route::get('/component-carousel','ComponentController@carouselComponenet');
Route::get('/component-collapse','ComponentController@collapseComponenet');
Route::get('/component-dropdowns','ComponentController@dropdownComponenet');
Route::get('/component-list-group','ComponentController@listGroupComponenet');
Route::get('/component-modals','ComponentController@modalComponenet');
Route::get('/component-pagination','ComponentController@paginationComponenet');
Route::get('/component-navbar','ComponentController@navbarComponenet');
Route::get('/component-tabs-component','ComponentController@tabsComponenet');
Route::get('/component-pills-component','ComponentController@pillComponenet');
Route::get('/component-tooltips','ComponentController@tooltipsComponenet');
Route::get('/component-popovers','ComponentController@popoversComponenet');
Route::get('/component-badges','ComponentController@badgesComponenet');
Route::get('/component-pill-badges','ComponentController@pillBadgesComponenet');
Route::get('/component-progress','ComponentController@progressComponenet');
Route::get('/component-media-objects','ComponentController@mediaObjectComponenet');
Route::get('/component-spinner','ComponentController@spinnerComponenet');
Route::get('/component-bs-toast','ComponentController@toastsComponenet');
// extra component
Route::get('/ex-component-avatar','ExComponentController@avatarComponent');
Route::get('/ex-component-chips','ExComponentController@chipsComponent');
Route::get('/ex-component-divider','ExComponentController@dividerComponent');
// form elements
Route::get('/form-inputs','FormController@inputForm');
Route::get('/form-input-groups','FormController@inputGroupForm');
Route::get('/form-number-input','FormController@numberInputForm');
Route::get('/form-select','FormController@selectForm');
Route::get('/form-radio','FormController@radioForm');
Route::get('/form-checkbox','FormController@checkboxForm');
Route::get('/form-switch','FormController@switchForm');
Route::get('/form-textarea','FormController@textareaForm');
Route::get('/form-quill-editor','FormController@quillEditorForm');
Route::get('/form-file-uploader','FormController@fileUploaderForm');
Route::get('/form-date-time-picker','FormController@datePickerForm');
Route::get('/form-layout','FormController@formLayout');
Route::get('/form-wizard','FormController@formWizard');
Route::get('/form-validation','FormController@formValidation');
Route::get('/form-repeater','FormController@formRepeater');
// table route
Route::get('/table','TableController@basicTable');
Route::get('/extended','TableController@extendedTable');
Route::get('/datatable','TableController@dataTable');
// page Route
Route::get('/page-user-profile','PageController@userProfilePage');
Route::get('/page-faq','PageController@faqPage');
Route::get('/page-knowledge-base','PageController@knowledgeBasePage');
Route::get('/page-knowledge-base/categories','PageController@knowledgeCatPage');
Route::get('/page-knowledge-base/categories/question','PageController@knowledgeQuestionPage');
Route::get('/page-search','PageController@searchPage');
Route::get('/page-account-settings','PageController@accountSettingPage');
// User Route
Route::get('/page-users-list','UsersController@listUser');
Route::get('/page-users-view','UsersController@viewUser');
Route::get('/page-users-edit','UsersController@editUser');
// Authentication  Route
Route::get('/auth-login','AuthenticationController@loginPage');
Route::get('/auth-register','AuthenticationController@registerPage');
Route::get('/auth-forgot-password','AuthenticationController@forgetPasswordPage');
Route::get('/auth-reset-password','AuthenticationController@resetPasswordPage');
Route::get('/auth-lock-screen','AuthenticationController@authLockPage');
// Miscellaneous
Route::get('/page-coming-soon','MiscellaneousController@comingSoonPage');
Route::get('/error-404','MiscellaneousController@error404Page');
Route::get('/error-500','MiscellaneousController@error500Page');
Route::get('/page-not-authorized','MiscellaneousController@notAuthPage');
Route::get('/page-maintenance','MiscellaneousController@maintenancePage');
// Charts Route
Route::get('/chart-apex','ChartController@apexChart');
Route::get('/chart-chartjs','ChartController@chartJs');
Route::get('/chart-chartist','ChartController@chartist');
Route::get('/maps-google','ChartController@googleMap');
// extension route
Route::get('/ext-component-sweet-alerts','ExtensionsController@sweetAlert');
Route::get('/ext-component-toastr','ExtensionsController@toastr');
Route::get('/ext-component-noui-slider','ExtensionsController@noUiSlider');
Route::get('/ext-component-drag-drop','ExtensionsController@dragComponent');
Route::get('/ext-component-tour','ExtensionsController@tourComponent');
Route::get('/ext-component-swiper','ExtensionsController@swiperComponent');
Route::get('/ext-component-treeview','ExtensionsController@treeviewComponent');
Route::get('/ext-component-block-ui','ExtensionsController@blockUIComponent');
Route::get('/ext-component-media-player','ExtensionsController@mediaComponent');
Route::get('/ext-component-miscellaneous','ExtensionsController@miscellaneous');
Route::get('/ext-component-i18n','ExtensionsController@i18n');
// locale Route
Route::get('lang/{locale}',[LanguageController::class,'swap']);

// acess controller
Route::get('/access-control', 'AccessController@index');
Route::get('/access-control/{roles}', 'AccessController@roles');
Route::get('/ecommerce', 'AccessController@home')->middleware('role:Admin');

//Auth::routes();

/*****************************************************/

Route::get('/', 'SiteController@index')->name('start_page');
Route::post('/login', 'SiteController@login')->name('login');
Route::post('/register', 'SiteController@register')->name('register');
Route::get('/logout', 'SiteController@logout');

Route::group(['prefix' => 'admin'], function () {
    Route::get('/', 'Admin\AdminController@index')->middleware('auth.admin')->name('admin.dashboard');
    Route::get('/pays', 'Admin\AdminController@pays')->middleware('auth.admin');

    //Design
    Route::get('/design', 'Admin\DesignController@index')->middleware('auth.admin')->name('admin.design.index');

    //Media
    Route::get('/media', 'Admin\MediaController@index')->middleware('auth.admin')->name('admin.media.index');
    Route::post('/media', 'Admin\MediaController@create')->middleware('auth.admin')->name('admin.media.create');

    // System
    Route::get('/system/pool', 'Admin\SystemsController@pool')->middleware('auth.admin')->name('admin.systems.pool');
    Route::get('/system/pool/{id}/edit', 'Admin\SystemsController@poolEdit')->middleware('auth.admin')->name('admin.systems.pool.edit');
    Route::post('/system/pool/{id}/save', 'Admin\SystemsController@poolSave')->middleware('auth.admin')->name('admin.systems.pool.save');
    Route::get('/system/main', 'Admin\SystemsController@index')->middleware('auth.admin')->name('admin.systems.index');
    Route::post('/system/main/update', 'Admin\SystemsController@update')->middleware('auth.admin')->name('admin.systems.update');
    Route::get('/system/permission', 'Admin\PermissionController@index')->middleware('auth.admin')->name('admin.systems.permission');
    Route::get('/system/permission/create', 'Admin\PermissionController@create')->middleware('auth.admin')->name('admin.systems.permission.create');
    Route::get('/system/seo', 'Admin\SystemsController@seo')->middleware('auth.admin')->name('admin.systems.seo');

    // Referral
    Route::post('/referrals/update', 'Admin\ReferralsController@update')->middleware('auth.admin')->name('admin.referrals.update');

    // Payouts
    Route::get('/payouts', 'Admin\PayoutsController@index')->middleware('auth.admin')->name('admin.list.payouts');

    // Users
    Route::get('/users', 'Admin\UsersManagementController@users')->middleware('auth.admin')->name('admin.list.user');
    Route::get('/users/{id}/show', 'Admin\UsersManagementController@show')->middleware('auth.admin')->name('admin.show.user');
    Route::get('/users/{id}/edit', 'Admin\UsersManagementController@edit')->middleware('auth.admin')->name('admin.edit.user');

    // Pages
    Route::get('/pages', 'Admin\PagesController@index')->middleware('auth.admin')->name('admin.list.page');
    Route::get('/pages/create', 'Admin\PagesController@create')->middleware('auth.admin')->name('admin.create.page');
    Route::get('/pages/{id}/edit', 'Admin\PagesController@edit')->middleware('auth.admin')->name('admin.edit.page');
    Route::get('/pages/{id}/delete', 'Admin\PagesController@delete')->middleware('auth.admin')->name('admin.delete.page');
    Route::put('/pages/{id}', 'Admin\PagesController@update')->middleware('auth.admin')->name('admin.update.page');
    Route::post('/pages/save', 'Admin\PagesController@store')->middleware('auth.admin')->name('admin.save.page');

    Route::get('/login', 'Admin\AuthController@login')->name('admin.login');
    Route::get('/logout', 'Admin\AuthController@logout')->name('admin.logout');
    Route::post('/login-in', 'Admin\AuthController@loginIn')->name('admin.post.login');
});

//Cabinet routes
Route::group(['prefix' => 'cabinet'], function () {
    Route::get('/', 'CabinetController@index')->name('cabinet');
    Route::get('/orders', 'CabinetController@orders');
    Route::get('/payments', 'CabinetController@payments');
    Route::get('/referral', 'CabinetController@referral');
    Route::get('/reminder-settings', 'CabinetController@reminderSettings');
    Route::get('/account-settings', 'CabinetController@accountSettings');
    Route::get('/setting-receiving-addresses', 'CabinetController@settingsReceivingAddress');
    Route::get('/purchase-pool', 'CabinetController@purchasePool');

    Route::post('/save-data-user', 'CabinetController@saveDataUser');
    Route::post('/save-data-wallet', 'CabinetController@saveDataWallet');
    Route::post('/change-remind-item', 'CabinetController@changeReminderItem');
    Route::get('/get-order-data/{id}', 'CabinetController@getDataOrder');
    Route::get('/get-operation-data/{id}', 'CabinetController@getDataOperation');
});
