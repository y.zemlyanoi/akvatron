<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group([
  'prefix' => 'auth'
], function () {
  Route::post('login', 'AuthController@login');
  Route::post('register', 'AuthController@register');

  Route::group([
    'middleware' => 'auth:api'
  ], function() {
      Route::get('logout', 'AuthController@logout');
      Route::get('user', 'AuthController@user');
  });
});

Route::group(['middleware' => 'auth:api'], function() {
    //Payouts
    Route::post('payouts', 'Api\PayoutsController@all');
    Route::post('payouts/count', 'Api\PayoutsController@count');
    Route::post('payouts/{slug}', 'Api\PayoutsController@index')->name('api.list.payouts');
    Route::post('payouts/action/{slug}', 'Api\PayoutsController@action')->name('api.action.payouts');

    //Users
    Route::post('users', 'Api\UsersController@index');

    //Images
    Route::post('image/upload', 'Api\ImagesController@store')->name('api.upload.image');
    Route::post('image/{id}/delete', 'Api\ImagesController@destroy')->name('api.delete.image');
    Route::post('image/{id}/detach', 'Api\ImagesController@detach')->name('api.detach.image');
    Route::post('image/{id}/attach', 'Api\ImagesController@attach')->name('api.attach.image');
});
