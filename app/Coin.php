<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coin extends Model
{
    /**
     * @var array
     */
    protected $guarded = [];
    /**
     * @var string
     */
    protected $table = 'coins';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function coinInfo()
    {
        return $this->hasOne('App\CoinsInfo');
    }

    public function wallets()
    {
        return $this->hasMany('App\Wallet');
    }
}
