<?php

namespace App\Listeners;

use App\Coin;
use App\Event\GetThirdServicesData;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

class SetCoinsPrice
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  GetThirdServicesData  $event
     * @return void
     */
    public function handle(GetThirdServicesData $event)
    {
        try {
            $coinTable = file_get_contents('https://rates.blockin.com/v1/rates');
            $jsonPrices = json_decode($coinTable, true);
            foreach ($jsonPrices["price"] as $key => $value) {
                if (in_array($key, $event->coins)) {
                    $coin = Coin::where('name', $key)->firstOrFail();
                    $coin->coinInfo->update(['price' => $jsonPrices["price"][$key]["price"]]);
                }
            }
        } catch(\Exception $e) {
            if($e->getCode() !== 200){
                Log::info("Cron alert! Cant get list of coin price!", ['message' =>$e->getMessage()]);
            }
        }
    }
}
