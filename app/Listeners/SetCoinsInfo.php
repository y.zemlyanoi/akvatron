<?php

namespace App\Listeners;

use App\Coin;
use App\Event\GetThirdServicesData;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

class SetCoinsInfo
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  GetThirdServicesData  $event
     * @return void
     */
    public function handle(GetThirdServicesData $event)
    {
        try {
            $emTable = file_get_contents('https://v3.antpool.com/auth/v3/index/coinList');
            $jsonEm = json_decode($emTable, true);
            $data = $jsonEm["data"]["items"];
            foreach ($data as $d) {
                $coinName = strtolower($d["coinType"]);
                if (in_array($coinName, $event->coins)) {
                    $earning_mode = array();
                    foreach ($d['miningType'] as $mT)
                        array_push($earning_mode, $mT['payMethod']);

                    $coin = Coin::where('name', $coinName)->firstOrFail();
                    $coin->coinInfo->update([
                        'earning_mode' => implode(", ", $earning_mode),
                        'minimum_payment' => $d["minimumPayment"] . ' ' . $d["coinType"]
                    ]);
                }
            }
        } catch(\Exception $e) {
            if($e->getCode() !== 200){
                Log::info("Cron alert! Cant get list of coin price!", ['message' =>$e->getMessage()]);
            }
        }
    }
}
