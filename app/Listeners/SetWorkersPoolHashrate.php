<?php

namespace App\Listeners;

use App\Coin;
use App\Event\GetThirdServicesData;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

class SetWorkersPoolHashrate
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  GetThirdServicesData  $event
     * @return void
     */
    public function handle(GetThirdServicesData $event)
    {
        try {
            $pullTable = file_get_contents('https://api-prod.poolin.com/api/public/v1/pool/stats/batchmerge?coin_type=btc,zec,ltc,eth,bch');
            $jsonPull = json_decode($pullTable, true);
            foreach ($jsonPull["data"] as $key => $value) {
                $coin = Coin::where('name', $key)->firstOrFail();
                $coin->coinInfo->update([
                    'workers' => $jsonPull["data"][$key]["workers"],
                    'pool_hashrate' => $jsonPull["data"][$key]["shares"]["shares_1h"] . ' ' . $jsonPull["data"][$key]["shares"]["shares_unit"] . 'H/s'
                ]);
            }
        } catch(\Exception $e) {
            if($e->getCode() !== 200){
                Log::info("Cron alert! Cant get list of coin price!", ['message' =>$e->getMessage()]);
            }
        }
    }
}
