<?php

namespace App\Listeners;

use App\Coin;
use App\Event\GetThirdServicesData;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

class SetRevenueIncomeUnit
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  GetThirdServicesData  $event
     * @return void
     */
    public function handle(GetThirdServicesData $event)
    {
        try{
            $revenueTable = file_get_contents('https://www.blockin.com/api/latest/stats');
            $jsonRevenue = json_decode($revenueTable, true);
            $data = $jsonRevenue["data"];
            foreach ($data as $d) {
                if (in_array(strtolower($d['coin']), $event->coins)) {
                    $dataTable["revenue"] =
                    $number = array(
                        "T" => 1000000000000,
                        "G" => 1000000000,
                        "M" => 1000000,
                        "K" => 1000
                    );
                    $coin = Coin::where('name', $d['coin'])->firstOrFail();
                    $coin->coinInfo->update([
                        'revenue' => '$ ' . number_format($d["income_usd"], 2, '.', '') . ' /' . $d["income_unit"],
                        'network' => number_format($d["hashrate"] / $number[$d["income_unit"]], 2, '.', '') . ' ' . $d["income_unit"] . 'H/s'
                    ]);
                }
            }
        } catch(\Exception $e) {
            if($e->getCode() !== 200){
                Log::info("Cron alert! Cant get list of coin price!", ['message' =>$e->getMessage()]);
            }
        }
    }
}
