<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersReferralsInfo extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user() {
        return $this->belongsTo('App\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function referral() {
        return $this->belongsTo('App\Referral');
    }
}
