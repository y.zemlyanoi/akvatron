<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CoinsInfo extends Model
{
    /**
     * @var array
     */
    protected $guarded = [];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function coin()
    {
        return $this->belongsTo('App\Coin');
    }
}
