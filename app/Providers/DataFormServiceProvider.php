<?php

namespace App\Providers;

//use App\Payout;
use Illuminate\Support\ServiceProvider;

class DataFormServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
    public $data = array();
    public $dataJsonPaths = array();
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $path = base_path('resources/views/admin/');
        $temp_files = scandir($path);
        natsort($temp_files);
        $this->scanDir($temp_files, $path);
        foreach($this->dataJsonPaths as $dj){
            $string = file_get_contents($dj);
            $dataJson = json_decode($string, true);
            foreach($dataJson as $k=>$i)
                $this->data[$k] = $i;
        }
        \View::share('formData', $this->data);
    }

    public function scanDir($targets, $path) {
        foreach($targets as $file) {
            if(is_dir($path.$file) && $file != "." && $file != "..") {
                $newfiles = scandir( $path . $file );
                $this->scanDir( $newfiles, $path.$file.'/');
            } else {
                $info = pathinfo($file);
                if(isset($info['extension']) && $info['extension'] == 'json'){
                    array_push($this->dataJsonPaths, $path.$file);
                }
            }
        }
    }
}
