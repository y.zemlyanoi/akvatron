<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class HelperServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        foreach (glob(app_path().'/Helpers/*.php') as $filename){
            require_once($filename);
        }
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $data = config('custom.custom');

        // all available option of materialize template
        $allOptions = [
            'mainLayoutType' => array('vertical-menu','horizontal-menu','vertical-menu-boxicons'),
            'theme' => array('light'=>'light','dark'=>'dark','semi-dark'=>'semi-dark'),
            'isContentSidebar'=> array(false,true),
            'pageHeader' => array(false,true),
            'bodyCustomClass' => '',
            'navbarBgColor' => array('bg-white','bg-primary', 'bg-success','bg-danger','bg-info','bg-warning','bg-dark'),
            'navbarType' => array('fixed'=>'fixed','static'=>'static','hidden'=>'hidden'),
            'isMenuCollapsed' => array(false,true),
            'footerType' => array('fixed'=>'fixed','static'=>'static','hidden'=>'hidden'),
            'templateTitle' => '',
            'isCustomizer' => array(true,false),
            'isCardShadow' => array(true,false),
            'isScrollTop' => array(true,false),
            'defaultLanguage'=>array('en' => 'en','pt' => 'pt','fr' => 'fr','de' => 'de'),
            'direction' => array('ltr' => 'ltr','rtl' => 'rtl'),
        ];
        // navbar body class array
        $navbarBodyClass = [
            'fixed'=>'navbar-sticky',
            'static'=>'navbar-static',
            'hidden'=>'navbar-hidden',
        ];
        $navbarClass  = [
            'fixed'=>'fixed-top',
            'static'=>'navbar-static-top',
            'hidden'=>'d-none',
        ];
        // footer class
        $footerBodyClass = [
            'fixed'=>'fixed-footer',
            'static'=>'footer-static',
            'hidden'=>'footer-hidden',
        ];
        $footerClass = [
            'fixed'=>'footer-sticky',
            'static'=>'footer-static',
            'hidden'=>'d-none',
        ];

        //  above arrary override through dynamic data
        $layoutClasses = [
            'mainLayoutType' => $data['mainLayoutType'],
            'theme' => $data['theme'],
            'isContentSidebar'=> $data['isContentSidebar'],
            'pageHeader' => $data['pageHeader'],
            'bodyCustomClass' => $data['bodyCustomClass'],
            'navbarBgColor' => $data['navbarBgColor'],
            'navbarType' => $navbarBodyClass[$data['navbarType']],
            'navbarClass' => $navbarClass[$data['navbarType']],
            'isMenuCollapsed' => $data['isMenuCollapsed'],
            'footerType' => $footerBodyClass[$data['footerType']],
            'footerClass' => $footerClass[$data['footerType']],
            'templateTitle' => $data['templateTitle'],
            'isCustomizer' => $data['isCustomizer'],
            'isCardShadow' => $data['isCardShadow'],
            'isScrollTop' => $data['isScrollTop'],
            'defaultLanguage' => $data['defaultLanguage'],
            'direction' => $data['direction'],
        ];

        // set default language if session hasn't locale value the set default language
        if(!session()->has('locale')){
            app()->setLocale($layoutClasses['defaultLanguage']);
        }

        \View::share('configData', $layoutClasses);
    }
}
