<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payout extends Model
{
    protected $guarded = [];

    // ---- //
    public function wallet()
    {
        return $this->belongsTo('App\Wallet');
    }
    // ---- //
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    // ---- //
    public function coin()
    {
        return $this->belongsTo('App\Coin');
    }
}
