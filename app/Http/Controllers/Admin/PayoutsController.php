<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Payout;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class PayoutsController extends Controller
{
    public function index(Request $request) {
        $unconfirmedPayouts = Payout::where('confirmed', '=', 0)->paginate(5);
        $completedPayouts = Payout::where('confirmed', '=', 1)->paginate(5);
        $canceledPayouts = Payout::where('confirmed', '=', 2)->paginate(5);
        return view('admin.payouts.index', [
            'unconfirmedPayouts' => $unconfirmedPayouts,
            'completedPayouts' => $completedPayouts,
            'canceledPayouts' => $canceledPayouts,
        ]);
    }

    public function completed(Request $request) {
        $unconfirmedPayouts = Payout::where('confirmed', '=', 1);
    }
}
