<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class UsersManagementController extends Controller
{
    public function users()
    {
        $users = User::where('id','>',0)->paginate(5);

        $tablePullUsers = [];

        foreach ($users as $user) {
            $user->power = 0;
            $user->daily_profit = 0;
            $user->earning = 0;
        }

        return view('admin.users.index', ['users' => $users, 'tablePullUsers' => $tablePullUsers]);
    }

    public function show($id) {
        $user = User::findOrFail($id);
        return view ('admin.users.show', ['user'=>$user]);
    }

    public function edit($id) {
        $user = User::findOrFail($id)->first();
        return view ('admin.users.edit', ['user'=>$user]);
    }
}
