<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionController extends Controller
{
    public function index() {
        $roles = Role::all();
        $permissions = Permission::all();
        $admins = User::role('admin')->get();
        return view('admin.systems.permission.index', [
            'admins' => $admins,
            'roles' => $roles,
            'permissions' => $permissions
        ]);
    }

    public function create() {
        $roles = Role::all();
        return view('admin.systems.permission.create', [

        ]);
    }
}
