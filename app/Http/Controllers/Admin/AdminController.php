<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Operation;
use App\Pull;
use App\Referal_level;
use App\Setting;
use App\User;
use App\Wallet;
use App\Worker;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.index');
    }

    public function users()
    {
        $users = User::all();

        $tablePullUsers = [];

        foreach ($users as $user) {
            $tablePullUsers[$user->id]['btc'] = 0;
            $tablePullUsers[$user->id]['bch'] = 0;
            $tablePullUsers[$user->id]['ltc'] = 0;
            $tablePullUsers[$user->id]['eth'] = 0;
            $tablePullUsers[$user->id]['zec'] = 0;
        }

        $workers = Worker::all();

        foreach ($workers as $worker) {
            $tablePullUsers[$worker->user_id][$worker->coin] += $worker->power / 1000000000000;
        }

        return view('admin.users', ['users' => $users, 'tablePullUsers' => $tablePullUsers]);
    }

    public function pays()
    {
        $countWaitedPays = Operation::where('status', '=', 0)->count();
        $waitedPays = Operation::where('status', '=', 0)->where('operation', '=', 'pay')->join('users', 'operations.user_id', '=', 'users.id')->select('operations.id as id', 'name', 'cost', 'operations.created_at as created_at', 'coin', 'btc', 'bch', 'ltc', 'eth', 'zec')->get();
        $submittedPays = Operation::where('status', '!=', 0)->where('operation', '=', 'pay')->join('users', 'operations.user_id', '=', 'users.id')->select('operations.id as id', 'name', 'cost', 'operations.created_at as created_at', 'coin', 'btc', 'bch', 'ltc', 'eth', 'zec', 'confirmed_at', 'status')->get();

        return view('admin.pays', [
            'countWaitedPays' => $countWaitedPays,
            'waitedPays' => $waitedPays,
            'submittedPays' => $submittedPays
        ]);
    }

    public function system()
    {
        $pulls = Pull::all();
        $dataPulls = [];
        foreach ($pulls as $pull) {
            $dataPulls[$pull->coin]['revenue'] = $pull->revenue;
            $dataPulls[$pull->coin]['pull_hashrate'] = $pull->pull_hashrate;
            $dataPulls[$pull->coin]['network'] = $pull->network;
            $dataPulls[$pull->coin]['minimum_payment'] = $pull->minimum_payment;
            $dataPulls[$pull->coin]['earning_mode'] = $pull->earning_mode;
            $dataPulls[$pull->coin]['auto_mode'] = $pull->auto_mode;
        }

        $settings = Setting::all();
        $dataSetting = [];

        foreach ($settings as $setting) {
            $dataSetting[$setting->name] = $setting->value;
        }

        $wallets = Wallet::all();
        $referralLevels = Referal_level::all();

        $refLevels = [];

        foreach ($referralLevels as $referralLevel) {
            $refLevels[$referralLevel->level]['conversion'] = $referralLevel->conversion;
            $refLevels[$referralLevel->level]['percent'] = $referralLevel->percent;
        }

        //$admins = User::where(['role', '=', 10])->;
        //$admins = User::get(['name', 'email', 'id'])->where(['role', '=', User::ROLE_ADMIN]);
        $admins = User::get(['name', 'email', 'id', 'role', 'page_content', 'page_users', 'page_pays', 'page_settings']);

        return view('admin.system', [
            'dataSetting' => $dataSetting ,
            'wallets' => $wallets,
            'referralLevels' => $refLevels,
            'dataPulls' => $dataPulls,
            'admins' => $admins,
        ]);
    }

    public function editPartTemplate($partTemplate)
    {
        return view('editable.edit_landing', ['partLanding' => $partTemplate]);
    }

    public function getPartTemplate($partTemplate)
    {
        return asset('edit-part-template/landing_parts.' . $partTemplate);
    }

    public function savePartTemplate(Request $request)
    {
        file_put_contents('../resources/views/landing_parts/' . $request->input('template') .'.blade.php', $request->input('content'));
        return 'Часть лэндинга успешно сохранена';
    }
}
