<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    public function login()
    {
        $pageConfigs = ['bodyCustomClass'=> 'bg-full-screen-image'];
        return view('admin.auth-login',['pageConfigs' => $pageConfigs]);
    }

    public function loginIn(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);
        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials)) {
            return redirect()->route('admin.login')
                ->with('error','Email-Address And Password Are Wrong.');
        }
        return redirect()->route('admin.dashboard');
    }

    public function logout() {
        Auth::logout();
        return redirect()->route('admin.login');
    }
}
