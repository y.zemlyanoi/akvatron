<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DesignController extends Controller
{
    public function index()
    {
        $partsLanding = [
            'header' => 'header',
            'section_1' => 'section_1',
            'section_2' => 'section_2',
            'section_3' => 'section_3',
            'section_4' => 'section_4',
            'section_5' => 'section_5',
            'section_6' => 'section_6',
            'footer' => 'footer',
        ];

        return view('admin.design.index', ['partsLanding' => $partsLanding]);
    }
}
