<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Image;
use App\Pool;
use App\Referral;
use App\Setting;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SystemsController extends Controller
{
    public function index() {
        $referrals = Referral::all();
        $siteSettings = array();
        $settings = Setting::all();
        foreach ($settings as $setting) {
            $siteSettings[$setting->name] = $setting->value;
        }

        $images = Image::all();
        $imagesIds = array();
        $imagesOptions = array();
        foreach ($images as $image) {
            if(!in_array($image->id, $imagesIds)){
                array_push($imagesOptions, array('value'=>$image->slug,'name'=>$image->name));
            }
        }

        return view('admin.systems.system', [
            'settings'      => $siteSettings,
            'referrals'     => $referrals,
            'imagesOptions' => $imagesOptions
        ]);
    }

    public function pool() {
        $pools = Pool::all();
        $settings = Setting::all();
        return view('admin.systems.pool.index', [
            'pools' => $pools,
            'settings' => $settings
        ]);
    }

    public function update(Request $request) {
        foreach($request->except('_token') as $name=>$value) {
            $setting = Setting::where('name', $name)->update(['value'=>$value]);
        }
        Session::flash('success', 'Success! Settings is updated!');
        return redirect()->route('admin.systems.index');
    }

    public function seo() {
        return view('admin.systems.seo.index', []);
    }

    public function poolEdit($id) {
        $pool = Pool::find($id);
        return view('admin.systems.pool.edit', [
            'pool' => $pool
        ]);
    }

    public function poolSave(Request $request, $id) {
        $poolData = $request->except('_token');
        if(!empty($poolData['auto_mode']))
            $poolData['auto_mode'] = 1;
        Pool::find($id)->update($poolData);
        Session::flash('success', 'Success! Pool is updated!');
        return redirect()->route('admin.systems.pool');
    }




}
