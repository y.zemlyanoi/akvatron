<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Referral;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ReferralsController extends Controller
{
    public function update(Request $request) {
        $referrals = array();
        foreach($request->except('_token') as $name=>$items){
            foreach($items as $index=>$item) {
                $referrals[$index][$name] = $item;
            }
        }
        foreach ($referrals as $ref)
            Referral::where('id', $ref['id'])->update([
                'name' => $ref['name'],
                'conversion' => $ref['conversion'],
                'percent' => $ref['percent']
            ]);
        Session::flash('success', 'Success! Referrals is updated!');
        return redirect()->route('admin.systems.index');
    }
}
