<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Image;
use App\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class PagesController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function index() {
        $pages = Page::paginate(5);
        return view('admin.pages.index', ['pages'=>$pages]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function edit($id) {
        $page = Page::where('id', $id)->first();
        $images = Image::all();
        $imagesOptions = array();
        $imagesIds = array();
        foreach($page->images as $image) {
            array_push($imagesIds, $image->id);
        }

        foreach ($images as $image) {
            if(!in_array($image->id, $imagesIds)){
                array_push($imagesOptions, array('value'=>$image->id,'name'=>$image->name));
            }
        }
        return view('admin.pages.edit', ['page' => $page, 'imagesOptions'=>$imagesOptions]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function create() {
        return view('admin.pages.create');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    function store(Request $request) {
        $sValidationRules = [
            'name' => 'required|string',
            'slug' => 'required|regex:/^[a-z0-9]+(?:-[a-z0-9]+)*$/',
            'description' => 'required',
        ];

        $validator = Validator::make($request->all(), $sValidationRules);

        if ($validator->fails()) // on validator found any error
        {
            // pass validator object in withErrors method & also withInput it should be null by default
            return redirect()->route('admin.create.page')->withErrors($validator)->withInput();
        }

        $page = Page::create($request->except('_token'));
        Session::flash('success', 'Success! Page is created!');
        return redirect()->route('admin.edit.page', ['page' => $page]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    function update(Request $request, $id) {
        $pageData = array();
        $pageData['name'] = $request->name;
        $pageData['description'] = $request->description;
        if($request->save_url) {
            $pageData['slug'] = $request->slug;
        }
        Page::where('id',$id)->update($pageData);
        Session::flash('success', 'Success! Page is updated!');
        return redirect()->route('admin.edit.page', $id);
    }

    function delete($id) {
        Page::where('id',$id)->delete();
        Session::flash('success', 'Success! Page is deleted!');
        return redirect()->route('admin.list.page');
    }

}
