<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Image;
use App\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;

class ImagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // file validation
        $validator      =       $request->validate([
            'images'  =>      'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        // if validation success
        if($file = $request->file('images')) {
            $name = str_replace('.'.$file->getClientOriginalExtension(), '', $file->getClientOriginalName());
            $fileName  =    $name.'.'.time().'.'.$file->getClientOriginalExtension();
            $destinationPath = public_path('/storage/images');
            if($file->move($destinationPath, $fileName)) {
                $image = Image::create([
                    'name' => $name,
                    'slug' => $fileName,
                ]);
            }
            return response()->json([
                'success' => true,
                'message' => 'Success! Image is added.',
                'htmlBody'=> View::make('admin.media.component.image', [
                    'imageId' => $image->id,
                    'imageAlt' => $image->description,
                    'imageSlug' => $image->slug,
                    'imageTitle' => $image->name,
                ])->render()
            ], 200);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $image = Image::where('id', $id)->first();
            if(isset($image->pages) && $image->pages->count())
                $image->pages()->detach();
            $image_path = public_path().'/storage/images/'.$image->slug;
            unlink($image_path);
            $image->delete();
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => $e->getMessage()], 500);
        }
        return response()->json(['success' => true, 'message' => 'Success! Images was deleted.'], 200);
    }

    public function detach(Request $request, $id)
    {
        if(empty($request->input('page_id')))
            return response()->json(['success' => false, 'message' => 'Request not exist'], 400);
        try {
            $page_id = $request->input('page_id');
            $image = Image::where('id', $id)->first();
            $page = Page::where('id',$page_id)->first();
            $image->pages()->detach($page->id);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => $e->getMessage()], 500);
        }
        return response()->json(['success' => true, 'message' => 'Success! Image was deleted.'], 200);
    }

    public function attach(Request $request, $id)
    {
        if(empty($request->input('page_id')))
            return response()->json(['success' => false, 'message' => 'Request not exist'], 400);
        try {
            $page_id = $request->input('page_id');
            $image = Image::where('id', $id)->first();
            $page = Page::where('id', $page_id)->first();
            $page->images()->attach($image->id);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => $e->getMessage()], 500);
        }
        return response()->json([
                'success' => true,
                'message' => 'Success! Image was deleted.',
                'htmlBody'=> View::make('admin.pages.component.image', [
                    'imageId' => $image->id,
                    'imageAlt' => $image->description,
                    'imageSlug' => $image->slug,
                    'imageTitle' => $image->name,
                ])->render()
            ], 200);
    }
}
