<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Payout;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\View;

class PayoutsController extends Controller
{
    public function index(Request $request, $slug) {
        $query = array();
        if(!empty($request->input('type')) && $request->input('type') != 'all') {
            array_push($query, array('type', '=', $request->input('type')));
        }
        if(!empty($request->input('page')) && $request->input('page') != '1') {
            $currentPage = $request->input('page');
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }
        if(!empty($request->input('start')) && !empty($request->input('end'))) {
            array_push($query, array('created_at', '>=', Carbon::parse($request->input('start'))));
            array_push($query, array('created_at', '<=', Carbon::parse($request->input('end'))));
        }
        switch ($slug) {
            case 'unconfirmed':
                array_push($query, array('confirmed', '=', 0));
                $payouts = Payout::where($query)->paginate(5);
                return response()->json([
                    'success' => true,
                    'htmlBody' => View::make('admin.payouts.component.table.unconfirmed-data', [
                        'payouts'=>$payouts
                    ])->render(),
                    'htmlPagination' =>  View::make('admin.payouts.component.pagination', [
                        'payouts'=>$payouts,
                        'id'=>'unconfirmed'
                    ])->render()], 200);
            case 'completed':
                array_push($query, array('confirmed', '=', 1));
                $payouts = Payout::where($query)->paginate(5);
                return response()->json([
                    'success' => true,
                    'htmlBody' => View::make('admin.payouts.component.table.completed-data', [
                        'payouts'=>$payouts
                    ])->render(),
                    'htmlPagination' =>  View::make('admin.payouts.component.pagination', [
                        'payouts'=>$payouts,
                        'id'=>'completed'
                    ])->render()], 200);
            case 'canceled':
                array_push($query, array('confirmed', '=', 2));
                $payouts = Payout::where($query)->paginate(5);
                return response()->json([
                    'success' => true,
                    'htmlBody' => View::make('admin.payouts.component.table.canceled-data', [
                        'payouts'=>$payouts
                    ])->render(),
                    'htmlPagination' =>  View::make('admin.payouts.component.pagination', [
                        'payouts'=>$payouts,
                        'id'=>'canceled'
                    ])->render()], 200);
            default:
                return response()->json(['success' => false, 'message' => 'Request not exist'], 400);
        }
    }

    public function action(Request $request, $slug) {
        switch ($slug) {
            case 'confirm':
                $payout = Payout::find($request->input('id'))->update([
                    'confirmed' => 1,'confirmed_at'=>Carbon::now()
                ]);
                return response()->json(['success' => true, 'payout'=>$payout], 200);
            case 'cancel':
                $payout = Payout::find($request->input('id'))->update([
                    'confirmed_at' => Carbon::now(),
                    'confirmed' => 2,
                    'reason' => $request->input('reason')
                ]);
                return response()->json(['success' => true,'payout'=>$payout], 200);
            default:
                return response()->json(['success' => false, 'message' => 'Request not exist'], 400);
        }
    }

    public function count(Request $request) {
        $query = array();
        switch ($request->input('type')) {
            case 'unconfirmed':
                array_push($query, array('confirmed', '=', 0));
                break;
            case 'completed':
                array_push($query, array('confirmed', '=', 1));
                break;
            case 'canceled':
                array_push($query, array('confirmed', '=', 2));
                break;
            default:
                return response()->json(['success' => false, 'message' => 'Not correct request data'], 400);
        }
        $payouts = Payout::where($query)->count();
        return response()->json(['success' => true, 'amount' => $payouts], 200);
    }
}
