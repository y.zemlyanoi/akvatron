<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Payout;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\View;

class UsersController extends Controller
{
    public function index(Request $request)
    {
        $query = array();
        if(!empty($request->input('type')) && $request->input('type') != 'all') {
            if($request->input('type') == 'active')
                array_push($query, array('active', '=', 1));
            else
                array_push($query, array('active', '=', 0));
        }
        if(!empty($request->input('page')) && $request->input('page') != '1') {
            $currentPage = $request->input('page');
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }
        if(!empty($request->input('start')) && !empty($request->input('end'))) {
            array_push($query, array('created_at', '>=', Carbon::parse($request->input('start'))));
            array_push($query, array('created_at', '<=', Carbon::parse($request->input('end'))));
        }

        $users = User::where($query)->paginate(5);
        return response()->json([
            'success' => true,
            'htmlBody' => View::make('admin.users.component.users-data', [
                'users'=>$users
            ])->render(),
            'htmlPagination' =>  View::make('admin.users.component.pagination', [
                'users'=>$users,
                'id'=>'users'
            ])->render()], 200);
    }
}
