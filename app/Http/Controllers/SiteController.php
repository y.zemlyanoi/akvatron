<?php

namespace App\Http\Controllers;

use App\{Coin, User, Setting, Pool};
use Illuminate\Http\Request;

class SiteController extends Controller
{
    public function index()
    {
        $dataTable = array();
        $coins = Coin::all();
        foreach ($coins as $coin) {
            $dataTable[$coin->name]['price'] = $coin->coinInfo->workers;
            $dataTable[$coin->name]['workers'] = $coin->coinInfo->workers;
            $dataTable[$coin->name]['revenue'] = $coin->coinInfo->revenue;
            $dataTable[$coin->name]['network'] = $coin->coinInfo->network;
            $dataTable[$coin->name]['pool_hashrate'] = $coin->coinInfo->pool_hashrate;
            $dataTable[$coin->name]['earning_mode'] = $coin->coinInfo->earning_mode;
            $dataTable[$coin->name]['minimum_payment'] = $coin->coinInfo->minimum_payment;
            $dataTable[$coin->name]["img"] = $coin->icon;
        }

        $settings = Setting::all();
        $dataSetting = [];

        foreach ($settings as $setting) {
            $dataSetting[$setting->name] = $setting->value;
        }
        if ($dataSetting['auto_mode'] == '0') {
            $pulls = Pool::all();
            foreach ($pulls as $pull) {
                if ($pull->auto_mode == 0) {
                    $dataTable[$pull->coin]["revenue"] = $pull->revenue;
                    $dataTable[$pull->coin]["network"] = $pull->network;
                    $dataTable[$pull->coin]["pool_hashrate"] = $pull->pull_hashrate;
                    $dataTable[$pull->coin]["minimum_payment"] = $pull->minimum_payment;
                    $dataTable[$pull->coin]["earning_mode"] = $pull->earning_mode;
                }
            }
        }

        return view('landing', ['dataTable' => $dataTable]);
    }

    public function login(Request $request)
    {
        $input = $request->all();

        $this->validator($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if(auth()->attempt(array('email' => $input['email'], 'password' => $input['password'])))
        {
            return redirect('cabinet');
        } else{
            return redirect()->route('start_page')
                ->with('error','Email-Address And Password Are Wrong.');
        }

    }

    public function register(Request $request)
    {
        $input = $request->all();

        $user = new User;
        $user->name = $input['name'];
        $user->email = $input['email'];
        $user->password = bcrypt($input['password']);
        $user->save();

        auth()->loginUsingId($user->id);
        return redirect('cabinet');
    }

    public function logout(Request $request)
    {
        auth()->logout();
        return redirect()->route('start_page');
    }
}
