<?php

namespace App\Console\Commands;

use App\Coin;
use App\Event\GetThirdServicesData;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class ThirdPartyServiceCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'thps:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $coins = Coin::all(['name']);
        Log::info("Cron is working fine!");
        $cryptoCoins = array();//["btc", "bch", "ltc", "eth", "zec"];
        foreach ($coins as $coin)
            array_push($cryptoCoins, $coin->name);
        event(new GetThirdServicesData($cryptoCoins));
    }
}
