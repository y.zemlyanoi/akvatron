<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pool extends Model
{
    protected $guarded = [];

    public function coin()
    {
        return $this->belongsTo('App\Coin');
    }
}
