<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    /**
     * @var array
     */
    protected $guarded = [];
    /**
     * @return string
     */
    public function status() {
        return ($this->active) ? 'Active' : 'Disabled';
    }

    public function images()
    {
        return $this->belongsToMany(Image::class, 'page_image');
    }

    public function main() {
        return ($this->main) ? 'Yes' : 'No';
    }
}
