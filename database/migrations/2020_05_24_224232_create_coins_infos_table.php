<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoinsInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coins_infos', function (Blueprint $table) {
            $table->id();
            $table->integer('coin_id')->unsigned();
            $table->float('price');
            $table->integer('workers');
            $table->string('pool_hashrate');
            $table->string('revenue');
            $table->string('network');
            $table->string('minimum_payment');
            $table->string('earning_mode');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coins_infos');
    }
}
