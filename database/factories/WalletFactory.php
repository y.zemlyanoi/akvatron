<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Wallet;
use Faker\Generator as Faker;

$factory->define(Wallet::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'user_id' => $faker->uuid,
        'coin_id' => $faker->uuid,
        'address' => $faker->regexify('[A-Za-z0-9]{20}'),
        'active' =>  (bool)random_int(0, 1)
    ];
});
