<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Page;
use Faker\Generator as Faker;

$factory->define(Page::class, function (Faker $faker) {
    return [
        'name' => $faker->title(),
        'H1' => $faker->title(),
        'slug' => $faker->slug(),
        'annotation' => $faker->randomHtml(),
        'description' => $faker->randomHtml(),
        'main' => (bool)random_int(0, 1),
        'position' => $faker->numberBetween(0,100),
        'active' => (bool)random_int(0, 1)
    ];
});
