<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Coin;
use App\Payout;
use App\User;
use Faker\Generator as Faker;

$factory->define(Payout::class, function (Faker $faker) {
    $confirmed = (bool)random_int(0, 1);
    return [
        'type' => $faker->randomElement(['payout', 'referral']),
        'amount' => $faker->randomFloat(),
        'confirmed' => $confirmed,
        'reason' => $confirmed ? $faker->sentence() : null,
        'confirmed_at' => $confirmed ? $faker->dateTimeBetween('-30 days', 'now') : null
    ];
});
