<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\UsersReferralsLink;
use Faker\Generator as Faker;

$factory->define(UsersReferralsLink::class, function (Faker $faker) {
    return [
        'user_id'=>$faker->uuid,
        'url'=>$faker->regexify('[a-z0-9]{20}'),
        'active' => (bool)random_int(0, 1)
    ];
});
