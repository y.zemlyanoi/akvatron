<?php

use App\Page;
use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            'header' => [
                'name' => 'Header',
                'H1' => 'HeaderH1',
                'main' => 1
            ],
            'section_1' => [
                'name' => 'Section',
                'H1' => 'Section1H1',
                'main' => 1
            ],
            'section_2' => [
                'name' => 'Section',
                'H1' => 'Section2H1',
                'main' => 1
            ],
            'section_3' => [
                'name' => 'Section',
                'H1' => 'Section3H1',
                'main' => 1
            ],
            'section_4' => [
                'name' => 'Section',
                'H1' => 'Section4H1',
                'main' => 1
            ],
            'section_5' => [
                'name' => 'Section',
                'H1' => 'Section5H1',
                'main' => 1
            ],
            'section_6' => [
                'name' => 'Section',
                'H1' => 'Section6H1',
                'main' => 1
            ],
            'footer' => [
                'name' => 'Footer',
                'H1' => 'FooterH1',
                'main' => 1
            ],
        );
        DB::table('pages')->delete();
        foreach ($data as $slug => $d) {
            factory(Page::class)->create([
                'name' => $d['name'],
                'slug' => $slug,
                'H1' => $d['H1'],
                'main' => $d['main'],
                'active' => 1,
                'position' => 1
            ]);
        }
    }
}
