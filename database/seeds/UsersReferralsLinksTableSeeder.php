<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersReferralsLinksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users_referrals_links')->delete();
        $users = User::all();
        foreach ($users as $user) {
            Factory(App\UsersReferralsLink::class, 5)->create([
                'user_id' => $user->id
            ]);
        }
    }
}
