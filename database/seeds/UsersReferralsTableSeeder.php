<?php

use App\Referral;
use App\User;
use App\UsersReferral;
use App\UsersReferralsLink;
use Illuminate\Database\Seeder;

class UsersReferralsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users_referrals')->delete();
        $users = User::role('user')->get();
        $referralIds = Referral::all(['id'])->pluck('id')->toArray();
        $referralsLinksIds = UsersReferralsLink::all(['id'])->pluck('id')->toArray();
        if(count($users) > 1) {
            foreach ($users as $index => $user) {
                if($index == 0) continue;
                UsersReferral::create([
                    'user_id'=>$user->id,
                    'parent_user_id'=>$users[$index-1]->id,
                    'referral_id'=>$referralIds[array_rand($referralIds)],
                    'user_referral_link_id'=>$referralsLinksIds[array_rand($referralsLinksIds)]
                ]);
            }
        }
    }
}
