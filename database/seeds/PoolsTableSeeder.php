<?php

use App\Coin;
use App\Pool;
use Illuminate\Database\Seeder;

class PoolsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            "bch" => [
                "revenue" => "$ 0.09 /T",
                "pool_hashrate" => "32.723 PH/s",
                "network" => "2381809.02 TH/s",
                "minimum_payment" => "0.01 BCH",
                "earning_mode" => "PPLNS, PPS+"
            ],
            "btc" => [
                "revenue" => "$ 0.09 /T",
                "pool_hashrate" => "15.782 EH/s",
                "network" => "93035027.37 TH/s",
                "minimum_payment" => "0.001 BTC",
                "earning_mode" => "PPLNS, PPS+"
            ],
            "eth" => [
                "revenue" => "$ 0.02 /M",
                "pool_hashrate" => "5.054 TH/s",
                "network" => "147528832.65 MH/s",
                "minimum_payment" => "0.01 ETH",
                "earning_mode" => "PPS",
            ],
            "ltc" => [
                "revenue" => "$ 1.78 /G",
                "pool_hashrate" => "40.52 TH/s",
                "network" => "186383.76 GH/s",
                "minimum_payment" => "0.001 LTC",
                "earning_mode" => "PPS, PPLNS"
            ],
            "zec" => [
                "revenue" => "$ 0.04 /K",
                "pool_hashrate" => "2.113 GH/s",
                "network" => "5547067.84 KH/s",
                "minimum_payment" => "0.001 ZEC",
                "earning_mode" => "PPS, PPLNS"
            ]
        );
        DB::table('pools')->delete();
        $coins = Coin::all();
        foreach ($coins as $coin) {
            $this->data[$coin->name]['coin_id'] = $coin->id;
            Pool::create([
                'coin_id'           => $coin->id,
                'revenue'           => $data[$coin->name]['revenue'],
                'pool_hashrate'     => $data[$coin->name]['pool_hashrate'],
                'network'           => $data[$coin->name]['network'],
                'minimum_payment'   => $data[$coin->name]['minimum_payment'],
                'earning_mode'      => $data[$coin->name]['earning_mode'],
                'auto_mode'         => true,
            ]);
        }
    }
}
