<?php

use App\Coin;
use App\CoinsInfo;
use Illuminate\Database\Seeder;

class CoinsInfoTableSeeder extends Seeder
{
    private $data = array(
        "bch" => [
            "coin_id" => '',
            "price" => 233.8,
            "workers" => 7558,
            "pool_hashrate" => "32.723 PH/s",
            "revenue" => "$ 0.09 /T",
            "network" => "2381809.02 TH/s",
            "minimum_payment" => "0.01 BCH",
            "earning_mode" => "PPLNS, PPS+"
        ],
        "btc" => [
            "coin_id" => '',
            "price" => 9155.32,
            "workers" => 542566,
            "pool_hashrate" => "15.782 EH/s",
            "revenue" => "$ 0.09 /T",
            "network" => "93035027.37 TH/s",
            "minimum_payment" => "0.001 BTC",
            "earning_mode" => "PPLNS, PPS+"
        ],
        "eth" => [
            "coin_id" => '',
            "price" => 206.76,
            "workers" => 15322,
            "pool_hashrate" => "5.054 TH/s",
            "revenue" => "$ 0.02 /M",
            "network" => "147528832.65 MH/s",
            "minimum_payment" => "0.01 ETH",
            "earning_mode" => "PPS",
        ],
        "ltc" => [
            "coin_id" => '',
            "price" => 44,
            "workers" => 74006,
            "pool_hashrate" => "40.52 TH/s",
            "revenue" => "$ 1.78 /G",
            "network" => "186383.76 GH/s",
            "minimum_payment" => "0.001 LTC",
            "earning_mode" => "PPS, PPLNS"
        ],
        "zec" => [
            "coin_id" => '',
            "price" => 46.99,
            "workers" => 26485,
            "pool_hashrate" => "2.113 GH/s",
            "revenue" => "$ 0.04 /K",
            "network" => "5547067.84 KH/s",
            "minimum_payment" => "0.001 ZEC",
            "earning_mode" => "PPS, PPLNS"
        ]
    );
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $coins = Coin::all();
        foreach ($coins as $coin) {
            $this->data[$coin->name]['coin_id'] = $coin->id;
            CoinsInfo::create($this->data[$coin->name]);
        }
    }
}
