<?php

use Illuminate\Database\Seeder;

class CoinsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('coins')->delete();
        $coinIcons["btc"] = 'bitcoin.png';
        $coinIcons["bch"] = 'bitcoin_cash.png';
        $coinIcons["ltc"] = 'litecoin.png';
        $coinIcons["eth"] = 'ethereum.png';
        $coinIcons["zec"] = 'zcash.png';
        $dbData = array();
        foreach (["btc", "bch", "ltc", "eth", "zec"] as $coinName){
            array_push($dbData, array(
                'name' => $coinName,
                'icon' => $coinIcons[$coinName],
                'active' => true
            ));
        }
        DB::table('coins')->insert($dbData);
    }
}
