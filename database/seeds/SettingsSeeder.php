<?php

use App\Setting;
use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    private $items = array(
        "auto_mode" => 1,
        "telegram" => 'test',
        "site_name" => 'Akvatron',
        "logo" => 'logo-white.png',
        "seo_title" => 'Akvatron best site',
        "seo_keywords" => 'crypto, exchange, pool',
        "seo_description" => 'crypto, exchange, pool',
        "gtm" => 'test google tag management',
        "fbpixel" => 'test facebook pixel',
        "meta_data"=>'test meta data'
    );
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->delete();
        foreach ($this->items as $name=>$value) {
            Setting::create([
                'name' => $name,
                'value' => $value
            ]);
        }
    }
}
