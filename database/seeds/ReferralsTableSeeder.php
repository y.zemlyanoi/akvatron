<?php

use App\Referral;
use Illuminate\Database\Seeder;

class ReferralsTableSeeder extends Seeder
{
    private $data = array(
        [
            "name" => 'First Level',
            "conversion" => 0,
            "percent" => 5,
            "active" => 1
        ], [
            "name" => 'Second Level',
            "conversion" => 0,
            "percent" => 10,
            "active" => 1
        ], [
            "name" => 'Third Level',
            "conversion" => 0,
            "percent" => 10,
            "active" => 1
        ]
    );
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('referrals')->delete();
        foreach ($this->data as $data) {
            Referral::create([
                'name'          => $data['name'],
                'conversion'    => $data['conversion'],
                'percent'       => $data['percent'],
                'active'        => $data['active']
            ]);
        }
    }
}
