<?php

use App\Coin;
use App\User;
use App\Wallet;
use Illuminate\Database\Seeder;

class WalletsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('wallets')->delete();

        $users = User::all();
        $coins = Coin::all();

        foreach ($users as $user) {
            foreach ($coins as $coin) {
                factory(Wallet::class)->create([
                    'user_id' => $user->id,
                    'coin_id' => $coin->id
                ]);
            }
        }
    }
}
