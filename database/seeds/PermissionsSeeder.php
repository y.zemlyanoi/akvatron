<?php

use App\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        Permission::create(['name' => 'pages']);
        Permission::create(['name' => 'users']);
        Permission::create(['name' => 'payouts']);
        Permission::create(['name' => 'design']);
        Permission::create(['name' => 'settings']);

        // create roles and assign existing permissions
        $role1 = Role::create(['name' => 'user']);
//        $role1->givePermissionTo('edit');
//        $role1->givePermissionTo('reed');

        $role2 = Role::create(['name' => 'admin']);
        $role2->givePermissionTo(Permission::all());
        $user = User::where('email', 'user@user.com')->first();
        $user->assignRole($role1);

        $test = User::where('email', 'test@test.com')->first();
        $test->assignRole($role1);

        $admin = User::where('email', 'admin@admin.com')->first();
        $admin->assignRole($role2);
    }
}
