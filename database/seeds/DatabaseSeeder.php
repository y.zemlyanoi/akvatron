<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(PermissionsSeeder::class);
        $this->call(PagesTableSeeder::class);
        $this->call(SettingsSeeder::class);
        $this->call(CoinsTableSeeder::class);
        $this->call(CoinsInfoTableSeeder::class);
        $this->call(PoolsTableSeeder::class);
        $this->call(WalletsTableSeeder::class);
        $this->call(PayoutsTableSeeder::class);
        $this->call(ReferralsTableSeeder::class);
        $this->call(UsersReferralsInfosTableSeeder::class);
        $this->call(UsersReferralsLinksTableSeeder ::class);
        $this->call(UsersReferralsTableSeeder ::class);
        $this->call(ImagesTableSeeder ::class);
        $this->call(PagesImagesTableSeeder::class);
    }
}
