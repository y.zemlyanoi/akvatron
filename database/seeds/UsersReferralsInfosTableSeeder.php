<?php

use App\Referral;
use App\User;
use App\UsersReferralsInfo;
use Illuminate\Database\Seeder;

class UsersReferralsInfosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users_referrals_infos')->delete();
        $users = User::role('user')->get();
        $referrals = Referral::all(['id'])->pluck('id')->toArray();
        foreach ($users as $user){
            UsersReferralsInfo::create([
                'user_id' => $user->id,
                'referral_id' => $referrals[array_rand($referrals)],
                'conversion' => 10,
                'used_link_amount' => 10,
            ]);
        }
    }
}
