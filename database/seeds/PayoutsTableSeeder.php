<?php

use App\Payout;
use App\Wallet;
use Illuminate\Database\Seeder;

class PayoutsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $wallets = Wallet::all();
        foreach ($wallets as $wallet) {
            factory(Payout::class, 2)->create([
                'user_id' => $wallet->user->id,
                'coin_id' => $wallet->coin->id,
                'wallet_id' => $wallet->id
            ]);
        }
    }
}
