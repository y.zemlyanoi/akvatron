<?php

use App\Image;
use App\Page;
use Illuminate\Database\Seeder;

class PagesImagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('page_image')->delete();
        $images = Image::all(['id'])->pluck('id')->toArray();
        $pages = Page::all();
        foreach ($pages as $page) {
            $imageKey = array_rand($images);
            $page->images()->attach($images[$imageKey]);
        }
    }
}
