<?php
return [
    'extensions' => [

        'ckeditor' => [

            //Set to false if you want to disable this extension
            'enable' => true,

            // Editor configuration
            'config' => [

            ]
        ]
    ]
//
];
