<section class="section section-intro bg-white pb-0 ov-h" id="ico">
    <div class="container">
        <div class="nk-block nk-block-about">
            <div class="row align-items-center justify-content-center justify-content-lg-between gutter-vr-30px">
                <div class="col-lg-6 col-sm-9 pdb-m">
                    <div class="nk-block-text text-center text-lg-left">
                        <h6 class="title title-xs title-xs-s3 tc-primary">What is our Crypto system</h6>
                        <h2 class="title title-semibold">
                            <span style="color: rgb(247, 218, 100);">We’ve built a platform<br>to buy and sell shares.</span>
                        </h2>
                        <p class="lead-s2">Crypto is a platform for the future of funding that powering dat for the new equity blockchain.</p>
                        <p>While existing solutions offer to solve just one problem at a time, our team is up to build a secure, useful, &amp; easy-to-use product based on private blockchain. It will include easy cryptocurrency payments integration, and even a digital arbitration system.</p>
                        <p>At the end, Our aims to integrate all companies, employees, and business assets into a unified blockchain ecosystem, which will make business truly efficient, transparent, and reliable.</p>
                    </div>
                </div>
                <div class="col-lg-5 col-sm-9">
                    <div class="nk-block-video">
                        <img src="/azure_pro/images/azure/gfx-y-light.png" alt="image" class="fr-fic fr-dii fr-draggable">
                        <a class="nk-block-video-play video-popup btn-play-wrap btn-play-wrap-s2" href="https://www.youtube.com/watch?v=SSo_EIwHSd4">
                            <div class="btn-play btn-play-sm btn-play-s2">
                                <br>
                            </div>
                            <div class="btn-play-text">How it work</div>
                        </a>
                    </div>
                </div>
            </div>
        </div><!-- .block @e        -->
    </div><!-- .conatiner  -->
</section>
