<section class="section bg-white">
    <div class="container">
        <div class="row justify-content-around">
            <div class="col-lg-12">
                <table class="table table-bordered" id="coin-table">
                    <tbody>
                    <tr>
                        <td class="table-head">Coin</td>
                        <td class="table-head">Revenue</td>
                        <td class="table-head">Pool Hashrate</td>
                        <td class="table-head">Network</td>
                        <td class="table-head">Minimum payment</td>
                        <td class="table-head">Earning Mode</td>
                    </tr>
                    @foreach($dataTable as $key => $value)
                        <tr>
                            <td class="table-des">
                                <img src="/images/coins/{{$dataTable[$key]["img"]}}" alt="{{strtoupper($key)}}" width="25">
                                {{strtoupper($key)}}
                            </td>
                            <td class="table-des">{{$dataTable[$key]["revenue"]}}</td>
                            <td class="table-des">{{$dataTable[$key]["pool_hashrate"]}}</td>
                            <td class="table-des">{{$dataTable[$key]["network"]}}</td>
                            <td class="table-des">{{$dataTable[$key]["minimum_payment"]}}</td>
                            <td class="table-des">{{$dataTable[$key]["earning_mode"]}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
