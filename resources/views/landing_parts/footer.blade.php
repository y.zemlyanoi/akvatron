<footer class="nk-footer bg-theme-dark has-ovm">
    <hr class="hr hr-white-5 my-0">
    <section class="section section-m bg-transparent tc-light ov-v">
        <div class="container">
            <!-- Block @s    -->
            <div class="nk-block block-footer">
                <div class="row justify-content-between gutter-vr-30px">
                    <div class="col-lg-4 col-sm-6">
                        <div class="wgs wgs-menu">
                            <ul class="social">
                                <li><a href="#"></a></li>
                                <li><a href="#"></a></li>
                                <li><a href="#"></a></li>
                                <li><a href="#"></a></li>
                                <li><a href="#"></a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- .col -->
                    <div class="col-lg-4 col-sm-6">
                        <div class="wgs wgs-subscribe-form wgs-subscribe-form-s1">
                            <h6 class="wgs-title wgs-title-s2 ttc tc-white">Subscribe to our newsleter</h6>
                            <div class="wgs-body">
                                <form action="form/subscribe.php" class="nk-form-submit" method="post" novalidate="novalidate">
                                    <div class="field-inline field-inline-s4">
                                        <div class="field-wrap">
                                            <input class="input-solid round required email bg-theme-alt" type="text" name="contact-email" placeholder="Enter your email" aria-required="true">
                                            <input type="text" class="d-none" name="form-anti-honeypot" value="">
                                        </div>
                                        <div class="submit-wrap">
                                            <button class="btn btn-round btn-md btn-primary btn-auto">Subscribe</button>
                                        </div>
                                    </div>
                                    <div class="form-results">
                                        <br>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div><!-- .col -->
                    <div class="col-lg-4">
                        <div class="wgs wgs-menu">
                            <div class="wgs-body ml-lg-n3">
                                <ul class="wgs-links wgs-links-3clumn">
                                    <li>
                                        <a href="#">What is system</a>
                                    </li>
                                    <li>
                                        <a href="#">Our Apps</a>
                                    </li>
                                    <li>
                                        <a href="#">Join Us</a>
                                    </li>
                                    <li>
                                        <a href="#">Tokens</a>
                                    </li>
                                    <li>
                                        <a href="#">Whitepaper</a>
                                    </li>
                                    <li>
                                        <a href="#">Contact</a>
                                    </li>
                                    <li>
                                        <a href="#">Roadmap</a>
                                    </li>
                                    <li>
                                        <a href="#">Teams</a>
                                    </li>
                                    <li>
                                        <a href="#">FAQ</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div><!-- .col -->
                </div><!-- .row -->
                <div class="footer-bottom pdt-l">
                    <div class="row justify-content-center">
                        <div class="col-lg-7">
                            <div class="copyright-text copyright-text-s3">
                                <p><span class="d-block">Copyright © 2019, Crypto.&nbsp;</span> All trademarks and copyrights belong to their respective owners.</p>
                            </div>
                        </div>
                        <div class="col-lg-5 text-lg-right">
                            <ul class="footer-links">
                                <li><a href="#">Privacy Policy</a></li>
                                <li><a href="#">Terms &amp; Conditions</a></li>
                                <li class="language-switcher toggle-wrap">
                                    <a class="toggle-tigger" href="#">EN</a>
                                    <ul class="toggle-class toggle-drop toggle-drop-top toggle-drop-center drop-list drop-list-xs text-center">
                                        <li><a href="#">FR</a></li>
                                        <li><a href="#">CH</a></li>
                                        <li><a href="#">HD</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div><!-- .row -->
            </div><!-- .block @e    -->
        </div>
    </section>
    <div class="nk-ovm shape-t h-75"><br></div>
</footer>
