<header class="nk-header page-header is-transparent is-sticky is-shrink is-dark has-fixed" id="header">
    <!-- Header @s-->
    <div class="header-main">
        <div class="header-container container">
            <div class="header-wrap">
                <!-- Logo @s -->
                <div class="header-logo header-logo-ls logo">
                    <a class="logo-link" href="./">
                        <img class="logo-dark fr-fic fr-dii" src="/azure_pro/images/logo.png" srcset="/azure_pro/images/logo2x.png 2x" alt="logo">
                        <img class="logo-light fr-fil fr-dib" src="/azure_pro/images/logo-white.png" srcset="/azure_pro/images/logo-white2x.png 2x" alt="logo">
                    </a>
                    <div class="language-switcher language-switcher-s3 toggle-wrap">
                        <a class="toggle-tigger" href="#">EN</a>
                        <ul class="toggle-class toggle-drop text-center drop-list drop-list-xs">
                            <li><a href="#">Fr</a></li>
                            <li><a href="#">Ch</a></li>
                            <li><a href="#">Hi</a></li>
                        </ul>
                    </div>
                </div><!-- Menu Toogle @s -->
                <div class="header-nav-toggle">
                    <a class="navbar-toggle" data-menu-toggle="example-menu-04" href="#">
                        <div class="toggle-line">
                            <br>
                        </div>
                    </a>
                </div><!-- Menu @s  -->
                <div class="header-navbar header-navbar-s3">
                    <nav class="header-menu justify-content-between" id="example-menu-04">
                        <ul class="menu menu-s2">
                            <li class="menu-item">
                                <a class="menu-link nav-link" href="#ico">What is system</a>
                            </li>
                            <li class="menu-item"><a class="menu-link nav-link" href="#tokens">Tokens</a></li>
                            <li class="menu-item"><a class="menu-link nav-link" href="#faq">Faq</a></li>
                            <li class="menu-item"><a class="menu-link nav-link" href="#contact">Contact</a></li>
                        </ul>
                        <ul class="menu-btns">
                            <li><a class="btn btn-rg btn-auto btn-outline btn-grad on-bg-theme-dark-alt" data-target="#register-popup" data-toggle="modal" href="#"><span>Sign Up</span></a></li>
                            <li><a class="btn btn-rg btn-auto btn-outline btn-grad on-bg-theme-dark-alt" data-target="#login-popup" data-toggle="modal" href="#"><span>Log In</span></a></li>
                        </ul>
                    </nav>
                    <div class="header-navbar-overlay"><br></div>
                    <div class="header-navbar-overlay"><br></div>
                    <div class="header-navbar-overlay"></div>
                </div><!-- .header-navbar @e -->
            </div>
        </div>
    </div><!-- .header-main @e -->
    <!-- Banner @s -->
    <div class="header-banner bg-theme-alt">
        <div class="nk-banner bg-grad-special">
            <div class="banner banner-fs banner-single bg-grad-special-alt tc-light has-ovm">
                <div class="banner-wrap mt-auto">
                    <div class="container">
                        <div class="row align-items-center justify-content-center justify-content-lg-between">
                            <div class="col-lg-5 order-lg-last">
                                <div class="banner-gfx banner-gfx-re-s5 op-30">
                                    <img src="/azure_pro/images/header/gfx-f.png" alt="header" class="fr-fic fr-dii">
                                </div>
                                <div class="abs-center w-100 index-9">
                                    <div class="token-status token-status-s5 no-bd round">
                                        <h4 class="title title-sm">PRE SALE STARTING IN</h4>
                                        <div class="countdown-s3 countdown-s4 countdown" data-date="2020/03/15"><br></div>
                                        <div class="progress-wrap progress-wrap-point progress-wrap-point-s1">
                                            <div class="progress-bar progress-bar-sm progress-bar-s3 round-sm">
                                                <div class="progress-percent progress-percent-s3 bg-accent" data-percent="75" style="width:75%;"><br></div>
                                                <div class="progress-point progress-point-s1" data-point="20" style="left: 20%;">Pre Sale</div>
                                                <div class="progress-point progress-point-s1" data-point="45" style="left: 45%;">Soft Cap</div>
                                                <div class="progress-point progress-point-s1" data-point="75" style="left: 75%;">Bonus</div>
                                            </div>
                                        </div>
                                        <div class="token-action">
                                            <a class="btn btn-secondary" href="#">REGISTER &amp; BUY &nbsp;NOW</a>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- .col -->
                            <div class="col-lg-6 text-center text-lg-left">
                                <div class="banner-caption cpn tc-light">
                                    <div class="cpn-head">
                                        <h1 class="title title-xl-2 title-lg title-semibold">#1 Best Cloud mining system</h1>
                                    </div>
                                    <div class="cpn-text">
                                        <p class="lead-s2">Most Trending and Powerful based on deeply research</p>
                                    </div>
                                    <div class="cpn-action">
                                        <ul class="cpn-links">
                                            <li><a class="btn btn-md btn-grad" href="#">Sign up to join</a></li>
                                            <li><a class="btn btn-md btn-grad" href="#">Token Distribution</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div><!-- .col -->
                        </div><!-- .row -->
                    </div>
                </div>
                <div class="nk-block mt-auto pdb-r">
                    <div class="container">
                        <ul class="partner-list-s3 row align-items-center justify-content-center">
                            <li class="col-12 col-md-2 text-center text-md-left">
                                <h6 class="title title-xs tc-primary mb-md-0 mb-4">Our<br>Partners</h6>
                            </li>
                            <li class="col-4 col-md-2">
                                <a href="#">
                                    <img src="/azure_pro/images/partners/a-xs-light.png" alt="partner" class="fr-fic fr-dii">
                                </a>
                            </li>
                            <li class="col-4 col-md-2">
                                <a href="#">
                                    <img src="/azure_pro/images/partners/b-xs-light.png" alt="partner" class="fr-fic fr-dii">
                                </a>
                            </li>
                            <li class="col-4 col-md-2">
                                <a href="#">
                                    <img src="/azure_pro/images/partners/c-xs-light.png" alt="partner" class="fr-fic fr-dii">
                                </a>
                            </li>
                            <li class="col-4 col-md-2">
                                <a href="#">
                                    <img src="/azure_pro/images/partners/d-xs-light.png" alt="partner" class="fr-fic fr-dii">
                                </a>
                            </li>
                            <li class="col-4 col-md-2">
                                <a href="#">
                                    <img src="/azure_pro/images/partners/e-xs-light.png" alt="partner" class="fr-fic fr-dii">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- .block @e        -->
                <div class="nk-ovm shape-u shape-contain shape-left-top"><br></div>
                <!-- Place Particle Js -->
                <div class="particles-container particles-bg" data-pt-base="#00c0fa" data-pt-base-op=".3" data-pt-line="#2b56f5" data-pt-line-op=".5" data-pt-shape="#00c0fa" data-pt-shape-op=".2" id="particles-bg"><br>
                    <canvas class="particles-js-canvas-el" width="4524" height="1494" style="width: 100%; height: 100%;"></canvas>
                </div>
            </div>
        </div>
        <!-- .nk-banner -->
    </div>
    <!-- .header-banner @e -->
</header>
