<section class="section section-contact bg-theme-dark tc-light">
    <div class="container">
    <!-- Section Head @s -->
        <div class="section-head text-center wide-auto-sm">
            <h4 class="title-xs title-semibold">As Seen In</h4>
        </div><!-- .section-head @e -->
    <!-- Block @s -->
        <div class="nk-block block-partners">
            <ul class="partner-list partner-list-s2 flex-wrap mgb-m30">
                <li class="partner-logo-s3"><img src="/azure_pro/images/partners/a-xs-light.png" alt="partner"></li>
                <li class="partner-logo-s3"><img src="/azure_pro/images/partners/b-xs-light.png" alt="partner"></li>
                <li class="partner-logo-s3"><img src="/azure_pro/images/partners/c-xs-light.png" alt="partner"></li>
                <li class="partner-logo-s3"><img src="/azure_pro/images/partners/d-xs-light.png" alt="partner"></li>
                <li class="partner-logo-s3"><img src="/azure_pro/images/partners/e-xs-light.png" alt="partner"></li>
                <li class="partner-logo-s3"><img src="/azure_pro/images/partners/f-xs-light.png" alt="partner"></li>
                <li class="partner-logo-s3"><img src="/azure_pro/images/partners/a-xs-light.png" alt="partner"></li>
                <li class="partner-logo-s3"><img src="/azure_pro/images/partners/b-xs-light.png" alt="partner"></li>
                <li class="partner-logo-s3"><img src="/azure_pro/images/partners/c-xs-light.png" alt="partner"></li>
                <li class="partner-logo-s3"><img src="/azure_pro/images/partners/d-xs-light.png" alt="partner"></li>
            </ul>
        </div>
    </div>
</section>