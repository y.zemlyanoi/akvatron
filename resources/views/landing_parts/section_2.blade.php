<section class="section bg-white ov-h">
    <div class="container">
        <div class="section-head section-head-s3 wide-auto-sm text-center">
            <h6 class="title title-xs title-xs-s3 tc-primary">Why our Crypto</h6>
            <h2 class="title">Competitive Advantage</h2>
            <p>With help from our teams, contributors and investors these are the milestones we are looking forward to achieve.</p>
        </div>
        <div class="nk-block">
            <div class="row justify-content-center gutter-vr-30px">
            <div class="col-lg-4 col-sm-8">
                <div class="feature text-center">
                    <div class="feature-icon feature-icon-s8">
                        <img src="/azure_pro/images/gfx/gfx-sm-e-light.png" alt="feature" class="fr-fic fr-dii fr-draggable">
                    </div>
                    <div class="feature-text">
                        <h5 class="title title-md">Blockchain Infrastructure</h5>
                        <p>Integrates blockchain technology to provide unique identities for each node.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-8">
                <div class="feature text-center">
                    <div class="feature-icon feature-icon-s8">
                        <img src="/azure_pro/images/gfx/gfx-sm-f-light.png" alt="feature" class="fr-fic fr-dii fr-draggable">
                    </div>
                    <div class="feature-text">
                        <h5 class="title title-md">Easy Token Integration</h5>
                        <p>Every node has its own token. You can earn tokens by doing work.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-8">
                <div class="feature text-center">
                    <div class="feature-icon feature-icon-s8">
                        <img src="/azure_pro/images/gfx/gfx-sm-g-light.png" alt="feature" class="fr-fic fr-dii fr-draggable">
                    </div>
                    <div class="feature-text">
                        <h5 class="title title-md">Global System and Secure</h5>
                        <p>An efficient global system covering all corners, provides best data security.</p>
                    </div>
                </div>
            </div>
        </div>
            <div class="text-center pdt-r">
                <ul class="btn-grp">
                    <li><br></li>
                    <li>
                        <a class="link link-light link-xs" href="#">
                            &nbsp;<em class="link-icon fa fa-paper-plane"></em>
                            <span>Join us on Telegram</span>&nbsp;
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
