<!DOCTYPE html>
<html lang="zxx" class="js">
<head>
    <meta charset="utf-8">
    <meta name="author" content="Softnio">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <!-- Fav Icon  -->
    <link rel="shortcut icon" href="/azure_pro/images/favicon.png">
    <!-- Site Title  -->
    <title>Azure pro WOA | ICO Crypto - ICO Landing Page &amp; Multi-Purpose Cryptocurrency HTML Template</title>
    <!-- Bundle and Base CSS -->
    <link rel="stylesheet" href="/azure_pro/assets/css/vendor.bundle.css?ver=192">
    <link rel="stylesheet" href="/azure_pro/assets/css/style-azure.css?ver=192" id="changeTheme">
    <!-- Extra CSS -->
    <link rel="stylesheet" href="/azure_pro/assets/css/theme.css?ver=192">
</head>


<body class="nk-body body-wider mode-onepage">

<div class="nk-wrap">
    <div id="cc"></div>
    {{view('landing_parts.header')}}

    <main class="nk-pages bg-transparent">
        <!-- Start Section -->
        {{view('landing_parts.table_coin', ['dataTable' => $dataTable])}}
        <!-- // -->
        {{view('landing_parts.section_1')}}
        <!-- // -->
        {{view('landing_parts.section_2')}}
        <!-- // -->
        {{view('landing_parts.section_3')}}
        <!-- // -->
        {{view('landing_parts.section_4')}}
        <!-- // -->
        {{view('landing_parts.section_5')}}
        <!-- // -->
        {{view('landing_parts.section_6')}}
    </main>
    {{view('landing_parts.footer')}}
</div>

<!-- Modal @s --><!-- // -->
<div class="modal fade" id="login-popup">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <a href="#" class="modal-close" data-dismiss="modal" aria-label="Close"><em class="ti ti-close"></em></a>
            <div class="ath-container m-0">
                <div class="ath-body">
                    <h5 class="ath-heading title">Sign in <small class="tc-default">with your ICO Account</small></h5>
                    <form method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <div class="field-item">
                            <div class="field-wrap">
                                <input type="text" class="input-bordered" placeholder="Your Email" name="email">
                            </div>
                        </div>
                        <div class="field-item">
                            <div class="field-wrap">
                                <input type="password" class="input-bordered" placeholder="Password" name="password">
                            </div>
                        </div>
                        <div class="field-item d-flex justify-content-between align-items-center">
                            <div class="field-item pb-0">
                                <input class="input-checkbox" id="remember-me-100" type="checkbox">
                                <label for="remember-me-100">Remember Me</label>
                            </div>
                            <div class="forget-link fz-6">
                                <a href="#" data-toggle="modal" data-dismiss="modal" data-target="#reset-popup">Forgot password?</a>
                            </div>
                        </div>
                        <button class="btn btn-primary btn-block btn-md">Sign In</button>
                    </form>

                    <div class="ath-note text-center">
                        Don’t have an account? <a href="#" data-toggle="modal" data-dismiss="modal" data-target="#register-popup"> <strong>Sign up here</strong></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- .modal @e -->

<!-- Modal @s -->
<div class="modal fade" id="register-popup">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <a href="#" class="modal-close" data-dismiss="modal" aria-label="Close"><em class="ti ti-close"></em></a>
            <div class="ath-container m-0">
                <div class="ath-body">
                    <h5 class="ath-heading title">Sign Up <small class="tc-default">Create New TokenWiz Account</small></h5>
                    <form method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
                        <div class="field-item">
                            <div class="field-wrap">
                                <input type="text" class="input-bordered" placeholder="Your Name" name="name">
                            </div>
                        </div>
                        <div class="field-item">
                            <div class="field-wrap">
                                <input type="text" class="input-bordered" placeholder="Your Email" name="email">
                            </div>
                        </div>
                        <div class="field-item">
                            <div class="field-wrap">
                                <input type="password" class="input-bordered" placeholder="Password" name="password">
                            </div>
                        </div>
                        <div class="field-item">
                            <div class="field-wrap">
                                <input type="password" class="input-bordered" placeholder="Repeat Password">
                            </div>
                        </div>
                        <div class="field-item">
                            <input class="input-checkbox" id="agree-term-2" type="checkbox">
                            <label for="agree-term-2">I agree to Icos <a href="#">Privacy Policy</a> &amp; <a href="#">Terms</a>.</label>
                        </div>
                        <button class="btn btn-primary btn-block btn-md">Sign Up</button>
                    </form>

                    <div class="ath-note text-center">
                        Already have an account? <a href="#" data-toggle="modal" data-dismiss="modal" data-target="#login-popup"> <strong>Sign in here</strong></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- .modal @e -->

<!-- Modal @s -->
<div class="modal fade" id="reset-popup">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <a href="#" class="modal-close" data-dismiss="modal" aria-label="Close"><em class="ti ti-close"></em></a>
            <div class="ath-container m-0">
                <div class="ath-body">
                    <h5 class="ath-heading title">Reset <small class="tc-default">with your Email</small></h5>
                    <form action="#">
                        <div class="field-item">
                            <div class="field-wrap">
                                <input type="text" class="input-bordered" placeholder="Your Email">
                            </div>
                        </div>
                        <button class="btn btn-primary btn-block btn-md">Reset Password</button>
                        <div class="ath-note text-center">
                            Remembered? <a href="#" data-toggle="modal" data-dismiss="modal" data-target="#login-popup"> <strong>Sign in here</strong></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- .modal @e -->

<div class="preloader"><span class="spinner spinner-round"></span></div>

<!-- JavaScript -->
<script src="/azure_pro/assets/js/jquery.bundle.js?ver=192"></script>
<script src="/azure_pro/assets/js/scripts.js?ver=192"></script>
<script src="/azure_pro/assets/js/charts.js"></script>
</body>

</html>
