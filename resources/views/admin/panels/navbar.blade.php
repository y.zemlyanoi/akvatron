{{-- navabar  --}}
<div class="header-navbar-shadow"></div>
<nav class="header-navbar main-header-navbar navbar-expand-lg navbar navbar-with-menu
@if(isset($configData['navbarType'])){{$configData['navbarClass']}} @endif"
data-bgcolor="@if(isset($configData['navbarBgColor'])){{$configData['navbarBgColor']}}@endif">
  <div class="navbar-wrapper">
    <div class="navbar-container content">
      <div class="navbar-collapse" id="navbar-mobile">
        <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center"></div>
        <ul class="nav navbar-nav float-right">
          <li class="nav-item d-none d-lg-block">
              <a class="nav-link nav-link-expand">
                  <i class="ficon bx bx-fullscreen"></i>
              </a>
          </li>

          @include('admin.component.notification')
          @include('admin.component.user-dropdown')

        </ul>
      </div>
    </div>
  </div>
</nav>
