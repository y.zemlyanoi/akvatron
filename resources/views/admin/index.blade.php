@extends('admin.layouts.baseLayout')

{{-- page Title --}}
@section('title','Управление наполнением сайта')

@section('vendor-styles')
@endsection

@section('content')
    <section>
        <div class="col-md-12">
            <div class="card">
                @include('admin.component.card-header', [
                   'before' => 'Dashboard',
                   'title' => '',
                   'after' => '',
               ])
            </div>
        </div>
    </section>
@endsection

@section('vendor-scripts')
    <script src="{{asset('vendors/js/editors/quill/katex.min.js')}}"></script>
    <script src="{{asset('vendors/js/editors/quill/highlight.min.js')}}"></script>
    <script src="{{asset('vendors/js/editors/quill/quill.min.js')}}"></script>
    <script src="{{asset('vendors/js/extensions/jquery.steps.min.js')}}"></script>
    <script src="{{asset('vendors/js/forms/validation/jquery.validate.min.js')}}"></script>
@endsection

@section('page-scripts')
@endsection
