<ul class="nav nav-tabs" role="tablist">
    @foreach($tabs as $tab)
        <li class="nav-item @if($tab['current']) current @endif">
            <a class="nav-link @if($tab['current']) active @endif" id="{{ $tab['id'] }}-tab" data-toggle="tab" href="#{{ $tab['id'] }}" aria-controls="{{ $tab['id'] }}" role="tab" aria-selected="true">
                <span class="align-middle">
                    {{ $tab['name'] }}
                    @if(isset($tab['amount']))
                        <span class="badge badge-light-danger badge-pill badge-round float-right mr-2" style="position: relative; left: 10px; top: 3px;">
                            {{ $tab['amount'] }}
                        </span>
                    @endif
                </span>
            </a>
        </li>
    @endforeach
</ul>
