<div class="row">
    <div class="col-lg-12">
        <a href='{{ $route }}' class="btn btn-danger shadow mr-1 mb-1 {{ $className }}">
            {{ $text }}
        </a>
    </div>
</div>
