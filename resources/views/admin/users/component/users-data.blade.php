@foreach ($users as $user)
    <tr>
        <td>{{$user->name}}</td>
        <td>{{sprintf('%f', $user->power)}} TH/s</td>
        <td>{{sprintf('%f', $user->daily_profit)}}</td>
        <td>{{sprintf('%f', $user->earning)}}</td>
        <td>
            <a href="{{ route('admin.show.user', $user->id) }}">
                <i class="bx bx-edit-alt"></i>
            </a>
            <!--a href="#" data-toggle="modal" data-target="#large" onclick="getUserData({{$user->id}})">
                <i class="bx bx-edit-alt"></i>
            </a-->
        </td>
    </tr>
@endforeach
