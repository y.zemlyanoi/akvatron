@extends('admin.layouts.baseLayout')

{{-- page title --}}
@section('title','Управление пользователями')

{{-- vendor styles --}}
@section('vendor-styles')
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/tables/datatable/datatables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/pickers/pickadate/pickadate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/pickers/daterange/daterangepicker.css')}}">
@endsection
@section('page-styles')
    <link rel="stylesheet" type="text/css" href="{{asset('css/pages/page-users.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/custom.css')}}">
@endsection

@section('content')
    <!-- users list start -->
    <section class="users-list-wrapper">
        <div class="users-list-table">
            <div class="card">
                @include('admin.component.card-header', [
                   'before' => 'Users',
                   'title' => 'List',
                   'after' => '',
                ])
                <div class="card-content">
                    <div id="users" class="card-body">
                        <div class="users-list-filter">
                            <div class="row justify-content-center">
                                <div class="col-lg-3">
                                    <fieldset class="form-group">
                                        @include('admin.component.form.select', [
                                            'select' => $formData['select-user']])
                                    </fieldset>
                                </div>
                                <div class="col-lg-3">
                                    @include('admin.component.calendar', ['id'=>'calendar_users'])
                                </div>
                            </div>
                        </div>
                        <!-- datatable start -->
                        <div class="table-responsive">
                            <table id="users-list-datatable" class="table">
                                @include('admin.component.table.thead',
                                    ['names'=>$formData['users-head-table']]
                                )
                                <tbody>
                                    @include('admin.users.component.users-data', ['users'=>$users])
                                </tbody>
                            </table>
                        </div>
                        @include('admin.component.spinner')
                        <!-- datatable ends -->
                        @include('admin.users.component.pagination', ['users'=>$users, 'id'=>'users'])
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- users list ends -->

    <!--Окно просмотра пользователя-->
    <!--large size Modal -->
{{--    @include('admin.users.modal-user-info')--}}

    <!--Окно редактирования пользователя-->
    <!--small size modal -->
{{--    @include('admin.users.modal-user-edit')--}}

    <!--Окно просмотра заказа -->
    <!--Default size Modal -->
{{--    @include('admin.users.modal-user-orders')--}}
@endsection

{{-- vendor scripts --}}
@section('vendor-scripts')
    <script src="{{asset('vendors/js/tables/datatable/datatables.min.js')}}"></script>
    <script src="{{asset('vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}"></script>

    <script src="{{asset('vendors/js/pickers/pickadate/picker.js')}}"></script>
    <script src="{{asset('vendors/js/pickers/pickadate/picker.date.js')}}"></script>
    <script src="{{asset('vendors/js/pickers/pickadate/picker.time.js')}}"></script>
    <script src="{{asset('vendors/js/pickers/pickadate/legacy.js')}}"></script>

    <script src="{{asset('vendors/js/pickers/daterange/moment.min.js')}}"></script>
    <script src="{{asset('vendors/js/pickers/daterange/daterangepicker.js')}}"></script>
@endsection

{{-- page scripts --}}
@section('page-scripts')
    <script src="{{asset('js/admin/pickers/pickers-datetime.js')}}"></script>
    <script src="{{asset('js/admin/custom/users.js')}}"></script>
@endsection
