<div class="modal fade text-left" id="large" tabindex="-1" role="dialog" aria-labelledby="myModalLabel17"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel17">Андрей Петров</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="bx bx-x"></i>
                </button>
            </div>
            <div class="modal-body" style="background-color: lightgrey;">
                <a href='#' class="btn btn-danger shadow mr-1 mb-1" data-toggle="modal" data-target="#small">Редактировать</a>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Профиль</h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-lg-9">
                                            <!--Контакты-->
                                            <div class="fonticon-container-2">
                                                <div class="fonticon-wrap" style="display: inline-block;">
                                                    <i class="bx bx-envelope"></i>
                                                </div>
                                                <label class="fonticon-classname" style="padding-top: 0px; position: absolute; left: 40px; top: 1px;">user2143@example.com</label>
                                            </div>

                                            <div class="fonticon-container-2">
                                                <div class="fonticon-wrap" style="display: inline-block;">
                                                    <i class="bx bxs-calendar"></i>
                                                </div>
                                                <label class="fonticon-classname" style="padding-top: 0px; position: absolute; left: 40px; top: 23px;">Дата регистрации: 05:15:37 2019-04-17</label>
                                            </div>

                                            <div class="fonticon-container-2">
                                                <div class="fonticon-wrap" style="display: inline-block;">
                                                    <i class="bx bxs-calendar"></i>
                                                </div>
                                                <label class="fonticon-classname" style="padding-top: 0px; position: absolute; left: 40px; top: 46px;">Последний визит: 05:15:37 2019-04-17</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-3" style="text-align: center;">
                                            <span style="font-weight: 600; font-size: 24px;">200 TH/s</span><br>
                                            <span style="font-size: 14px;">Активная мощность</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Активные валюты</h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-lg-6" style="margin-bottom: 10px;">
                                            <div class="row">
                                                <div class="col-lg-5" style="line-height: 1px;">
                                                    <div style="float: left; margin-right: 10px;">
                                                        <img src="/images/coins/bitcoin.png" alt="btc" style="display: inline-block;" width="44">
                                                    </div>
                                                    <h5 style="font-weight: 600;">BTC</h5>
                                                    <span style="color: darkgray; font-size: 10px; font-weight: 700;">Bitcoin</span>
                                                </div>
                                                <div class="col-lg-7" style="padding:0; font-size: 10px; font-weight: 500">
                                                    Мощность: <span style="color: black; font-weight: 700;">100 TH/s</span><br>
                                                    Прибыль: <span style="color: black; font-weight: 700;">$0.0904/T</span><br>
                                                    Окончание аренды: <span style="color: black; font-weight: 700;">12:22 04-18-2020</span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-6" style="margin-bottom: 10px;">
                                            <div class="row">
                                                <div class="col-lg-5" style="line-height: 1px;">
                                                    <div style="float: left; margin-right: 10px;">
                                                        <img src="/images/coins/bitcoin_cash.png" alt="btc" style="display: inline-block;" width="44">
                                                    </div>
                                                    <h5 style="font-weight: 600;">BCH</h5>
                                                    <span style="color: darkgray; font-size: 10px; font-weight: 700;">Bitcoin cash</span>
                                                </div>
                                                <div class="col-lg-7" style="padding:0; font-size: 10px; font-weight: 500">
                                                    Мощность: <span style="color: black; font-weight: 700;">100 TH/s</span><br>
                                                    Прибыль: <span style="color: black; font-weight: 700;">$0.0904/T</span><br>
                                                    Окончание аренды: <span style="color: black; font-weight: 700;">12:22 04-18-2020</span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-6" style="margin-bottom: 10px;">
                                            <div class="row">
                                                <div class="col-lg-5" style="line-height: 1px;">
                                                    <div style="float: left; margin-right: 10px;">
                                                        <img src="/images/coins/litecoin.png" alt="btc" style="display: inline-block;" width="44">
                                                    </div>
                                                    <h5 style="font-weight: 600;">LTC</h5>
                                                    <span style="color: darkgray; font-size: 10px; font-weight: 700;">Litecoin</span>
                                                </div>
                                                <div class="col-lg-7" style="padding:0; font-size: 10px; font-weight: 500">
                                                    Мощность: <span style="color: black; font-weight: 700;">100 TH/s</span><br>
                                                    Прибыль: <span style="color: black; font-weight: 700;">$0.0904/T</span><br>
                                                    Окончание аренды: <span style="color: black; font-weight: 700;">12:22 04-18-2020</span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-6" style="margin-bottom: 10px;">
                                            <div class="row">
                                                <div class="col-lg-5" style="line-height: 1px;">
                                                    <div style="float: left; margin-right: 10px;">
                                                        <img src="/images/coins/ethereum.png" alt="btc" style="display: inline-block;" width="44">
                                                    </div>
                                                    <h5 style="font-weight: 600;">ETC</h5>
                                                    <span style="color: darkgray; font-size: 10px; font-weight: 700;">Ethereum</span>
                                                </div>
                                                <div class="col-lg-7" style="padding:0; font-size: 10px; font-weight: 500">
                                                    Мощность: <span style="color: black; font-weight: 700;">100 TH/s</span><br>
                                                    Прибыль: <span style="color: black; font-weight: 700;">$0.0904/T</span><br>
                                                    Окончание аренды: <span style="color: black; font-weight: 700;">12:22 04-18-2020</span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-6" style="margin-bottom: 10px;">
                                            <div class="row">
                                                <div class="col-lg-5" style="line-height: 1px;">
                                                    <div style="float: left; margin-right: 10px;">
                                                        <img src="/images/coins/zcash.png" alt="btc" style="display: inline-block;" width="44">
                                                    </div>
                                                    <h5 style="font-weight: 600;">ZEC</h5>
                                                    <span style="color: darkgray; font-size: 10px; font-weight: 700;">Zcash</span>
                                                </div>
                                                <div class="col-lg-7" style="padding:0; font-size: 10px; font-weight: 500">
                                                    Мощность: <span style="color: black; font-weight: 700;">100 TH/s</span><br>
                                                    Прибыль: <span style="color: black; font-weight: 700;">$0.0904/T</span><br>
                                                    Окончание аренды: <span style="color: black; font-weight: 700;">12:22 04-18-2020</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Финансовая статистика</h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <!--Финансовая статистика-->
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <fieldset class="form-group">
                                                <select class="form-control" id="users-list-verified-2">
                                                    <option value="0">Все операции</option>
                                                    <option value="1">Пополнения</option>
                                                    <option value="2">Выплаты</option>
                                                </select>
                                            </fieldset>
                                        </div>
                                        <div class="col-lg-5">
                                            <fieldset class="form-group position-relative has-icon-left">
                                                <input type="text" class="form-control showCalRanges" placeholder="Select Date">
                                                <div class="form-control-position">
                                                    <i class='bx bx-calendar-check'></i>
                                                </div>
                                            </fieldset>
                                        </div>
                                        <div class="col-lg-2" style="text-align: center;">
                                            <span style="font-weight: 600; font-size: 18px;">75000 $</span><br>
                                            <span style="font-size: 12px;">ПОПОЛНЕНИИ</span>
                                        </div>
                                        <div class="col-lg-2" style="text-align: center;">
                                            <span style="font-weight: 600; font-size: 18px;">100000 $</span><br>
                                            <span style="font-size: 12px;">ВЫПЛАТ</span>
                                        </div>
                                    </div>

                                    <div class="row" style="margin-left: 0; margin-right: 0;">
                                        <div class="col-lg-12" style="padding-left: 30px; padding-right: 30px; padding-top: 10px;">
                                            <div class="row">
                                                <div class="order-block col-lg-12" style="border: solid 1px gainsboro; border-radius: 5px; margin-bottom: 10px;">
                                                    <div class="header-order-block" style="margin: 6px 0;border-bottom: solid 1px gainsboro;padding-bottom: 6px;">
                                                        <div class="row">
                                                            <div class="col-lg-8" style="font-weight: 500; color: #0066ff;">
                                                                Пополнение
                                                            </div>
                                                            <div class="col-lg-4" style="font-size: 12px; margin-top: 3px; text-align: right;">
                                                                March 11, 2020 4:54 AM
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="body-order-block" style="margin: 10px 0;">
                                                        <div class="row">
                                                            <div class="col-lg-7">
                                                                <table style="font-size: 12px; border-collapse: inherit;">
                                                                    <tr>
                                                                        <td style="font-weight: 500;">Сумма</td>
                                                                        <td style="padding-left: 15px;">120 Eth</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="font-weight: 500;">Статус</td>
                                                                        <td style="padding-left: 15px;" style="font-weight: 500;"><span class="text-warning">Ожидает</span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="font-weight: 500;">ID Заказа</td>
                                                                        <td style="padding-left: 15px;"><a href="#" data-toggle="modal" data-target="#defaultSize">1114352462467357</a></td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                            <div class="col-lg-3" style="vertical-align: middle;">
                                                                <button type="button" class="btn btn-primary mr-1 mb-1 btn-sm" style="margin: 0 auto; top: 25%; position: absolute;">Проверить</button>
                                                            </div>
                                                            <div class="col-lg-2" style="vertical-align: middle;text-align: right;">
                                                                <img src="/images/coins/ethereum.png" alt="eth" width="37" style="margin: 0 auto; margin-top: 18%;">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="order-block col-lg-12" style="border: solid 1px gainsboro; border-radius: 5px; margin-bottom: 10px;">
                                                    <div class="header-order-block" style="margin: 6px 0;border-bottom: solid 1px gainsboro;padding-bottom: 6px;">
                                                        <div class="row">
                                                            <div class="col-lg-8" style="font-weight: 500; color: #0066ff;">
                                                                Пополнение
                                                            </div>
                                                            <div class="col-lg-4" style="font-size: 12px; margin-top: 3px; text-align: right;">
                                                                March 11, 2020 4:54 AM
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="body-order-block" style="margin: 10px 0;">
                                                        <div class="row">
                                                            <div class="col-lg-7">
                                                                <table style="font-size: 12px; border-collapse: inherit;">
                                                                    <tr>
                                                                        <td style="font-weight: 500;">Сумма</td>
                                                                        <td style="padding-left: 15px;">12 Btc</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="font-weight: 500;">Кошелёк</td>
                                                                        <td style="padding-left: 15px;">q19enege045bergfhner4y</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="font-weight: 500;">Статус</td>
                                                                        <td style="padding-left: 15px;" style="font-weight: 500;"><span class="text-danger">Отказ</span></td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                            <div class="col-lg-3" style="vertical-align: middle;">

                                                            </div>
                                                            <div class="col-lg-2" style="vertical-align: middle;text-align: right;">
                                                                <img src="/images/coins/bitcoin.png" alt="eth" width="37" style="margin: 0 auto; margin-top: 18%;">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="order-block col-lg-12" style="border: solid 1px gainsboro; border-radius: 5px; margin-bottom: 10px;">
                                                    <div class="header-order-block" style="margin: 6px 0;border-bottom: solid 1px gainsboro;padding-bottom: 6px;">
                                                        <div class="row">
                                                            <div class="col-lg-8" style="font-weight: 500; color: #0066ff;">
                                                                Пополнение
                                                            </div>
                                                            <div class="col-lg-4" style="font-size: 12px; margin-top: 3px; text-align: right;">
                                                                March 11, 2020 4:54 AM
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="body-order-block" style="margin: 10px 0;">
                                                        <div class="row">
                                                            <div class="col-lg-7">
                                                                <table style="font-size: 12px; border-collapse: inherit;">
                                                                    <tr>
                                                                        <td style="font-weight: 500;">Сумма</td>
                                                                        <td style="padding-left: 15px;">12 Btc</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="font-weight: 500;">Статус</td>
                                                                        <td style="padding-left: 15px;" style="font-weight: 500;"><span class="text-success">Успешно</span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="font-weight: 500;">ID Заказа</td>
                                                                        <td style="padding-left: 15px;"><a href="#" data-toggle="modal" data-target="#defaultSize">1114352462467357</a></td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                            <div class="col-lg-3" style="vertical-align: middle;">

                                                            </div>
                                                            <div class="col-lg-2" style="vertical-align: middle;text-align: right;">
                                                                <img src="/images/coins/bitcoin.png" alt="eth" width="37" style="margin: 0 auto; margin-top: 18%;">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Заметки</h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    Click to add notes
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--
            <div class="modal-footer">
                <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                    <i class="bx bx-x d-block d-sm-none"></i>
                    <span class="d-none d-sm-block">Close</span>
                </button>
                <button type="button" class="btn btn-primary ml-1" data-dismiss="modal">
                    <i class="bx bx-check d-block d-sm-none"></i>
                    <span class="d-none d-sm-block">Accept</span>
                </button>
            </div>
            -->
        </div>
    </div>
</div>
