<div class="modal fade text-left" id="small" tabindex="-1" role="dialog" aria-labelledby="myModalLabel19"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel19">Редактирование пользователя</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="bx bx-x"></i>
                </button>
            </div>
            <div class="modal-body">
                <fieldset class="form-group">
                    <label for="nameInput">Имя</label>
                    <input type="text" class="form-control" id="nameInput">
                </fieldset>
                <fieldset class="form-group">
                    <label for="emailInput">Email</label>
                    <input type="text" class="form-control" id="emailInput">
                </fieldset>
                <button type="button" class="btn btn-dark mr-1 mb-1" onclick="$('#field-password').show();">Сменить пароль</button><br>
                <fieldset class="form-group" id="field-password" style="display: none;">
                    <label for="passwordInput">Пароль</label>
                    <input type="password" class="form-control" id="passwordInput">
                </fieldset>
                <a href="#" style="font-size: 10px; font-weight: 500;"><i class="bx bx-link" style="font-size: 12px;"></i>Управление привязанными кошелькми</a>
                <div id="type-coins">
                    <fieldset class="form-group">
                        <label for="btcInput">BTC</label>
                        <input type="text" class="form-control" id="btcInput">
                    </fieldset>
                    <fieldset class="form-group">
                        <label for="ltcInput">LTC</label>
                        <input type="text" class="form-control" id="ltcInput">
                    </fieldset>
                    <fieldset class="form-group">
                        <label for="bchInput">BCH</label>
                        <input type="text" class="form-control" id="bchInput">
                    </fieldset>
                    <fieldset class="form-group">
                        <label for="ethInput">ETH</label>
                        <input type="text" class="form-control" id="ethInput">
                    </fieldset>
                    <fieldset class="form-group">
                        <label for="zecInput">ZEC</label>
                        <input type="text" class="form-control" id="zecInput">
                    </fieldset>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-secondary btn-sm" data-dismiss="modal">
                    <i class="bx bx-x d-block d-sm-none"></i>
                    <span class="d-sm-block d-none">Отмена</span>
                </button>
                <button type="button" class="btn btn-primary ml-1 btn-sm">
                    <i class="bx bx-check d-block d-sm-none"></i>
                    <span class="d-sm-block d-none">Сохранить</span>
                </button>
            </div>
        </div>
    </div>
</div>
