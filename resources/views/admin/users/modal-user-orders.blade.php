<div class="modal fade text-left" id="defaultSize" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel18" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel18">Сведения о заказе</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="bx bx-x"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="alert bg-rgba-warning alert-dismissible mb-2" role="alert">
                    <div class="d-flex align-items-center">
                        <i class="bx bx-error-circle"></i>
                        <span>
                            Remaining payment time 02:44:26 or the order will be automatically closed after the timeout.
                        </span>
                    </div>
                </div>

                <h4>Product information</h4>
                <div class="table-responsive product-information-table">
                    <table class="table mb-0">
                        <thead class="thead-light">
                        <tr>
                            <th>Currency</th>
                            <th>Amount</th>
                            <th>Plan duration</th>
                            <th>Price</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>BTC</td>
                            <td>50 TH/s</td>
                            <td>360 Days</td>
                            <td class="text-bold-500">$ 506.00</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <h4>Payment information</h4>
                <div class="table-responsive">
                    <table class="table mb-0">
                        <tbody>
                        <tr>
                            <td class="text-muted">Amount</td>
                            <td>$526.30</td>
                        </tr>
                        <tr>
                            <td class="text-muted">Payment Currency</td>
                            <td style="color: black;">ETH</td>
                        </tr>
                        <tr>
                            <td class="text-muted">Payment Status</td>
                            <td>Unpaid</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <!--
                I love candy candy cake powder I love icing ice cream pastry. Biscuit lemon drops sesame
                snaps. Topping biscuit croissant gummi bears jelly beans cake cake bear claw muffin. Lemon
                drops oat cake pastry bear claw liquorice lemon drops.
                -->
            </div>
            <!--
            <div class="modal-footer">
                <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                    <i class="bx bx-x d-block d-sm-none"></i>
                    <span class="d-none d-sm-block">Close</span>
                </button>
                <button type="button" class="btn btn-primary ml-1" data-dismiss="modal">
                    <i class="bx bx-check d-block d-sm-none"></i>
                    <span class="d-none d-sm-block">Accept</span>
                </button>
            </div>
            -->
        </div>
    </div>
</div>
