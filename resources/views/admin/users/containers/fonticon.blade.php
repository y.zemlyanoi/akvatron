<div class="fonticon-container-2">
    <div class="fonticon-wrap">
        <i class="bx {{ $classNameIcon }}"></i>
        <label class="{{ $classNameLabel }}">{{ $labelData }}</label>
    </div>
</div>
