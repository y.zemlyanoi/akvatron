<div class="row">
    @include('admin.users.containers.power')

    <div class="col-lg-6" style="margin-bottom: 10px;">
        <div class="row">
            <div class="col-lg-5" style="line-height: 1px;">
                <div style="float: left; margin-right: 10px;">
                    <img src="/images/coins/bitcoin_cash.png" alt="btc" style="display: inline-block;" width="44">
                </div>
                <h5 style="font-weight: 600;">BCH</h5>
                <span style="color: darkgray; font-size: 10px; font-weight: 700;">Bitcoin cash</span>
            </div>
            <div class="col-lg-7" style="padding:0; font-size: 10px; font-weight: 500">
                Мощность: <span style="color: black; font-weight: 700;">100 TH/s</span><br>
                Прибыль: <span style="color: black; font-weight: 700;">$0.0904/T</span><br>
                Окончание аренды: <span style="color: black; font-weight: 700;">12:22 04-18-2020</span>
            </div>
        </div>
    </div>

    <div class="col-lg-6" style="margin-bottom: 10px;">
        <div class="row">
            <div class="col-lg-5" style="line-height: 1px;">
                <div style="float: left; margin-right: 10px;">
                    <img src="/images/coins/litecoin.png" alt="btc" style="display: inline-block;" width="44">
                </div>
                <h5 style="font-weight: 600;">LTC</h5>
                <span style="color: darkgray; font-size: 10px; font-weight: 700;">Litecoin</span>
            </div>
            <div class="col-lg-7" style="padding:0; font-size: 10px; font-weight: 500">
                Мощность: <span style="color: black; font-weight: 700;">100 TH/s</span><br>
                Прибыль: <span style="color: black; font-weight: 700;">$0.0904/T</span><br>
                Окончание аренды: <span style="color: black; font-weight: 700;">12:22 04-18-2020</span>
            </div>
        </div>
    </div>

    <div class="col-lg-6" style="margin-bottom: 10px;">
        <div class="row">
            <div class="col-lg-5" style="line-height: 1px;">
                <div style="float: left; margin-right: 10px;">
                    <img src="/images/coins/ethereum.png" alt="btc" style="display: inline-block;" width="44">
                </div>
                <h5 style="font-weight: 600;">ETC</h5>
                <span style="color: darkgray; font-size: 10px; font-weight: 700;">Ethereum</span>
            </div>
            <div class="col-lg-7" style="padding:0; font-size: 10px; font-weight: 500">
                Мощность: <span style="color: black; font-weight: 700;">100 TH/s</span><br>
                Прибыль: <span style="color: black; font-weight: 700;">$0.0904/T</span><br>
                Окончание аренды: <span style="color: black; font-weight: 700;">12:22 04-18-2020</span>
            </div>
        </div>
    </div>

    <div class="col-lg-6" style="margin-bottom: 10px;">
        <div class="row">
            <div class="col-lg-5" style="line-height: 1px;">
                <div style="float: left; margin-right: 10px;">
                    <img src="/images/coins/zcash.png" alt="btc" style="display: inline-block;" width="44">
                </div>
                <h5 style="font-weight: 600;">ZEC</h5>
                <span style="color: darkgray; font-size: 10px; font-weight: 700;">Zcash</span>
            </div>
            <div class="col-lg-7" style="padding:0; font-size: 10px; font-weight: 500">
                Мощность: <span style="color: black; font-weight: 700;">100 TH/s</span><br>
                Прибыль: <span style="color: black; font-weight: 700;">$0.0904/T</span><br>
                Окончание аренды: <span style="color: black; font-weight: 700;">12:22 04-18-2020</span>
            </div>
        </div>
    </div>
</div>
