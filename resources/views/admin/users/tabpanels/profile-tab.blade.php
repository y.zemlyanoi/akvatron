@include('admin.users.component.btn-edit', [
    'route' => route('admin.edit.user', $user->id),
    'text' => 'Edit',
    'className' => 'float-right'
])
<div class="row">
    <div class="col-lg-9">
        <!--Контакты-->
        @include('admin.users.containers.fonticon', [
            'classNameIcon' => 'bx-envelope pr-1 float-left',
            'classNameLabel' => '',
            'labelData' => $user->email
        ])

        @include('admin.users.containers.fonticon', [
            'classNameIcon' => 'bx-calendar pr-1 float-left',
            'classNameLabel' => '',
            'labelData' => 'Дата регистрации: '.$user->created_at
        ])

        @include('admin.users.containers.fonticon', [
            'classNameIcon' => 'bx-calendar pr-1 float-left',
            'classNameLabel' => '',
            'labelData' => 'Последний визит: '.$user->updated_at
        ])
    </div>
    <div class="col-lg-3" style="text-align: center;">
        <span style="font-weight: 600; font-size: 24px;">200 TH/s</span><br>
        <span style="font-size: 14px;">Активная мощность</span>
    </div>
</div>
