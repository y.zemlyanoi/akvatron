@extends('admin.layouts.baseLayout')

{{-- page title --}}
@section('title','Управление пользователями')

{{-- vendor styles --}}
@section('vendor-styles')
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/tables/datatable/datatables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/pickers/pickadate/pickadate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/pickers/daterange/daterangepicker.css')}}">
@endsection

{{-- page styles --}}
@section('page-styles')
@endsection

@section('content')
    <!-- users list start -->
    <section class="users-list-wrapper">
        <div class="card">
            @include('admin.component.card-header', [
                   'before' => 'Show Information',
                   'title' => 'about',
                   'after' => $user->name,
               ])
            <div class="card-content">
                <div class="card-body">
                    @include('admin.users.component.nav-tab-head', [
                        'tabs' => $formData["nav-tab-users"]
                    ])
                    <div class="tab-content">
                        <!--Профиль-->
                        <div class="tab-pane active" id="profile" aria-labelledby="profile-tab" role="tabpanel">
                            @include('admin.users.tabpanels.profile-tab', ['user'=>$user])
                        </div>
                        <!--Активные валюты-->
                        <div class="tab-pane" id="active_currencies" aria-labelledby="active_currencies-tab" role="tabpanel">
                            @include('admin.users.tabpanels.user-currencies-tab')
                        </div>
                        <!--Финансовая статистика-->
                        <div class="tab-pane" id="financial_statistics" aria-labelledby="financial_statistics-tab" role="tabpanel">
                            <div class="row">
                                <div class="col-lg-3">
                                    <fieldset class="form-group">
                                        @include('admin.component.form.select', [
                                                'select' => $formData['select-financial-statistic']])
                                    </fieldset>
                                </div>
                                <div class="col-lg-5">
                                    @include('admin.component.calendar', ['id'=>'calendar_financial_statistics'])
                                </div>
                                <div class="col-lg-2" style="text-align: center;">
                                    <span style="font-weight: 600; font-size: 18px;">75000 $</span><br>
                                    <span style="font-size: 12px;">ПОПОЛНЕНИИ</span>
                                </div>
                                <div class="col-lg-2" style="text-align: center;">
                                    <span style="font-weight: 600; font-size: 18px;">100000 $</span><br>
                                    <span style="font-size: 12px;">ВЫПЛАТ</span>
                                </div>
                            </div>

                            <div class="row" style="margin-left: 0; margin-right: 0;">
                                <div class="col-lg-12" style="padding-left: 30px; padding-right: 30px; padding-top: 10px;">
                                    <div class="row">
                                        <div class="order-block col-lg-12" style="border: solid 1px gainsboro; border-radius: 5px; margin-bottom: 10px;">
                                            <div class="header-order-block" style="margin: 6px 0;border-bottom: solid 1px gainsboro;padding-bottom: 6px;">
                                                <div class="row">
                                                    <div class="col-lg-8" style="font-weight: 500; color: #0066ff;">
                                                        Пополнение
                                                    </div>
                                                    <div class="col-lg-4" style="font-size: 12px; margin-top: 3px; text-align: right;">
                                                        March 11, 2020 4:54 AM
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="body-order-block" style="margin: 10px 0;">
                                                <div class="row">
                                                    <div class="col-lg-7">
                                                        <table style="font-size: 12px; border-collapse: inherit;">
                                                            <tr>
                                                                <td style="font-weight: 500;">Сумма</td>
                                                                <td style="padding-left: 15px;">120 Eth</td>
                                                            </tr>
                                                            <tr>
                                                                <td style="font-weight: 500;">Статус</td>
                                                                <td style="padding-left: 15px;" style="font-weight: 500;"><span class="text-warning">Ожидает</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="font-weight: 500;">ID Заказа</td>
                                                                <td style="padding-left: 15px;"><a href="#" data-toggle="modal" data-target="#defaultSize">1114352462467357</a></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div class="col-lg-3" style="vertical-align: middle;">
                                                        <button type="button" class="btn btn-primary mr-1 mb-1 btn-sm" style="margin: 0 auto; top: 25%; position: absolute;">Проверить</button>
                                                    </div>
                                                    <div class="col-lg-2" style="vertical-align: middle;text-align: right;">
                                                        <img src="/images/coins/ethereum.png" alt="eth" width="37" style="margin: 0 auto; margin-top: 18%;">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="order-block col-lg-12" style="border: solid 1px gainsboro; border-radius: 5px; margin-bottom: 10px;">
                                            <div class="header-order-block" style="margin: 6px 0;border-bottom: solid 1px gainsboro;padding-bottom: 6px;">
                                                <div class="row">
                                                    <div class="col-lg-8" style="font-weight: 500; color: #0066ff;">
                                                        Пополнение
                                                    </div>
                                                    <div class="col-lg-4" style="font-size: 12px; margin-top: 3px; text-align: right;">
                                                        March 11, 2020 4:54 AM
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="body-order-block" style="margin: 10px 0;">
                                                <div class="row">
                                                    <div class="col-lg-7">
                                                        <table style="font-size: 12px; border-collapse: inherit;">
                                                            <tr>
                                                                <td style="font-weight: 500;">Сумма</td>
                                                                <td style="padding-left: 15px;">12 Btc</td>
                                                            </tr>
                                                            <tr>
                                                                <td style="font-weight: 500;">Кошелёк</td>
                                                                <td style="padding-left: 15px;">q19enege045bergfhner4y</td>
                                                            </tr>
                                                            <tr>
                                                                <td style="font-weight: 500;">Статус</td>
                                                                <td style="padding-left: 15px;" style="font-weight: 500;"><span class="text-danger">Отказ</span></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div class="col-lg-3" style="vertical-align: middle;">

                                                    </div>
                                                    <div class="col-lg-2" style="vertical-align: middle;text-align: right;">
                                                        <img src="/images/coins/bitcoin.png" alt="eth" width="37" style="margin: 0 auto; margin-top: 18%;">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="order-block col-lg-12" style="border: solid 1px gainsboro; border-radius: 5px; margin-bottom: 10px;">
                                            <div class="header-order-block" style="margin: 6px 0;border-bottom: solid 1px gainsboro;padding-bottom: 6px;">
                                                <div class="row">
                                                    <div class="col-lg-8" style="font-weight: 500; color: #0066ff;">
                                                        Пополнение
                                                    </div>
                                                    <div class="col-lg-4" style="font-size: 12px; margin-top: 3px; text-align: right;">
                                                        March 11, 2020 4:54 AM
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="body-order-block" style="margin: 10px 0;">
                                                <div class="row">
                                                    <div class="col-lg-7">
                                                        <table style="font-size: 12px; border-collapse: inherit;">
                                                            <tr>
                                                                <td style="font-weight: 500;">Сумма</td>
                                                                <td style="padding-left: 15px;">12 Btc</td>
                                                            </tr>
                                                            <tr>
                                                                <td style="font-weight: 500;">Статус</td>
                                                                <td style="padding-left: 15px;" style="font-weight: 500;"><span class="text-success">Успешно</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="font-weight: 500;">ID Заказа</td>
                                                                <td style="padding-left: 15px;"><a href="#" data-toggle="modal" data-target="#defaultSize">1114352462467357</a></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div class="col-lg-3" style="vertical-align: middle;">

                                                    </div>
                                                    <div class="col-lg-2" style="vertical-align: middle;text-align: right;">
                                                        <img src="/images/coins/bitcoin.png" alt="eth" width="37" style="margin: 0 auto; margin-top: 18%;">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Заметка-->
                        <div class="tab-pane" id="notes" aria-labelledby="notes-tab" role="tabpanel">
                            @include('admin.users.tabpanels.notes-tab', ['user'=>$user])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- users list ends -->
@endsection

{{-- vendor scripts --}}
@section('vendor-scripts')
    <script src="{{asset('vendors/js/tables/datatable/datatables.min.js')}}"></script>
    <script src="{{asset('vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}"></script>

    <script src="{{asset('vendors/js/pickers/pickadate/picker.js')}}"></script>
    <script src="{{asset('vendors/js/pickers/pickadate/picker.date.js')}}"></script>
    <script src="{{asset('vendors/js/pickers/pickadate/picker.time.js')}}"></script>
    <script src="{{asset('vendors/js/pickers/pickadate/legacy.js')}}"></script>

    <script src="{{asset('vendors/js/pickers/daterange/moment.min.js')}}"></script>
    <script src="{{asset('vendors/js/pickers/daterange/daterangepicker.js')}}"></script>
@endsection

{{-- page scripts --}}
@section('page-scripts')
    <script src="{{asset('js/scripts/navs/navs.js')}}"></script>
@endsection
