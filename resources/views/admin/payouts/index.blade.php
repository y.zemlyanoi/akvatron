@php
    use Illuminate\Support\Carbon;
@endphp

@extends('admin.layouts.baseLayout')

{{-- page Title --}}
@section('title','Управление выплатами')

{{-- vendor css --}}
@section('vendor-styles')
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/tables/datatable/datatables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/pickers/pickadate/pickadate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/pickers/daterange/daterangepicker.css')}}">
@endsection

@section('page-styles')
    <link rel="stylesheet" type="text/css" href="{{asset('css/pages/dashboard-ecommerce.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/custom.css')}}">
@endsection

@section('content')
    <section id="basic-tabs-components">
        <div class="card">
            @include('admin.component.card-header', [
               'before' => 'Payout',
               'title' => 'Management',
               'after' => '',
            ])
            <div class="card-content">
                <div class="card-body">
                    @include('admin.payouts.component.nav-tab-head', [
                        'tabs' => $formData["nav-tab-payouts"]
                    ])
                    <div class="tab-content">
                        <div class="tab-pane active" id="unconfirmed" aria-labelledby="unconfirmed-tab" role="tabpanel">
                            <div class="row">
                                @include('admin.payouts.component.sort-payouts')
                                @include('admin.payouts.component.calendar-payouts', ['id'=>'unconfirmed_calendar_payouts'])
                            </div>
                            <!-- datatable start -->
                            <div class="table-responsive">
                                @include('admin.component.spinner')
                                <table id="unconfirmed-list-datatable" class="table" bgcolor="#F2F4F4">
                                    @include('admin.component.table.thead',
                                        ['names'=>$formData['unconfirmed-head-table']]
                                    )
                                    <tbody>
                                        @include('admin.payouts.component.table.unconfirmed-data', ['payouts'=>$unconfirmedPayouts])
                                    </tbody>
                                </table>
                            </div>
                            <!-- datatable ends -->
                            @include('admin.payouts.component.pagination', ['payouts'=>$unconfirmedPayouts, 'id'=>'unconfirmed'])
                        </div>
                        <div class="tab-pane" id="completed" aria-labelledby="completed-tab" role="tabpanel">
                            <div class="row">
                                @include('admin.payouts.component.sort-payouts')
                                @include('admin.payouts.component.calendar-payouts', ['id'=>'completed_calendar_payouts'])
                            </div>
                            <!-- datatable start -->
                            <div class="table-responsive">
                                @include('admin.component.spinner')
                                <table id="completed-list-datatable" class="table">
                                    @include('admin.component.table.thead',
                                        ['names'=>$formData['completed-head-table']]
                                    )
                                    <tbody>
                                        @include('admin.payouts.component.table.completed-data', [
                                            "payouts"=>$completedPayouts
                                        ])
                                    </tbody>
                                </table>
                            </div>
                            <!-- datatable ends -->
                            @include('admin.payouts.component.pagination', [
                                'payouts'=>$completedPayouts,
                                'id'=>'completed'
                             ])
                        </div>
                        <div class="tab-pane" id="canceled" aria-labelledby="canceled-tab" role="tabpanel">
                            <div class="row">
                                @include('admin.payouts.component.sort-payouts')
                                @include('admin.payouts.component.calendar-payouts', ['id'=>'canceled_calendar_payouts'])
                            </div>
                            <!-- datatable start -->
                            <div class="table-responsive">
                                @include('admin.component.spinner')
                                <table id="canceled-list-datatable" class="table">
                                    @include('admin.component.table.thead',
                                        ['names'=>$formData['canceled-head-table']]
                                    )
                                    <tbody>
                                        @include('admin.payouts.component.table.canceled-data', [
                                            "payouts"=>$canceledPayouts
                                        ])
                                    </tbody>
                                </table>
                            </div>
                            <!-- datatable ends -->
                            @include('admin.payouts.component.pagination', [
                                'payouts'=>$canceledPayouts,
                                'id'=>'canceled'
                             ])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Default size Modal -->
    @include('admin.payouts.modal-payout-accept')
    <!--small size modal -->
    @include('admin.payouts.modal-payout-cancel')
@endsection

@section('vendor-scripts')
    <script src="{{asset('vendors/js/tables/datatable/datatables.min.js')}}"></script>
    <script src="{{asset('vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}"></script>

    <script src="{{asset('vendors/js/pickers/pickadate/picker.js')}}"></script>
    <script src="{{asset('vendors/js/pickers/pickadate/picker.date.js')}}"></script>
    <script src="{{asset('vendors/js/pickers/pickadate/picker.time.js')}}"></script>
    <script src="{{asset('vendors/js/pickers/pickadate/legacy.js')}}"></script>

    <script src="{{asset('vendors/js/pickers/daterange/moment.min.js')}}"></script>
    <script src="{{asset('vendors/js/pickers/daterange/daterangepicker.js')}}"></script>
    <!-- ||||||||||||||||||||||||||||||||||||||||||||||||| -->
    <script src="{{asset('js/scripts/navs/navs.js')}}"></script>
{{--    <script src="{{asset('js/scripts/pickers/dateTime/pick-a-datetime.js')}}"></script>--}}
    <script src="{{asset('js/admin/custom/payouts.js')}}"></script>
    <script src="{{asset('js/admin/pickers/pickers-datetime.js')}}"></script>
@endsection

