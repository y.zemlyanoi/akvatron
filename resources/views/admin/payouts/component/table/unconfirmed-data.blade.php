@if(count($payouts) == 0)
    <tr><td colspan="5" align="center">No result</td></tr>
@endif
@foreach($payouts as $payout)
    <tr class="pay-{{$payout->id}}">
        <td>{{$payout->user->name}}</td>
        <td>{{$payout->amount}} {{$payout->coin->name}} (1253 $)</td>
        <td>
            {{$payout->wallet->address}}
        </td>
        <td>
            {{ Illuminate\Support\Carbon::parse($payout->created_at)->format('H:i:s d.m.Y')}}
        </td>
        <td>
            <a href="#" data-toggle="modal" data-target="#defaultSize" data-pay="{{$payout->id}}" class="confirm-pay text-success">
                <i class="bx bx-check-circle"></i>
            </a>

            <a href="#" data-toggle="modal" data-target="#small" data-pay="{{$payout->id}}" class="cancel-pay text-danger">
                <i class="bx bx-x-circle"></i>
            </a>
        </td>
    </tr>
@endforeach
