@if(count($payouts) == 0)
    <tr><td colspan="6" align="center">No result</td></tr>
@endif
@foreach($payouts as $payout)
    <tr @if ($payout->confirmed) class="table-danger" @endif>
        <td>{{$payout->user->name}}</td>
        <td>{{$payout->amount}} {{$payout->coin->name}} (1253 $)</td>
        <td>
            {{$payout->wallet->address}}
        </td>
        <td>{{ Illuminate\Support\Carbon::parse($payout->created_at)->format('H:i:s d.m.Y')}}</td>
        <td>{{ Illuminate\Support\Carbon::parse($payout->confirmed_at)->format('H:i:s d.m.Y')}}</td>
        <td>{{ $payout->reason }}</td>
    </tr>
@endforeach
