<div class="col-lg-3">
    <fieldset class="form-group position-relative has-icon-left">
        <input id="{{ $id }}" type="text" class="form-control" placeholder="Select Date">
        <div class="form-control-position">
            <i class='bx bx-calendar-check'></i>
        </div>
    </fieldset>
</div>
