<div class="modal fade text-left" id="small" tabindex="-1" role="dialog" aria-labelledby="myModalLabel19"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel19">Отказ в выводе</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="bx bx-x"></i>
                </button>
            </div>
            <div class="modal-body">
                Укажите причину отказа вывода средств
                <fieldset class="form-group">
                    <textarea class="form-control" id="basicTextarea" rows="3" placeholder="Textarea"></textarea>
                </fieldset>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-secondary btn-sm" data-dismiss="modal">
                    <i class="bx bx-x d-block d-sm-none"></i>
                    <span class="d-sm-block d-none">Отмена</span>
                </button>
                <button type="button" class="btn btn-primary ml-1 btn-sm btn-cancel-pay">
                    <i class="bx bx-check d-block d-sm-none"></i>
                    <span class="d-sm-block d-none">Отказать</span>
                </button>
            </div>
        </div>
    </div>
</div>
@section('page-scripts')
@endsection
