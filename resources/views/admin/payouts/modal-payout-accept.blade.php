<div class="modal fade text-left" id="defaultSize" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel18" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel18">Подтверждения вывода</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="bx bx-x"></i>
                </button>
            </div>
            <div class="modal-body">
                Подтвердите вывод 12 BCH для Matrin Blank на кошелёк 123yhm8gj879ergdf73g
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                    <i class="bx bx-x d-block d-sm-none"></i>
                    <span class="d-none d-sm-block">Отмена</span>
                </button>
                <button type="button" class="btn btn-primary ml-1 btn-confirm-pay">
                    <i class="bx bx-check d-block d-sm-none"></i>
                    <span class="d-none d-sm-block">Подтвердить</span>
                </button>
            </div>
        </div>
    </div>
</div>
