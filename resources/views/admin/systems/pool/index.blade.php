@extends('admin.layouts.baseLayout')
{{-- page Title --}}
@section('title','Настройка системы')
{{-- vendor css --}}
@section('vendor-styles')
@endsection

@section('page-styles')
@endsection

@section('content')
    <!-- Dashboard Ecommerce Starts -->
    <section id="nav-filled">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    @include('admin.component.card-header', [
                       'before' => 'Edit',
                       'title' => 'System settings',
                       'after' => '',
                    ])
                    <div class="card-content">
                        <div class="card-body">
                            <!--Таблица-->
                            <div class="custom-control custom-switch custom-control-inline mb-1">
                                <input type="checkbox" class="custom-control-input" {{(!empty($settings['auto_mode']) && $settings['auto_mode'] == '1') ? 'checked' : ''}} id="customSwitch1">
                                <label class="custom-control-label mr-1" for="customSwitch1">
                                </label>
                                <span>Автоматический режим</span>
                            </div>
                            <br>

                            <div class="table-responsive">
                                <table class="table">
                                    @include('admin.component.table.thead',
                                        ['names'=>$formData['pool-head-table']]
                                    )
                                    <tbody>
                                        @foreach($pools as $pool)
                                            <tr>
                                                <td class="text-bold-500">{{ strtoupper($pool->coin->name) }}</td>
                                                <td>
                                                    <div class="input-group">
                                                        {{ $pool->revenue }}
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="input-group">
                                                        {{ $pool->pool_hashrate }}
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="input-group">
                                                        {{ $pool->network }}
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="input-group">
                                                        {{ $pool->minimum_payment }} {{ strtoupper($pool->coin->name) }}
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="input-group">
                                                        {{ $pool->earning_mode }}
                                                    </div>
                                                </td>
                                                <td>
                                                    <a href="{{ route('admin.systems.pool.edit', $pool->id) }}">
                                                        <i class="bx bx-edit-alt"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Dashboard Ecommerce ends -->
@endsection

@section('vendor-scripts')
@endsection

@section('page-scripts')
    <script src="{{asset('js/scripts/navs/navs.js')}}"></script>

    <script>
        $('#save-table').on('click', function(){
            $.ajax({
                method: 'POST',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: '/admin/setting/save-table',
                data: {
                    'auto_mode' : ($('#customSwitch1').prop('checked')) ? 1 : 0,
                    'btc': {
                        'revenue' : $('#btc-revenue').val(),
                        'pull_hashrate' : $('#btc-pool-hashrate').val(),
                        'network' : $('#btc-network').val(),
                        'minimum_payment' : $('#btc-minimum-payment').val(),
                        'earning_mode' : $('#btc-earning-mode').val(),
                        'auto_mode': ($('#btc-auto-mode').prop('checked')) ? 1 : 0
                    },
                    'bch': {
                        'revenue' : $('#bch-revenue').val(),
                        'pull_hashrate' : $('#bch-pool-hashrate').val(),
                        'network' : $('#bch-network').val(),
                        'minimum_payment' : $('#bch-minimum-payment').val(),
                        'earning_mode' : $('#bch-earning-mode').val(),
                        'auto_mode': ($('#bch-auto-mode').prop('checked')) ? 1 : 0
                    },
                    'ltc': {
                        'revenue' : $('#ltc-revenue').val(),
                        'pull_hashrate' : $('#ltc-pool-hashrate').val(),
                        'network' : $('#ltc-network').val(),
                        'minimum_payment' : $('#ltc-minimum-payment').val(),
                        'earning_mode' : $('#ltc-earning-mode').val(),
                        'auto_mode': ($('#ltc-auto-mode').prop('checked')) ? 1 : 0
                    },
                    'eth': {
                        'revenue' : $('#eth-revenue').val(),
                        'pull_hashrate' : $('#eth-pool-hashrate').val(),
                        'network' : $('#eth-network').val(),
                        'minimum_payment' : $('#eth-minimum-payment').val(),
                        'earning_mode' : $('#eth-earning-mode').val(),
                        'auto_mode': ($('#eth-auto-mode').prop('checked')) ? 1 : 0
                    },
                    'zec': {
                        'revenue' : $('#zec-revenue').val(),
                        'pull_hashrate' : $('#zec-pool-hashrate').val(),
                        'network' : $('#zec-network').val(),
                        'minimum_payment' : $('#zec-minimum-payment').val(),
                        'earning_mode' : $('#zec-earning-mode').val(),
                        'auto_mode': ($('#zec-auto-mode').prop('checked')) ? 1 : 0
                    }
                },
                success: function(data) {
                    if (data == '200') {
                        toastr.success('Таблица', 'Ваши данные успешно сохранены!');
                    }
                }
            });
        });

        $('#save-wallets').on('click', function(){
            $.ajax({
                method: 'POST',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: '/admin/setting/save-wallets',
                data: {
                    'btc': $('#btc-wallet').val(),
                    'bch': $('#bch-wallet').val(),
                    'ltc': $('#ltc-wallet').val(),
                    'eth': $('#eth-wallet').val(),
                    'zec': $('#zec-wallet').val()
                },
                success: function(data) {
                    if (data == '200') {
                        toastr.success('Кошельки', 'Ваши данные успешно сохранены!');
                    }
                }
            })
        });

        $('#save-referral').on('click', function(){
            $.ajax({
                method: 'POST',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: '/admin/setting/save-referral',
                data: {
                    'percent1': $('#firstLevelPercent').val(),
                    'percent2': $('#secondLevelPercent').val(),
                    'percent3': $('#thirdLevelPercent').val(),
                    'conversion2' : $('#secondLevelConversion').val(),
                    'conversion3' : $('#thirdLevelConversion').val()
                },
                success: function(data) {
                    if (data == '200') {
                        toastr.success('Реферальная программа', 'Ваши данные успешно сохранены!');
                    }
                }
            });
        });

        $('#save-telegram-account').on('click', function(){
            $.ajax({
                method: 'POST',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: '/admin/setting/save-telegram',
                data: {
                    'telegram': $('#telegram-account').val()
                },
                success: function(data) {
                    if (data == '200') {
                        toastr.success('Обратная связь', 'Ваши данные успешно сохранены!');
                    }
                }
            });
        });

        $('#save-img').on('click', function(){
            $.ajax({
                method: 'POST',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: '/admin/setting/save-image',
                data: $('#inputGroupFile01').files,
                processData: false,
                contentType : false,
                success: function(data) {
                    $('.logo-img').attr('src', data);
                    toastr.success('Логотип', 'Ваши данные успешно сохранены!');
                }
            })
        });

        function creatingAdmin()
        {
            $.ajax({
                method: 'POST',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: '/admin/setting/add-admin',
                data: {
                    'name' : $('#nameInput').val(),
                    'email' : $('#emailInput').val(),
                    'password': $('#passwordInput').val(),
                    'page_content': ($('#page-content').prop('checked')) ? 1 : 0,
                    'page_users': ($('#page-users').prop('checked')) ? 1 : 0,
                    'page_pays': ($('#page-pays').prop('checked')) ? 1 : 0,
                    'page_settings': ($('#page-settings').prop('checked')) ? 1 : 0,
                },
                success: function(data) {
                    if (data = '200') {
                        $('#small').modal('hide');
                        toastr.success('Администраторы и права', 'Админ успешно добавлен!');
                    }
                }
            })
        }

        function getAdmin(id)
        {
            $.ajax({
                method: 'POST',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: '/admin/setting/get-admin',
                dataType: 'json',
                data: {'id' : id},
                success: function(data) {
                    $('#nameInput').val(data.name);
                    $('#emailInput').val(data.email);
                    $('#page-content').attr('checked', (data.page_content == '1') ? true : false);
                    $('#page-users').attr('checked', (data.page_users == '1') ? true : false);
                    $('#page-pays').attr('checked', (data.page_pays == '1') ? true : false);
                    $('#page-settings').attr('checked', (data.page_settings == '1') ? true : false);
                    $('#small .btn-primary').attr('onclick', 'saveDataAdmin(' + id + ');');
                }
            })
        }

        function saveDataAdmin(id)
        {
            $.ajax({
                method: 'POST',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: '/admin/setting/edit-admin',
                data: {
                    'id' : id,
                    'name' : $('#nameInput').val(),
                    'email' : $('#emailInput').val(),
                    'password': $('#passwordInput').val(),
                    'page_content': ($('#page-content').prop('checked')) ? 1 : 0,
                    'page_users': ($('#page-users').prop('checked')) ? 1 : 0,
                    'page_pays': ($('#page-pays').prop('checked')) ? 1 : 0,
                    'page_settings': ($('#page-settings').prop('checked')) ? 1 : 0,
                },
                success: function(data) {
                    if (data = '200') {
                        $('#small').modal('hide');
                        toastr.success('Администраторы и права', 'Данные администратора успешно сохранены!');
                    }
                }
            })
        }

        function deleteAdmin(id)
        {
            var isDelete = confirm("Вы уверены, что хотите удалить этого администратора?");

            if (isDelete) {
                $.ajax({
                    method: 'POST',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: '/admin/setting/delete-admin',
                    data: {'id': id},
                    success: function(data) {
                        if (data = '200') {
                            toastr.success('Администраторы и права', 'Администратор удалён!');
                        }
                    }
                });
            }
        }
    </script>
@endsection
