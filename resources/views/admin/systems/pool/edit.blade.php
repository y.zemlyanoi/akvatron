@extends('admin.layouts.baseLayout')
{{-- page Title --}}
@section('title','Настройка пула')
{{-- vendor css --}}
@section('vendor-styles')
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/extensions/toastr.css')}}">
@endsection

@section('page-styles')
    <link rel="stylesheet" type="text/css" href="{{asset('css/plugins/extensions/toastr.css')}}">
@endsection

@section('content')
    <!-- Dashboard Ecommerce Starts -->
    <section id="nav-filled">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <form action="{{ route('admin.systems.pool.save', $pool->id) }}" method="POST">
                        @csrf
                        @include('admin.component.card-header', [
                            'before' => 'Edit',
                            'title' => 'Pool for',
                            'after' => $pool->coin->name
                           ])
                        <div class="card-content">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        @include('admin.component.table.thead',
                                            ['names'=>$formData['pool-head-table-edit']]
                                        )
                                        <tbody>
                                        <tr>
                                            <td class="text-bold-500">Coin</td>
                                            <td class="text-bold-500">{{ strtoupper($pool->coin->name) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Revenue</td>
                                            <td>
                                                <input id="{{ $pool->id }}-revenue" type="text" class="form-control" name="revenue"  value="{{ $pool->revenue }}">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Pool Hashrate</td>
                                            <td>
                                                <input id="{{ $pool->id }}-pool-hashrate" type="text" class="form-control" name="pool_hashrate" value="{{ $pool->pool_hashrate }}">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Network</td>
                                            <td>
                                                <input id="{{ $pool->id }}-network" type="text" class="form-control" name="network" value="{{ $pool->network }}">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Minimum Payment</td>
                                            <td>
                                                <input id="{{ $pool->id }}-minimum_payment" type="text" class="form-control" name="minimum_payment" value="{{ $pool->minimum_payment }}">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                {{ $pool->earning_mode }}
                                            </td>
                                            <td>
                                                <input id="{{ $pool->id }}-earning-mode" type="text" class="form-control" name="earning_mode" value="{{ $pool->earning_mode }}">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Auto Mode
                                            </td>
                                            <td>
                                                <div class="custom-control custom-switch custom-control-inline mb-1">
                                                    <input id="{{ $pool->id }}-auto-mode" type="checkbox" class="custom-control-input" name="auto_mode" {{ ($pool->auto_mode == '1') ? 'checked' : '' }} />
                                                    <label class="custom-control-label mr-1" for="{{ $pool->id}}-auto-mode">
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <button type="submit" class="btn btn-primary mr-1 mb-1" id="save-table">Сохранить</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- Dashboard Ecommerce ends -->
@endsection

@section('vendor-scripts')
@endsection
