<form action="{{ route('admin.systems.update') }}" method="POST">
    @csrf
    <div class="row">
        <div class="col-6">
            @include('admin.component.form.input', [
                'inputId' => 'site-name',
                'inputLabel' => 'Site name',
                'inputName' => 'site_name',
                'inputPlaceholder' => 'Enter site name',
                'inputValue' => isset($settings['site_name']) ? $settings['site_name'] : ''
            ])
        </div>
    </div>
    <div class="row">
        <div class="col-12 mb-1">
            <label>Site logo</label>
        </div>
        <div class="col-lg-3">
            <img style="max-width:200px"
                 class="logo-img"
                 src="{{ url('storage/images') }}/{{ $settings['logo'] }}"
                 alt="logo" />
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                @include('admin.component.form.select', [ 'select' => array(
                    'id' => 'image-choose',
                    'className' => 'select2',
                    'name' => 'logo',
                    'options' => $imagesOptions)
                ])
            </div>
        </div>
    </div>
    <div class="row mt-2">
        <div class="col-12 text-center">
            <button id="save-img" type="submit" class="btn btn-primary mr-1 mb-1">Сохранить</button>
        </div>
    </div>
</form>
