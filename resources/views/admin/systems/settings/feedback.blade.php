<form action="{{ route('admin.systems.update') }}" method="POST">
    @csrf
    <div class="row">
        <div class="col-lg-2 col-3">
            <label class="col-form-label">Аккаунт в телеграме</label>
        </div>
        <div class="col-lg-10 col-9">
            <input type="text" id="telegram-account" class="form-control" name="telegram" value="{{$settings['telegram']}}">
        </div>
    </div>
    <div class="row justify-content-center">
        <button type="submit" class="btn btn-primary mr-1 mb-1 " id="save-telegram-account">Сохранить</button>
    </div>
</form>
