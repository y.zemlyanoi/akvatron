<tr>
    <td>
        <input type="hidden" name="id[]" value="{{ $referral->id }}">
        <input type="text" class="form-control" name="name[]" value="{{ $referral->name }}">
    </td>
    <td>
        <fieldset class="form-group">
            <input type="text" class="form-control" name="conversion[]" value="{{ $referral->conversion }}">
        </fieldset>
    </td>
    <td>
        <fieldset class="form-group">
            <input type="text" class="form-control" name="percent[]" value="{{ $referral->percent }}">
        </fieldset>
    </td>
</tr>
