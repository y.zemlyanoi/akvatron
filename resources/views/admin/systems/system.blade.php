@extends('admin.layouts.baseLayout')
{{-- page Title --}}
@section('title','Настройка системы')
{{-- vendor css --}}
@section('vendor-styles')
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/forms/select/select2.min.css')}}">
@endsection

@section('page-styles')
@endsection

@section('content')
    <section id="main-system">
        <div class="card">
            @include('admin.component.card-header', [
               'before' => 'Edit',
               'title' => 'System settings',
               'after' => '',
            ])
            <div class="card-content">
                <div class="card-body">
                    @include('admin.component.nav-tab-head', [
                        'tabs' => $formData["nav-tab-system"]
                    ])
                    <div class="tab-content pt-1">
                        <div class="tab-pane active" id="main" role="tabpanel" aria-labelledby="main-tab-fill">
                            @include('admin.systems.settings.main')
                        </div>
                        <div class="tab-pane" id="feedback" role="tabpanel" aria-labelledby="feedback-tab-fill">
                            @include('admin.systems.settings.feedback')
                        </div>
                        <div class="tab-pane" id="referral" role="tabpanel" aria-labelledby="referral-tab-fill">
                            <!--Реф. программа-->
                            <div class="table-responsive">
                                <form action="{{ route('admin.referrals.update') }}" method="POST">
                                    @csrf
                                    <table class="table">
                                        @include('admin.component.table.thead',
                                            ['names'=>$formData['referral-head-table']]
                                        )
                                        <tbody>
                                        @foreach($referrals as $referral)
                                            @include('admin.systems.settings.referrals', ['referral'=>$referral])
                                        @endforeach
                                        </tbody>
                                    </table>
                                    <div class="col-12 text-center">
                                        <button id="save-referral" type="submit" class="btn btn-primary mr-1 mb-1" >Сохранить</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Dashboard Ecommerce ends -->
@endsection

@section('vendor-scripts')
    <script src="{{asset('vendors/js/forms/select/select2.full.min.js')}}"></script>
@endsection

@section('page-scripts')
    <script src="{{asset('js/scripts/forms/select/form-select2.js')}}"></script>
@endsection
