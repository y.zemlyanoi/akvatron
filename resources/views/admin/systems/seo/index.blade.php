@extends('admin.layouts.baseLayout')
{{-- page Title --}}
@section('title','Настройка системы')
{{-- vendor css --}}
@section('vendor-styles')
@endsection

@section('page-styles')
@endsection

@section('content')
    <section id="seo-block">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    @include('admin.component.card-header', [
                       'before' => '',
                       'title' => 'SEO',
                       'after' => '',
                    ])
                    <div class="card-content">
                        <div class="card-body">
                            @include('admin.component.nav-tab-head', [
                                'tabs' => $formData["nav-tab-seo"]
                            ])
                            <div class="tab-content pt-1">
                                <div class="tab-pane active" id="seo" role="tabpanel" aria-labelledby="seo-tab-fill">
                                    1
                                </div>
                                <div class="tab-pane" id="gtm" role="tabpanel" aria-labelledby="gtm-tab-fill">
                                    2
                                </div>
                                <div class="tab-pane" id="facebook-pixel" role="tabpanel" aria-labelledby="facebook-pixel-tab-fill">
                                    3
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('vendor-scripts')
@endsection

@section('page-scripts')
@endsection
