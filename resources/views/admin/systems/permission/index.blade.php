@extends('admin.layouts.baseLayout')
{{-- page Title --}}
@section('title','Настройка системы')
{{-- vendor css --}}
@section('vendor-styles')
@endsection

@section('page-styles')
@endsection

@section('content')
    <section id="permission-system">
        <div class="card">
            @include('admin.component.card-header', [
               'before' => 'Edit',
               'title' => 'System',
               'after' => 'Permission',
            ])
            <div class="card-content">
                <div class="card-body">
                    @include('admin.payouts.component.nav-tab-head', [
                        'tabs' => $formData["nav-tab-permission"]
                    ])
                    <div class="tab-content">
                        <!-- Админы -->
                        <div class="tab-pane active" id="admins" aria-labelledby="admins-tab" role="tabpanel">
                            <div class="row">
                                <button type="button" class="btn btn-success mr-1 mb-1" onclick="document.location.href='{{ route("admin.systems.permission.create") }}'">
                                    <i class="bx bx-plus-medical"></i> Add Administrator
                                </button>
                                <div class="table-responsive">
                                    <table class="table">
                                        @include('admin.component.table.thead',
                                            ['names'=>$formData['permissions-head-table']]
                                        )
                                        <tbody>
                                        @foreach($admins as $admin)
                                            <tr>
                                                <td>{{$admin->name}}</td>
                                                <td>{{$admin->email}}</td>
                                                <td>

                                                </td>
                                                <td>
                                                    <a href="#" class="text-warning" onclick="getAdmin({{$admin->id}})" data-toggle="modal" data-target="#small"><i class="bx bxs-pencil"></i></a>
                                                    @if(Auth::id() != $admin->id)
                                                        <a href="#" class="text-danger" onclick="deleteAdmin({{$admin->id}})"><i class="bx bxs-trash-alt"></i></a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- Роли -->
                        <div class="tab-pane" id="roles" aria-labelledby="roles-tab" role="tabpanel">
                            <div class="row">
                                <button type="button" class="btn btn-success mr-1 mb-1">
                                    <i class="bx bx-plus-medical"></i> Add Role
                                </button>
                                <div class="table-responsive">
                                    <table class="table">
                                        @include('admin.component.table.thead',
                                            ['names'=>$formData['roles-head-table']]
                                        )
                                        <tbody>
                                        @if(count($roles) == 0)
                                            <tr><td colspan="5" align="center">No result</td></tr>
                                        @endif
                                        @foreach($roles as $role)
                                            <tr>
                                                <td>{{$role->name}}</td>
                                                <td>
                                                    @foreach($role->permissions as $permission)
                                                        {{ $permission->name }},
                                                    @endforeach
                                                </td>
                                                <td>
                                                    <a href="#" class="text-warning" onclick=""><i class="bx bxs-pencil"></i></a>
                                                    <a href="#" class="text-danger" onclick="" ><i class="bx bxs-trash-alt"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- Доступы-->
                        <div class="tab-pane" id="permission" aria-labelledby="permission-tab" role="tabpanel">
                            <div class="row">
                                <div class="col-lg-12">
                                    <button type="button" class="btn btn-success mr-1 mb-1">
                                        <i class="bx bx-plus-medical"></i> Add Permission
                                    </button>
                                    <div class="list-group">
                                        @foreach($permissions as $permission)
                                            <span class="list-group-item">{{ $permission->name }}</span>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('vendor-scripts')
@endsection
