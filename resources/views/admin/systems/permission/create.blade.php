@extends('admin.layouts.baseLayout')

{{-- page Title --}}
@section('title','Управление наполнением сайта')

@section('vendor-styles')
@endsection

@section('content')
    <section class="users-list-wrapper">
        <div class="users-list-table">
            <div class="card">
                @include('admin.component.card-header', [
                    'before' => 'Create',
                    'title' => 'New',
                    'after' => 'Admin',
                ])
                <div class="card-content">
                    <div class="card-body">
                        <form class="form form-vertical" action="{{ route('admin.create.page') }}" method="post">
                            @csrf
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-12">
                                        @include('admin.component.form.input', [
                                            'inputId' => 'admin-name',
                                            'inputLabel' => 'Admin name',
                                            'inputName' => 'name',
                                            'inputPlaceholder' => 'Enter admin name',
                                            'inputValue' => old('name', '')
                                        ])
                                        @include('admin.component.form.input', [
                                           'inputId' => 'admin-email',
                                           'inputLabel' => 'Email',
                                           'inputName' => 'email',
                                           'inputPlaceholder' => 'Enter admin email',
                                           'inputValue' => old('email', '')
                                        ])
                                        @include('admin.component.form.input', [
                                           'inputId' => 'admin-password',
                                           'inputLabel' => 'Password',
                                           'inputName' => 'password',
                                           'inputPlaceholder' => 'Enter admin password',
                                           'inputValue' => old('password', '')
                                        ])
                                    </div>

                                    <div class="col-12 d-flex mt-1 justify-content-end">
                                        <button type="submit" class="btn btn-primary mb-1">Save</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

{{-- vendor scripts --}}
@section('vendor-scripts')
@endsection

{{-- page scripts --}}
@section('page-scripts')
@endsection
