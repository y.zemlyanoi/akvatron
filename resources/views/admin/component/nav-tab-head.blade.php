<ul class="nav nav-tabs nav-fill" role="tablist">
    @foreach($tabs as $tab)
        <li class="nav-item @if($tab['current']) current @endif">
            <a class="nav-link @if($tab['current']) active @endif" id="{{ $tab['id'] }}-tab" data-toggle="tab" href="#{{ $tab['id'] }}" aria-controls="{{ $tab['id'] }}" role="tab" aria-selected="false">
                <span class="align-middle">
                    {{ $tab['name'] }}
                </span>
                @if(isset($tab['tag']))
                    <span class="tag-{{ $tab['tag'] }} badge badge-light-danger badge-pill badge-round float-right mr-2" style="position: relative; left: 10px;">
                    </span>
                @endif
            </a>
        </li>
    @endforeach
</ul>
