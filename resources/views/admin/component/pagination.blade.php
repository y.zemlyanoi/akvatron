@if ($paginator->hasPages())
<nav aria-label="Page navigation">
    <ul class="pagination pagination-borderless justify-content-center mt-2" data-current="{{ $paginator->currentPage() }}">
        @if ($paginator->onFirstPage())
{{--            <li class="page-item disabled"><span>&laquo;</span></li>--}}
        @else
            <li class="page-item previous">
                <a class="page-link" href="{{ $paginator->previousPageUrl() }}" data-page="{{ $paginator->currentPage()-1 }}" rel="prev">
                    <i class="bx bx-chevron-left"></i>
                </a>
            </li>
        @endif
        @foreach ($elements as $element)
            @if (is_string($element))
                <li class="page-item disabled"><span>{{ $element }}</span></li>
            @endif
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="page-item active" aria-current="page">
                            <a class="page-link">{{ $page }}</a>
                        </li>
                    @else
                        <li class="page-item">
                            <a class="page-link" href="{{ $url }}" data-page="{{ $page }}">
                                {{ $page }}
                            </a>
                        </li>
                    @endif
                @endforeach
            @endif
        @endforeach

        @if ($paginator->hasMorePages())
            <li class="page-item next">
                <a class="page-link" href="{{ $paginator->nextPageUrl() }}" data-page="{{ $paginator->currentPage()+1 }}" rel="next">
                    <i class="bx bx-chevron-right"></i>
                </a>
            </li>
        @else
{{--            <li class="disabled"><span>&raquo;</span></li>--}}
        @endif

    </ul>
</nav>
@endif
