<li class="dropdown dropdown-user nav-item">
    <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
        <div class="user-nav d-sm-flex d-none">
            <span class="user-name">{{ Auth::user()->name }}</span>
            <span class="user-status text-muted">Online</span>
        </div>
        <span><img class="round" src="{{asset('images/portrait/small/avatar-s-11.jpg')}}" alt="avatar" height="40" width="40"></span>
    </a>
    <div class="dropdown-menu dropdown-menu-right pb-0">
        <a class="dropdown-item" href="{{asset('page-user-profile')}}">
            <i class="bx bx-user mr-50"></i> Edit Profile
        </a>
        <div class="dropdown-divider mb-0"></div>
        <a class="dropdown-item" href="{{ route('admin.logout') }}"><i class="bx bx-power-off mr-50"></i> Logout</a>
    </div>
</li>
