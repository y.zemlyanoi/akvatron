<div class="form-group">
    <div class="checkbox">
        <input type="checkbox" class="checkbox-input" id="{{ $checkboxId }}" name="{{ $checkboxName }}" @if($checked) checked @endif />
        <label for="{{ $checkboxId }}">{{ $checkboxLable }}</label>
    </div>
</div>
