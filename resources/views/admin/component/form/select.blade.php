<select
    id="{{ $select['id'] }}"
    class="form-control {{ $select['className'] }}"
    name="{{ $select['name'] }}"
    data-type="{{ $select["options"][0]["value"] }}">
    @include('admin.component.form.option', ['options' => $select['options']])
</select>
