<div class="form-group">
    <label for="{{ $inputId }}">{{ $inputLabel }}</label>
    <input type="text"
           id="{{ $inputId }}"
           class="form-control"
           name="{{ $inputName }}"
           value="{{ $inputValue }}"
           placeholder="{{ $inputPlaceholder }}">
</div>
