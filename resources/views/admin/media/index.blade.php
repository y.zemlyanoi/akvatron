@extends('admin.layouts.baseLayout')

{{-- page Title --}}
@section('title','Все страницы')

@section('content')
    <section class="users-list-wrapper">
        <div class="users-list-table">
            <div class="card">
                @include('admin.component.card-header', [
                   'before' => 'All',
                   'title' => 'Images',
                   'after' => '',
                ])
                <div class="card-content">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-8">
                                @include('admin.media.component.form', [
                                    'action' => route('api.upload.image')
                                ])
                            </div>
                            <div class="col-lg-4">
                                <button type="submit" class="btn btn-danger delete-images mb-1 float-right">Delete</button>
                            </div>
                        </div>

                        <div class="row app-file-files">
                            @foreach($images as $image)
                                @include('admin.media.component.image', [
                                    'imageId' => $image->id,
                                    'imageAlt' => $image->description,
                                    'imageSlug' => $image->slug,
                                    'imageTitle' => $image->name,
                                ])
                            @endforeach
                        </div>

                        {{$images->links('admin.component.pagination')}}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

{{-- vendor scripts --}}
@section('vendor-scripts')
@endsection

{{-- page scripts --}}
@section('page-scripts')
    <script src="{{asset('js/admin/custom/media.js')}}"></script>
@endsection
