<form id="images-uploader" method="POST" action="{{ $action }}"
      class="images-upload"
      accept-charset="UTF-8"
      enctype="multipart/form-data">
    <div class="row">
        @csrf
        <div class="col-lg-8 col-md-12">
            <fieldset class="form-group">
                <div class="custom-file">
                    <input id="upload-image" type="file" class="custom-file-input" name="images" />
                    <label class="custom-file-label" for="upload-image">Choose file</label>
                </div>
            </fieldset>
        </div>
        <div class="col-lg-4 col-md-12">
            <button type="submit" class="btn btn-primary mb-1">Upload</button>
        </div>
    </div>
</form>
