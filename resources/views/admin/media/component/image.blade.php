<div id="image-block-{{ $imageId }}" class="col-md-3 col-6">
    <div class="card border shadow-none mb-1 app-file-info">
        <div class="card-content">
            <div class="card-body p-50">
                <div class="app-file-details">
                    <div class="app-file-name font-size-small font-weight-bold">
                        {{ $imageTitle }}
                        <input type="checkbox"
                               id="image-{{ $imageId }}"
                               class="checkbox-input float-right"
                               name="images[]" value="{{ $imageId }}" />
                    </div>
                </div>
            </div>
            <div class="app-file-content-logo card-img-top">
                @include('admin.component.image', [
                    'class' => 'd-block mx-auto w-100',
                    'src' => asset('storage/images/'.$imageSlug),
                    'alt' => $imageAlt,
                ])
            </div>
            <div class="card-body p-50">
                <div class="app-file-details">
                    <div class="app-file-name font-size-small font-weight-bold">{{ $imageSlug }}</div>
                </div>
            </div>
        </div>
    </div>
</div>
