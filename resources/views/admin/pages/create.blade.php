@extends('admin.layouts.baseLayout')

{{-- page Title --}}
@section('title','Управление наполнением сайта')

@section('vendor-styles')
@endsection

@section('content')
    <section class="users-list-wrapper">
        <div class="users-list-table">
            <div class="card">
                @include('admin.component.card-header', [
                    'before' => 'Create',
                    'title' => 'New',
                    'after' => 'Page',
                ])
                <div class="card-content">
                    <div class="card-body">
                        <form class="form form-vertical" action="{{ route('admin.create.page') }}" method="post">
                            @csrf
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-12">
                                        @include('admin.component.form.input', [
                                            'inputId' => 'page-name',
                                            'inputLabel' => 'Page name',
                                            'inputName' => 'name',
                                            'inputPlaceholder' => 'Enter page name',
                                            'inputValue' => old('name', '')
                                        ])
                                        @include('admin.component.form.input', [
                                            'inputId' => 'page-h1',
                                            'inputLabel' => 'H1',
                                            'inputName' => 'H1',
                                            'inputPlaceholder' => 'Enter title name',
                                            'inputValue' => old('H1', '')
                                        ])
                                        @include('admin.component.form.input', [
                                           'inputId' => 'page-slug',
                                           'inputLabel' => 'Page url',
                                           'inputName' => 'slug',
                                           'inputPlaceholder' => 'Enter page slug',
                                           'inputValue' => old('slug', '')
                                        ])
                                    </div>

                                    <div class="col-sm-12">
                                        <label for="full-wrapper">Page content annotation</label>
                                        <textarea id="editor-annotation" name="annotation">
                                            {{ old('annotation', '') }}
                                        </textarea>
                                    </div>

                                    <div class="col-sm-12">
                                        <label for="full-wrapper">Page content description</label>
                                        <textarea id="editor-description" name="description">
                                            {{ old('description', '') }}
                                        </textarea>
                                    </div>

                                    <div class="col-12 d-flex mt-1 justify-content-center">
                                        <button type="submit" class="btn btn-primary mb-1">Save</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

{{-- vendor scripts --}}
@section('vendor-scripts')
    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
@endsection

{{-- page scripts --}}
@section('page-scripts')
    <script>
        $(document).ready(function(){
            CKEDITOR.replace( 'editor-annotation' );
            CKEDITOR.replace( 'editor-description' );
        });
    </script>
@endsection
