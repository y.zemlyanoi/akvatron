<div id="image-{{ $imageId }}" class="col-md-4 col-6">
    <div class="card border shadow-none mb-1 app-file-info">
        <div class="card-content">
            <div class="card-body p-50">
                <div class="app-file-details">
                    <div class="app-file-name font-size-small font-weight-bold">
                        {{ $imageTitle }}
                        <a data-id="{{ $imageId }}"
                           href="{{ route('api.detach.image', $imageId) }}"
                           class="delete-image float-right">
                            <i class="bx bx-trash danger"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="app-file-content-logo card-img-top">
                @include('admin.component.image', [
                    'class' => 'd-block mx-auto w-100',
                    'src' => asset('storage/images/'.$imageSlug),
                    'alt' => $imageAlt,
                ])
            </div>
            <div class="card-body p-50">
                <div class="app-file-details">
                    <div class="app-file-name font-size-small font-weight-bold">{{ $imageSlug }}</div>
                </div>
            </div>
        </div>
    </div>
</div>
