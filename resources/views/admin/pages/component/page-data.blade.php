<tr>
    <td>{{ $page->id }}</td>
    <td>
        <a href="{{ route('admin.edit.page', $page->id) }}">
            {{ $page->name }}
        </a>
    </td>
    <td>{{ $page->slug }}</td>
    <td>
        <span class="badge @if($page->active) badge-light-success @endif">
            {{ $page->status() }}
        </span>
    </td>
    <td>
        <span class="badge @if($page->main) badge-light-success @endif">
            {{ $page->main() }}
        </span>
    </td>
    <td>
        <a href="{{ route('admin.edit.page', $page->id) }}">
            <i class="bx bx-edit-alt"></i>
        </a>
        <a href="{{ route('admin.delete.page', $page->id) }}">
            <i class="bx bx-trash danger"></i>
        </a>
    </td>
</tr>
