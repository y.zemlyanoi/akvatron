<div class="col-6">
    <div class="row">
        <div class="col-12">
            <h6>Select images for this page</h6>
            <p>Type <code>image name</code> for selecting.</p>
        </div>
        <div class="col-10">
            <div class="form-group">
                @include('admin.component.form.select', [ 'select' => array(
                    'id' => 'image-choose',
                    'className' => 'select2',
                    'name' => 'page_images',
                    'options' => $imagesOptions)
                ])
            </div>
        </div>
        <div class="col-2">
            <button data-url="{{ route('api.attach.image', $page->id) }}" type="button" class="add-image btn btn-primary">+</button>
        </div>
    </div>
</div>
