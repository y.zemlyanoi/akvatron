@extends('admin.layouts.baseLayout')

{{-- page Title --}}
@section('title','Все страницы')

@section('content')
    <section class="users-list-wrapper">
        <div class="users-list-table">
            <div class="card">
                @include('admin.component.card-header', [
                   'before' => 'All',
                   'title' => 'Pages',
                   'after' => '',
               ])
                <div class="card-content">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <button type="button" onclick="location.href='{{ route('admin.create.page') }}'" class="btn btn-outline-primary mr-1 mb-1 float-right">
                                    <i class="bx bx-plus-circle"></i>
                                    <span class="align-middle ml-25">Add</span>
                                </button>
                            </div>
                        </div>
                        <!-- datatable start -->
                        <div class="table-responsive">
                            <table id="users-list-datatable" class="table">
                                @include('admin.component.table.thead',
                                    ['names'=> $formData["pages-head"]]
                                )
                                <tbody>
                                    @foreach($pages as $page)
                                        @include('admin.pages.component.page-data', $page)
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- datatable ends -->
                        <div id="pages-pagination">
                            {{$pages->links('admin.component.pagination')}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

{{-- vendor scripts --}}
@section('vendor-scripts')
@endsection

{{-- page scripts --}}
@section('page-scripts')
@endsection
