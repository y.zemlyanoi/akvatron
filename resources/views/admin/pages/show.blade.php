@extends('admin.layouts.baseLayout')

{{-- page Title --}}
@section('title','Управление наполнением сайта')

@section('vendor-styles')
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/editors/quill/katex.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/editors/quill/monokai-sublime.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/editors/quill/quill.snow.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/editors/quill/quill.bubble.css')}}">
@endsection

@section('content')
    <section class="users-list-wrapper">
        <div class="users-list-table">
            <div class="card">
                @include('admin.component.card-header', [
                    'before' => 'Edit',
                    'title' => $page->name,
                    'after' => '',
                ])
                <div class="card-content">
                    <div class="card-body">
                        <form class="form form-vertical" action="{{ route('admin.put.page', $page->id) }}" method="post">
                            {{ method_field('PUT') }}
                            @csrf
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-12">
                                        @include('admin.component.form.input', [
                                            'inputId' => 'page-name',
                                            'inputLabel' => 'Page name',
                                            'inputName' => 'name',
                                            'inputPlaceholder' => 'Enter page name',
                                            'inputValue' => $page->name
                                        ])
                                        @include('admin.component.form.input', [
                                           'inputId' => 'page-slug',
                                           'inputLabel' => 'Page url',
                                           'inputName' => 'slug',
                                           'inputPlaceholder' => 'Enter page slug',
                                           'inputValue' => $page->slug
                                        ])
                                    </div>
                                    <div class="col-12">
                                        @include('admin.component.form.checkbox',[
                                           'checkboxId' => 'page-save-url',
                                           'checkboxLable' => 'Save url',
                                           'checkboxName' => 'save_url',
                                        ])
                                    </div>

                                    <div class="col-sm-12">
                                        <input type="hidden" name="annotation" value="" />
                                        <label for="full-wrapper">Page content annotation</label>
                                        <div id="full-wrapper">
                                            <div id="full-container">
                                                <div class="editor" style="height:200px">
                                                    {{ $page->annotation }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <input type="hidden" name="description" value="" />
                                        <label for="full-wrapper">Page content description</label>
                                        <div id="full-wrapper">
                                            <div id="full-container">
                                                <div class="editor" style="height:200px">
                                                    {{ $page->description }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12 d-flex mt-1 justify-content-center">
                                        <button type="submit" class="btn btn-primary mb-1">Save</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

{{-- vendor scripts --}}
@section('vendor-scripts')
{{--    <script src="{{asset('vendors/js/editors/quill/katex.min.js')}}"></script>--}}
{{--    <script src="{{asset('vendors/js/editors/quill/highlight.min.js')}}"></script>--}}
{{--    <script src="{{asset('vendors/js/editors/quill/quill.min.js')}}"></script>--}}
@endsection

{{-- page scripts --}}
@section('page-scripts')
{{--    <script src="{{asset('js/admin/editor-quill.js')}}"></script>--}}
{{--    <script src="{{asset('js/admin/editor-quill-submit.js')}}"></script>--}}
@endsection
