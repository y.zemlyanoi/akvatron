@extends('admin.layouts.baseLayout')

{{-- page Title --}}
@section('title','Управление наполнением сайта')

@section('vendor-styles')
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/forms/select/select2.min.css')}}">
@endsection

@section('content')
    <section class="users-list-wrapper">
        <div class="users-list-table">
            <div class="card">
                @include('admin.component.card-header', [
                    'before' => 'Edit',
                    'title' => $page->name,
                    'after' => '',
                ])
                <div class="card-content">
                    <div class="card-body">
                        <form class="form form-vertical" action="{{ route('admin.update.page', $page->id) }}" method="post">
                            {{ method_field('PUT') }}
                            @csrf
                            <input name="id" type="hidden" value="{{ $page->id }}" />
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-6">
                                        @include('admin.component.form.input', [
                                            'inputId' => 'page-name',
                                            'inputLabel' => 'Page name',
                                            'inputName' => 'name',
                                            'inputPlaceholder' => 'Enter page name',
                                            'inputValue' => $page->name
                                        ])
                                    </div>
                                    <div class="col-2 pt-2">
                                        @include('admin.component.form.checkbox',[
                                          'checkboxId' => 'page-main',
                                          'checkboxLable' => 'Main page',
                                          'checkboxName' => 'page_main',
                                          'checked' => 1
                                       ])
                                    </div>
                                    <div class="col-2 pt-2">
                                        @include('admin.component.form.checkbox',[
                                           'checkboxId' => 'page-save-url',
                                           'checkboxLable' => 'Save url',
                                           'checkboxName' => 'save_url',
                                           'checked' => 0
                                        ])
                                    </div>
                                    <!-- Position -->
                                    <div class="col-2">
                                        @include('admin.component.form.input', [
                                           'inputId' => 'page-position',
                                           'inputLabel' => 'Page position',
                                           'inputName' => 'position',
                                           'inputPlaceholder' => 'Enter page position',
                                           'inputValue' => $page->position
                                        ])
                                    </div>
                                    <!-- String for url -->
                                    <div class="col-6">
                                        @include('admin.component.form.input', [
                                           'inputId' => 'page-slug',
                                           'inputLabel' => 'Page url',
                                           'inputName' => 'slug',
                                           'inputPlaceholder' => 'Enter page slug',
                                           'inputValue' => $page->slug
                                        ])
                                    </div>

                                    <!-- String for h1 tag -->
                                    <div class="col-6">
                                        @include('admin.component.form.input', [
                                            'inputId' => 'page-h1',
                                            'inputLabel' => 'H1',
                                            'inputName' => 'H1',
                                            'inputPlaceholder' => 'Enter title name',
                                            'inputValue' => $page->H1
                                        ])
                                    </div>


                                    <!-- Choose image from list -->
                                    @if(!empty($imagesOptions))
                                        @include('admin.pages.component.images-select', $imagesOptions)
                                    @endif

                                    <!-- Images list -->
                                    <div class="col-md-6">
                                        <div id="images-list" class="row">
                                            @if(empty($page->images))
                                                <div>No images</div>
                                            @else
                                                @foreach($page->images as $image)
                                                    @include('admin.pages.component.image', [
                                                        'imageId' => $image->id,
                                                        'imageAlt' => $image->description,
                                                        'imageSlug' => $image->slug,
                                                        'imageTitle' => $image->name,
                                                    ])
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <label for="full-wrapper">Page content annotation</label>
                                        <textarea id="editor-annotation" name="annotation">
                                            {{ $page->annotation }}
                                        </textarea>
                                    </div>

                                    <div class="col-sm-12">
                                        <label for="full-wrapper">Page content description</label>
                                        <textarea id="editor-description" name="description">
                                            {{ $page->description }}
                                        </textarea>
                                    </div>

                                    <div class="col-12 d-flex mt-1 justify-content-center">
                                        <button type="submit" class="btn btn-primary mb-1">Update</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

{{-- vendor scripts --}}
@section('vendor-scripts')
    <script src="{{asset('vendors/js/forms/select/select2.full.min.js')}}"></script>
@endsection

{{-- page scripts --}}
@section('page-scripts')
    <script src="{{asset('js/admin/custom/pages.js')}}"></script>
    <script>
        $(document).ready(function(){
            CKEDITOR.replace( 'editor-annotation' );
            CKEDITOR.replace( 'editor-description' );
        });
    </script>
    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script src="{{asset('js/scripts/forms/select/form-select2.js')}}"></script>
@endsection
