@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','Настройка системы')
{{-- vendor css --}}
@section('vendor-styles')
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/extensions/toastr.css')}}">
@endsection

@section('page-styles')
    <link rel="stylesheet" type="text/css" href="{{asset('css/plugins/extensions/toastr.css')}}">
@endsection

@section('content')
<!-- Dashboard Ecommerce Starts -->
<section id="nav-filled">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Настройка системы</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
                            <li class="nav-item current">
                                <a class="nav-link active" id="table-tab-fill" data-toggle="tab" href="#table-fill" role="tab" aria-controls="home-fill" aria-selected="true">
                                    Таблица
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="admin-tab-fill" data-toggle="tab" href="#admin-fill" role="tab" aria-controls="profile-fill" aria-selected="false">
                                    Админы и доступы
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="wallets-tab-fill" data-toggle="tab" href="#wallets-fill" role="tab" aria-controls="messages-fill" aria-selected="false">
                                    Кошельки
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="referral-tab-fill" data-toggle="tab" href="#referral-fill" role="tab" aria-controls="settings-fill" aria-selected="false">
                                    Реф. программа
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="faq-tab-fill" data-toggle="tab" href="#faq-fill" role="tab" aria-controls="settings-fill" aria-selected="false">
                                    Обр. связь
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="logo-tab-fill" data-toggle="tab" href="#logo-fill" role="tab" aria-controls="settings-fill" aria-selected="false">
                                    Логотип
                                </a>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content pt-1">
                            <div class="tab-pane active" id="table-fill" role="tabpanel" aria-labelledby="table-tab-fill">
                                <!--Таблица-->
                                <div class="custom-control custom-switch custom-control-inline mb-1">
                                    <input type="checkbox" class="custom-control-input" {{($dataSetting['auto_mode'] == '1') ? 'checked' : ''}} id="customSwitch1">
                                    <label class="custom-control-label mr-1" for="customSwitch1">
                                    </label>
                                    <span>Автоматический режим</span>
                                </div>
                                <br>

                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Coin</th>
                                                <th>Revenue</th>
                                                <th>Pool Hashrate</th>
                                                <th>Network</th>
                                                <th>Minimum payment</th>
                                                <th>Earning Mode</th>
                                                <th>Auto Mode</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="text-bold-500">BTC</td>
                                                <td>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="btc-revenue" value="{{$dataPulls['btc']['revenue']}}">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text"> /T</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="btc-pool-hashrate" value="{{$dataPulls['btc']['pull_hashrate']}}">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text"> TH/s</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="btc-network" value="{{$dataPulls['btc']['network']}}">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text"> TH/s</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="btc-minimum-payment" value="{{$dataPulls['btc']['minimum_payment']}}">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text"> BTC</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <fieldset class="form-group" style="padding-top: 14px;">
                                                        <input type="text" class="form-control" id="btc-earning-mode" value="{{$dataPulls['btc']['earning_mode']}}">
                                                    </fieldset>
                                                </td>
                                                <td>
                                                    <div class="custom-control custom-switch custom-control-inline mb-1">
                                                        <input type="checkbox" class="custom-control-input" {{($dataPulls['btc']['auto_mode'] == '1') ? 'checked' : ''}} id="btc-auto-mode">
                                                        <label class="custom-control-label mr-1" for="btc-auto-mode">
                                                        </label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold-500">BCH</td>
                                                <td>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="bch-revenue" value="{{$dataPulls['bch']['revenue']}}">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text"> /T</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="bch-pool-hashrate" value="{{$dataPulls['bch']['pull_hashrate']}}">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text"> TH/s</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="bch-network" value="{{$dataPulls['bch']['network']}}">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text"> TH/s</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="bch-minimum-payment" value="{{$dataPulls['bch']['minimum_payment']}}">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text"> BCH</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <fieldset class="form-group" style="padding-top: 14px;">
                                                        <input type="text" class="form-control" id="bch-earning-mode" value="{{$dataPulls['bch']['earning_mode']}}">
                                                    </fieldset>
                                                </td>
                                                <td>
                                                    <div class="custom-control custom-switch custom-control-inline mb-1">
                                                        <input type="checkbox" class="custom-control-input" {{($dataPulls['bch']['auto_mode'] == '1') ? 'checked' : ''}} id="bch-auto-mode">
                                                        <label class="custom-control-label mr-1" for="bch-auto-mode">
                                                        </label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold-500">LTC</td>
                                                <td>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="ltc-revenue" value="{{$dataPulls['ltc']['revenue']}}">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text"> /T</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="ltc-pool-hashrate" value="{{$dataPulls['ltc']['pull_hashrate']}}">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text"> TH/s</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="ltc-network" value="{{$dataPulls['ltc']['network']}}">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text"> TH/s</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="ltc-minimum-payment" value="{{$dataPulls['ltc']['minimum_payment']}}">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text"> LTC</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <fieldset class="form-group" style="padding-top: 14px;">
                                                        <input type="text" class="form-control" id="ltc-earning-mode" value="{{$dataPulls['ltc']['earning_mode']}}">
                                                    </fieldset>
                                                </td>
                                                <td>
                                                    <div class="custom-control custom-switch custom-control-inline mb-1">
                                                        <input type="checkbox" class="custom-control-input" {{($dataPulls['ltc']['auto_mode'] == '1') ? 'checked' : ''}} id="ltc-auto-mode">
                                                        <label class="custom-control-label mr-1" for="ltc-auto-mode">
                                                        </label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold-500">ETH</td>
                                                <td>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="eth-revenue" value="{{$dataPulls['eth']['revenue']}}">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text"> /T</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="eth-pool-hashrate" value="{{$dataPulls['eth']['pull_hashrate']}}">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text"> TH/s</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="eth-network" value="{{$dataPulls['eth']['network']}}">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text"> TH/s</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="eth-minimum-payment" value="{{$dataPulls['eth']['minimum_payment']}}">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text"> ETH</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <fieldset class="form-group" style="padding-top: 14px;">
                                                        <input type="text" class="form-control" id="eth-earning-mode" value="{{$dataPulls['eth']['earning_mode']}}">
                                                    </fieldset>
                                                </td>
                                                <td>
                                                    <div class="custom-control custom-switch custom-control-inline mb-1">
                                                        <input type="checkbox" class="custom-control-input" {{($dataPulls['eth']['auto_mode'] == '1') ? 'checked' : ''}} id="eth-auto-mode">
                                                        <label class="custom-control-label mr-1" for="eth-auto-mode">
                                                        </label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold-500">ZEC</td>
                                                <td>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="zec-revenue" value="{{$dataPulls['zec']['revenue']}}">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text"> /T</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="zec-pool-hashrate" value="{{$dataPulls['zec']['pull_hashrate']}}">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text"> TH/s</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="zec-network" value="{{$dataPulls['zec']['network']}}">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text"> TH/s</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="zec-minimum-payment" value="{{$dataPulls['zec']['minimum_payment']}}">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text"> ZEC</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <fieldset class="form-group" style="padding-top: 14px;">
                                                        <input type="text" class="form-control" id="zec-earning-mode" value="{{$dataPulls['zec']['earning_mode']}}">
                                                    </fieldset>
                                                </td>
                                                <td>
                                                    <div class="custom-control custom-switch custom-control-inline mb-1">
                                                        <input type="checkbox" class="custom-control-input" {{($dataPulls['zec']['auto_mode'] == '1') ? 'checked' : ''}} id="zec-auto-mode">
                                                        <label class="custom-control-label mr-1" for="zec-auto-mode">
                                                        </label>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <button type="button" class="btn btn-primary mr-1 mb-1" id="save-table">Сохранить</button>
                            </div>
                            <div class="tab-pane" id="admin-fill" role="tabpanel" aria-labelledby="admin-tab-fill">
                                <!--Админы и доступы-->
                                <button type="button" class="btn btn-success mr-1 mb-1" data-toggle="modal" data-target="#small" onclick="$('#small .btn-primary').attr('onclick', 'creatingAdmin();');"><i class="bx bx-plus-medical"></i> Добавить нового</button>

                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Имя</th>
                                                <th>Email</th>
                                                <th>Доступ</th>
                                                <th>Действия</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($admins as $admin)
                                                @if ($admin->role == 10)
                                                    <tr>
                                                        <td>{{$admin->name}}</td>
                                                        <td>{{$admin->email}}</td>
                                                        <td>
                                                            <!--Наполнение, пользователи, выплаты, настройки-->
                                                            @php
                                                                $pages = [];
                                                                if ($admin->page_content == 1) {
                                                                    array_push($pages, 'наполнение');
                                                                }
                                                                if ($admin->page_users == 1) {
                                                                    array_push($pages, 'пользоваьели');
                                                                }
                                                                if ($admin->page_pays == 1) {
                                                                    array_push($pages, 'выплаты');
                                                                }
                                                                if ($admin->page_settings == 1) {
                                                                    array_push($pages, 'настройки');
                                                                }
                                                                echo implode(', ', $pages);
                                                            @endphp
                                                        </td>
                                                        <td>
                                                            <a href="#" class="text-warning" onclick="getAdmin({{$admin->id}})" data-toggle="modal" data-target="#small"><i class="bx bxs-pencil"></i></a>
                                                            <a href="#" class="text-danger" onclick="deleteAdmin({{$admin->id}})"><i class="bx bxs-trash-alt"></i></a>
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane" id="wallets-fill" role="tabpanel" aria-labelledby="wallets-tab-fill">
                                @foreach ($wallets as $wallet)
                                    <div class="form-group row align-items-center">
                                        <div class="col-lg-2 col-3" style="text-align: right;">
                                            <label class="col-form-label" style="font-size:20px;">{{strtoupper($wallet->coin)}}</label>
                                        </div>
                                        <div class="col-lg-10 col-9">
                                            <input type="text" id="{{$wallet->coin}}-wallet" class="form-control" name="{{$wallet->coin}}-wallet" value="{{$wallet->wallet}}">
                                        </div>
                                    </div>
                                @endforeach
                                <button type="button" class="btn btn-primary mr-1 mb-1" id="save-wallets">Сохранить</button>
                            </div>

                            <div class="tab-pane" id="referral-fill" role="tabpanel" aria-labelledby="referral-tab-fill">
                                <!--Реф. программа-->
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Уровень</th>
                                            <th>Конверсия</th>
                                            <th>Процент</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Первый</td>
                                            <td>

                                            </td>
                                            <td>
                                                <fieldset class="form-group">
                                                    <input type="text" class="form-control" id="firstLevelPercent" value="{{$referralLevels[1]['percent']}}">
                                                </fieldset>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Второй</td>
                                            <td>
                                                <fieldset class="form-group">
                                                    <input type="text" class="form-control" id="secondLevelConversion" value="{{$referralLevels[2]['conversion']}}">
                                                </fieldset>
                                            </td>
                                            <td>
                                                <fieldset class="form-group">
                                                    <input type="text" class="form-control" id="secondLevelPercent" value="{{$referralLevels[2]['percent']}}">
                                                </fieldset>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Третий</td>
                                            <td>
                                                <fieldset class="form-group">
                                                    <input type="text" class="form-control" id="thirdLevelConversion" value="{{$referralLevels[3]['conversion']}}">
                                                </fieldset>
                                            </td>
                                            <td>
                                                <fieldset class="form-group">
                                                    <input type="text" class="form-control" id="thirdLevelPercent" value="{{$referralLevels[3]['percent']}}">
                                                </fieldset>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <button type="button" class="btn btn-primary mr-1 mb-1" id="save-referral">Сохранить</button>
                                </div>
                            </div>
                            <div class="tab-pane" id="faq-fill" role="tabpanel" aria-labelledby="faq-tab-fill">
                                <div class="form-group row align-items-center">
                                    <div class="col-lg-2 col-3">
                                        <label class="col-form-label">Аккаунт в телеграме</label>
                                    </div>
                                    <div class="col-lg-10 col-9">
                                        <input type="text" id="telegram-account" class="form-control" name="telegram-account" value="{{$dataSetting['telegram']}}">
                                    </div>
                                </div>
                                <button type="button" class="btn btn-primary mr-1 mb-1" id="save-telegram-account">Сохранить</button>
                            </div>
                            <div class="tab-pane" id="logo-fill" role="tabpanel" aria-labelledby="logo-tab-fill">
                                <div class="row">
                                    <div class="col-lg-6">
                                        Текущий логотип: <br><br>
                                        <img class="logo-img" src="{{$dataSetting['logo']}}" alt="logo">
                                    </div>
                                    <div class="col-lg-6">
                                        <fieldset class="form-group">
                                            <label for="basicInputFile">Загрузить лого</label>
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="inputGroupFile01" name="inputGroupFile01">
                                                <label class="custom-file-label" for="inputGroupFile01">Выберите файл</label>
                                            </div>
                                        </fieldset>
                                        <button type="button" class="btn btn-primary mr-1 mb-1" id="save-img">Сохранить</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Dashboard Ecommerce ends -->

<div class="modal fade text-left" id="small" tabindex="-1" role="dialog" aria-labelledby="myModalLabel19"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel19">Данные пользователя</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="bx bx-x"></i>
                </button>
            </div>
            <div class="modal-body">
                <fieldset class="form-group">
                    <label for="nameInput">Имя</label>
                    <input type="text" class="form-control" id="nameInput" value="">
                </fieldset>
                <fieldset class="form-group">
                    <label for="emailInput">Email</label>
                    <input type="text" class="form-control" id="emailInput" value="">
                </fieldset>
                <fieldset class="form-group">
                    <label for="passwordInput">Пароль</label>
                    <input type="password" class="form-control" id="passwordInput" value="">
                </fieldset>
                <h5>Доступы до страниц</h5>
                <div class="custom-control custom-switch mb-1">
                    <input type="checkbox" class="custom-control-input" id="page-content">
                    <label class="custom-control-label mr-1" for="page-content">
                    </label>
                    <span>Наполнение</span>
                </div>
                <div class="custom-control custom-switch mb-1">
                    <input type="checkbox" class="custom-control-input" id="page-users">
                    <label class="custom-control-label mr-1" for="page-users">
                    </label>
                    <span>Пользователи</span>
                </div>
                <div class="custom-control custom-switch mb-1">
                    <input type="checkbox" class="custom-control-input" id="page-pays">
                    <label class="custom-control-label mr-1" for="page-pays">
                    </label>
                    <span>Выплаты</span>
                </div>
                <div class="custom-control custom-switch mb-1">
                    <input type="checkbox" class="custom-control-input" id="page-settings">
                    <label class="custom-control-label mr-1" for="page-settings">
                    </label>
                    <span>Настройки</span>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-secondary btn-sm" data-dismiss="modal">
                    <i class="bx bx-x d-block d-sm-none"></i>
                    <span class="d-sm-block d-none">Close</span>
                </button>
                <button type="button" class="btn btn-primary ml-1 btn-sm">
                    <i class="bx bx-check d-block d-sm-none"></i>
                    <span class="d-sm-block d-none">Accept</span>
                </button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('vendor-scripts')
    <script src="{{asset('vendors/js/extensions/toastr.min.js')}}"></script>
@endsection

@section('page-scripts')
    <script src="{{asset('js/scripts/navs/navs.js')}}"></script>

    <script>
        $('#save-table').on('click', function(){
            $.ajax({
                method: 'POST',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: '/admin/setting/save-table',
                data: {
                    'auto_mode' : ($('#customSwitch1').prop('checked')) ? 1 : 0,
                    'btc': {
                        'revenue' : $('#btc-revenue').val(),
                        'pull_hashrate' : $('#btc-pool-hashrate').val(),
                        'network' : $('#btc-network').val(),
                        'minimum_payment' : $('#btc-minimum-payment').val(),
                        'earning_mode' : $('#btc-earning-mode').val(),
                        'auto_mode': ($('#btc-auto-mode').prop('checked')) ? 1 : 0
                    },
                    'bch': {
                        'revenue' : $('#bch-revenue').val(),
                        'pull_hashrate' : $('#bch-pool-hashrate').val(),
                        'network' : $('#bch-network').val(),
                        'minimum_payment' : $('#bch-minimum-payment').val(),
                        'earning_mode' : $('#bch-earning-mode').val(),
                        'auto_mode': ($('#bch-auto-mode').prop('checked')) ? 1 : 0
                    },
                    'ltc': {
                        'revenue' : $('#ltc-revenue').val(),
                        'pull_hashrate' : $('#ltc-pool-hashrate').val(),
                        'network' : $('#ltc-network').val(),
                        'minimum_payment' : $('#ltc-minimum-payment').val(),
                        'earning_mode' : $('#ltc-earning-mode').val(),
                        'auto_mode': ($('#ltc-auto-mode').prop('checked')) ? 1 : 0
                    },
                    'eth': {
                        'revenue' : $('#eth-revenue').val(),
                        'pull_hashrate' : $('#eth-pool-hashrate').val(),
                        'network' : $('#eth-network').val(),
                        'minimum_payment' : $('#eth-minimum-payment').val(),
                        'earning_mode' : $('#eth-earning-mode').val(),
                        'auto_mode': ($('#eth-auto-mode').prop('checked')) ? 1 : 0
                    },
                    'zec': {
                        'revenue' : $('#zec-revenue').val(),
                        'pull_hashrate' : $('#zec-pool-hashrate').val(),
                        'network' : $('#zec-network').val(),
                        'minimum_payment' : $('#zec-minimum-payment').val(),
                        'earning_mode' : $('#zec-earning-mode').val(),
                        'auto_mode': ($('#zec-auto-mode').prop('checked')) ? 1 : 0
                    }
                },
                success: function(data) {
                    if (data == '200') {
                        toastr.success('Таблица', 'Ваши данные успешно сохранены!');
                    }
                }
            });
        });

        $('#save-wallets').on('click', function(){
            $.ajax({
                method: 'POST',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: '/admin/setting/save-wallets',
                data: {
                    'btc': $('#btc-wallet').val(),
                    'bch': $('#bch-wallet').val(),
                    'ltc': $('#ltc-wallet').val(),
                    'eth': $('#eth-wallet').val(),
                    'zec': $('#zec-wallet').val()
                },
                success: function(data) {
                    if (data == '200') {
                        toastr.success('Кошельки', 'Ваши данные успешно сохранены!');
                    }
                }
            })
        });

        $('#save-referral').on('click', function(){
            $.ajax({
                method: 'POST',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: '/admin/setting/save-referral',
                data: {
                    'percent1': $('#firstLevelPercent').val(),
                    'percent2': $('#secondLevelPercent').val(),
                    'percent3': $('#thirdLevelPercent').val(),
                    'conversion2' : $('#secondLevelConversion').val(),
                    'conversion3' : $('#thirdLevelConversion').val()
                },
                success: function(data) {
                    if (data == '200') {
                        toastr.success('Реферальная программа', 'Ваши данные успешно сохранены!');
                    }
                }
            });
        });

        $('#save-telegram-account').on('click', function(){
            $.ajax({
                method: 'POST',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: '/admin/setting/save-telegram',
                data: {
                    'telegram': $('#telegram-account').val()
                },
                success: function(data) {
                    if (data == '200') {
                        toastr.success('Обратная связь', 'Ваши данные успешно сохранены!');
                    }
                }
            });
        });

        $('#save-img').on('click', function(){
            $.ajax({
                method: 'POST',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: '/admin/setting/save-image',
                data: $('#inputGroupFile01').files,
                processData: false,
                contentType : false,
                success: function(data) {
                    $('.logo-img').attr('src', data);
                    toastr.success('Логотип', 'Ваши данные успешно сохранены!');
                }
            })
        });

        function creatingAdmin()
        {
            $.ajax({
                method: 'POST',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: '/admin/setting/add-admin',
                data: {
                    'name' : $('#nameInput').val(),
                    'email' : $('#emailInput').val(),
                    'password': $('#passwordInput').val(),
                    'page_content': ($('#page-content').prop('checked')) ? 1 : 0,
                    'page_users': ($('#page-users').prop('checked')) ? 1 : 0,
                    'page_pays': ($('#page-pays').prop('checked')) ? 1 : 0,
                    'page_settings': ($('#page-settings').prop('checked')) ? 1 : 0,
                },
                success: function(data) {
                    if (data = '200') {
                        $('#small').modal('hide');
                        toastr.success('Администраторы и права', 'Админ успешно добавлен!');
                    }
                }
            })
        }

        function getAdmin(id)
        {
            $.ajax({
                method: 'POST',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: '/admin/setting/get-admin',
                dataType: 'json',
                data: {'id' : id},
                success: function(data) {
                    $('#nameInput').val(data.name);
                    $('#emailInput').val(data.email);
                    $('#page-content').attr('checked', (data.page_content == '1') ? true : false);
                    $('#page-users').attr('checked', (data.page_users == '1') ? true : false);
                    $('#page-pays').attr('checked', (data.page_pays == '1') ? true : false);
                    $('#page-settings').attr('checked', (data.page_settings == '1') ? true : false);
                    $('#small .btn-primary').attr('onclick', 'saveDataAdmin(' + id + ');');
                }
            })
        }

        function saveDataAdmin(id)
        {
            $.ajax({
                method: 'POST',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: '/admin/setting/edit-admin',
                data: {
                    'id' : id,
                    'name' : $('#nameInput').val(),
                    'email' : $('#emailInput').val(),
                    'password': $('#passwordInput').val(),
                    'page_content': ($('#page-content').prop('checked')) ? 1 : 0,
                    'page_users': ($('#page-users').prop('checked')) ? 1 : 0,
                    'page_pays': ($('#page-pays').prop('checked')) ? 1 : 0,
                    'page_settings': ($('#page-settings').prop('checked')) ? 1 : 0,
                },
                success: function(data) {
                    if (data = '200') {
                        $('#small').modal('hide');
                        toastr.success('Администраторы и права', 'Данные администратора успешно сохранены!');
                    }
                }
            })
        }

        function deleteAdmin(id)
        {
            var isDelete = confirm("Вы уверены, что хотите удалить этого администратора?");

            if (isDelete) {
                $.ajax({
                    method: 'POST',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: '/admin/setting/delete-admin',
                    data: {'id': id},
                    success: function(data) {
                        if (data = '200') {
                            toastr.success('Администраторы и права', 'Администратор удалён!');
                        }
                    }
                });
            }
        }
    </script>
@endsection
