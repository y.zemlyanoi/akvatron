@extends('admin.layouts.baseLayout')
{{-- page Title --}}
@section('title','Управление наполнением сайта')
{{-- vendor style --}}
<!--
@section('vendor-styles')
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/editors/quill/katex.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/editors/quill/monokai-sublime.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/editors/quill/quill.snow.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/editors/quill/quill.bubble.css')}}">
@endsection
    -->

@section('content')
    <section>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Управление наполнением сайта</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <h6>Выберете редактируемую часть лэндинга</h6>
                        <div class="row">
                            <div class="col-md-10">
                                <fieldset class="form-group">
                                    <select class="form-control" id="landing-part-select">
                                        @foreach ($partsLanding as $key => $value)
                                            <option value="{{$key}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                </fieldset>
                            </div>
                            <button type="button" class="btn btn-primary col-md-2 form-group" id="select-section">Редактировать</button>
                        </div>

                        <button type="button" class="btn btn-primary mr-1 mb-1" id="save-section">Сохранить содержимое части лэндинга</button>
                        <input type="hidden" value="header" id="name-template"></input>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

{{-- vendor scripts --}}
@section('vendor-scripts')
    <script src="{{asset('vendors/js/editors/quill/katex.min.js')}}"></script>
    <script src="{{asset('vendors/js/editors/quill/highlight.min.js')}}"></script>
    <script src="{{asset('vendors/js/editors/quill/quill.min.js')}}"></script>
    <script src="{{asset('vendors/js/extensions/jquery.steps.min.js')}}"></script>
    <script src="{{asset('vendors/js/forms/validation/jquery.validate.min.js')}}"></script>
@endsection

{{-- page scripts --}}
@section('page-scripts')
    <script>
        var editFrame = $('#edit-frame');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#save-section').on('click', function(){
            //alert(editFrame.contents().find(".nk-wrap").html());
            $.ajax({
                method: 'POST',
                //url: '/save-part-template/' + $('#name-template').val() + '.blade/' + editFrame.contents().find(".nk-wrap").html(),
                url : '/save-part-template',
                data: {
                    template: $('#name-template').val(),
                    content: editFrame.contents().find(".nk-wrap").html()
                },
                success: function(data) {
                    alert(data);
                }
            })
        });

        $('#select-section').on('click', function () {
            var nameTemplate = $('#landing-part-select').val();
            $.ajax({
                method: 'POST',
                url: '/get-part-template/' + nameTemplate,
                success: function (data) {
                    editFrame.attr('src', data);
                    $('#name-template').val(nameTemplate);
                }
            })
        });
    </script>
@endsection
