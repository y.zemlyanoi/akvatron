(function(window, document, $) {
    'use strict';

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#users-list").on('change', function (e) {
        let url = $("body").data('base');
        let params = {
            'start': $('#calendar_users').data('daterangepicker').startDate.format(),
            'end': $('#calendar_users').data('daterangepicker').endDate.format()
        };
        if($(this).val() != 'all') {
            params.type = $(this).val();
        }
        $(".spinner-table").css('opacity',1);
        $("#users tbody").css('opacity',.4)
        $.ajax({
            type:'POST',
            url:url+'/api/users',
            data:params,
            datatype: "html"
        }).done(function(data){
            $("#users-pagination").remove();
            $("#users div.table-responsive").after(data.htmlPagination);
            $("#users-list-datatable tbody").html(data.htmlBody);
            $(".spinner-table").css('opacity', 0);
            $("#users tbody").css('opacity', 1);
            init();
        }).fail(function(jqXHR, ajaxOptions, thrownError){
            alert('No response from server');
        });
    });

    $("#calendar_users").on('apply.daterangepicker', function(ev, picker) {
        let params = {
            'id': 'users',
            'start': picker.startDate.format(),
            'end': picker.endDate.format()
        }
        showUsers(params);
    });

    function showUsers(params) {
        let id = params.id;
        if(id !== undefined) {
            let url = $("body").data('base');
            params.type = $("#"+id+"-list-type option:checked").val();
            $(".spinner-table").css('opacity',1);
            $("#"+id+" tbody").css('opacity',.4)
            $.ajax({
                type:'POST',
                url:url+'/api/users',
                data:params,
                datatype: "html"
            }).done(function(data){
                $("#"+id+"-pagination").remove();
                $("#"+id+" div.table-responsive").after(data.htmlPagination);
                $("#"+id+"-list-datatable tbody").html(data.htmlBody);
                $(".spinner-table").css('opacity', 0);
                $("#"+id+" tbody").css('opacity', 1);
                init();
            }).fail(function(jqXHR, ajaxOptions, thrownError){
                alert('No response from server');
            });

        }
    }

    function init() {
        /**
         * Pagination api request
         */
        $('ul.pagination li.page-item a.page-link').on('click', function (e) {
            e.preventDefault();
            let a = $(this);
            let url = $("body").data('base');
            let type = $("#users-list option:checked").val();
            let page = parseInt(a.data('page'));
            let params = {
                'type':type,
                'page': (page != undefined) ? page : 1,
                'start': $("#calendar_users").data('daterangepicker').startDate.format(),
                'end': $("#calendar_users").data('daterangepicker').endDate.format()
            };
            $(".spinner-table").css('opacity',1);
            $("#users tbody").css('opacity',.4)
            $.ajax({
                type:'POST',
                url:url+'/api/users',
                data:params,
                datatype: "html"
            }).done(function(data){
                $("#users-pagination").remove();
                $("#users div.table-responsive").after(data.htmlPagination);
                $("#users-list-datatable tbody").html(data.htmlBody);
                $(".spinner-table").css('opacity', 0);
                $("#users tbody").css('opacity', 1);
                init();
            }).fail(function(jqXHR, ajaxOptions, thrownError){
                alert('No response from server');
            });
        });
    }
    init()

    function getUserData(id) {
        $.ajax({
            method : 'get',
            dataType: 'json',
            url : '/admin/users/get-user-data/' + id,
            success: function(data) {
                $("#myModalLabel17").html(data.name);
                $("#large .modal-body").html(data.view);
            }
        });
    }
    function editUserData(id) {
        $.ajax({
            method : 'get',
            dataType: 'json',
            url : '/admin/users/edit-user-data/' + id,
            success: function(data) {
                $('#nameInput').val(data.name);
                $('#emailInput').val(data.email);

                $('#btcInput').val(data.btc);
                $('#bchInput').val(data.bch);
                $('#ltcInput').val(data.ltc);
                $('#ethInput').val(data.eth);
                $('#zecInput').val(data.zec);

                $('#small .btn-primary').attr('onclick', 'saveUserData(' + id + ')');
            }
        });
    }
    function saveUserData(id) {
        var pass = '-';
        if ( $('#field-password').css('display') == 'none') {
            alert('none');
        }

        $.ajax({
            method: 'get',
            dataType: 'json',
            url: '/admin/users/save-user-data/' + id,
            data: {
                'name' : $('#nameInput').val(),
                'email' : $('#emailInput').val(),
                'btc' : $('#btcInput').val(),
                'bch' : $('#bchInput').val(),
                'ltc' : $('#ltcInput').val(),
                'eth' : $('#ethInput').val(),
                'zec' : $('#zecInput').val(),
                'password': pass,
            },
            success: function(data) {
                if (data == '200') {
                    alert('Данные пользователя успешно изменены');
                    $('#small').modal('hide');
                }
            }
        })
    }
})(window, document, jQuery);
