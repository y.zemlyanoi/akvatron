(function(window, document, $) {
    'use strict';

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    /**
     * Select type of payouts
     */
    $("select.payouts-list-type").on('change', function (e) {
        let id = $(this).closest('div.tab-pane').attr('id');
        if(id !== undefined) {
            let url = $("body").data('base');
            let type = $(this).val();
            let data = {'type':type};
            $(".spinner-table").css('opacity',1);
            $("#"+id+" tbody").css('opacity',.4)
            $.ajax({
                type:'POST',
                url:url+'/api/payouts/'+id,
                data:data,
                datatype: "html"
            }).done(function(data){
                $("#"+id+"-pagination").remove();
                $("#"+id+" div.table-responsive").after(data.htmlPagination);
                $("#"+id+"-list-datatable tbody").html(data.htmlBody);
                $(".spinner-table").css('opacity', 0);
                $("#"+id+" tbody").css('opacity', 1);
                init();
            }).fail(function(jqXHR, ajaxOptions, thrownError){
                alert('No response from server');
            });
        }
    });


    function init() {

        /**
         * Datapicker apply with request to api
         */
        $("#unconfirmed_calendar_payouts").on('apply.daterangepicker', function(ev, picker) {
            let data = {
                'id': 'unconfirmed',
                'start': picker.startDate.format(),
                'end': picker.endDate.format()
            }
            showPayouts(data);
        });

        $("#completed_calendar_payouts").on('apply.daterangepicker', function(ev, picker) {
            let data = {
                'id': 'completed',
                'start': picker.startDate.format(),
                'end': picker.endDate.format()
            }
            showPayouts(data);
        });

        $("#canceled_calendar_payouts").on('apply.daterangepicker', function(ev, picker) {
            let data = {
                'id': 'canceled',
                'start': picker.startDate.format(),
                'end': picker.endDate.format()
            }
            showPayouts(data);
        });

        function showPayouts(data) {
            let id = data.id;
            if(id !== undefined) {
                let url = $("body").data('base');
                data.type = $("#"+id+" .payouts-list-type option:checked").val();
                $(".spinner-table").css('opacity',1);
                $("#"+id+" tbody").css('opacity',.4)
                $.ajax({
                    type:'POST',
                    url:url+'/api/payouts/'+id,
                    data:data,
                    datatype: "html"
                }).done(function(data){
                    $("#"+id+"-pagination").remove();
                    $("#"+id+" div.table-responsive").after(data.htmlPagination);
                    $("#"+id+"-list-datatable tbody").html(data.htmlBody);
                    $(".spinner-table").css('opacity', 0);
                    $("#"+id+" tbody").css('opacity', 1);
                    init();
                }).fail(function(jqXHR, ajaxOptions, thrownError){
                    alert('No response from server');
                });
            }
        }

        /**
         * Pagination api request
         */
        $('ul.pagination li.page-item a.page-link').on('click', function (e) {
            e.preventDefault();
            let a = $(this);
            let id = $(this).closest('div.tab-pane').attr('id');
            if(id !== undefined) {
                let url = $("body").data('base');
                let type = $("#"+id+" .payouts-list-type option:checked").val();
                let page = parseInt(a.data('page'));
                let data = {
                    'type':type,
                    'page': (page != undefined) ? page : 1,
                };
                $.ajax({
                    type:'POST',
                    url:url+'/api/payouts/'+id,
                    data:data,
                    datatype: "html"
                }).done(function(data){
                    $("#"+id+"-pagination").remove();
                    $("#"+id+" div.table-responsive").after(data.htmlPagination);
                    $("#"+id+"-list-datatable tbody").html(data.htmlBody);
                    $(".spinner-table").css('opacity', 0);
                    $("#"+id+" tbody").css('opacity', 1);
                    init();
                }).fail(function(jqXHR, ajaxOptions, thrownError){
                    alert('No response from server');
                });
            }
        });

        $('a.confirm-pay').on('click', function (e){
            let id = $(this).data('pay');
            var name = $('.pay-' + id + ' td:nth-child(1)').html();
            var sum = $('.pay-' + id + ' td:nth-child(2)').html();
            var wallet = $('.pay-' + id + ' td:nth-child(3)').html();
            $('#defaultSize .btn-confirm-pay').data('pay', id);
            $('#defaultSize .modal-body').html('Подтвердите вывод ' + sum + ' для ' + name + ' на кошелёк ' + wallet);
        });

        $('a.cancel-pay').on('click', function (e){
            let id = $(this).data('pay');
            $('#small .btn-cancel-pay').data('pay', id);
        });

        $('button.btn-confirm-pay').on('click', function (e){
            let id = $(this).data('pay');
            let url = $("body").data('base');
            $.ajax({
                method : 'POST',
                url: url+'/api/payouts/action/confirm',
                data: {
                    id: id
                },
                success: function (data) {
                    if (data.success == true) {
                        toastr.success('Заказ успешно подтвержден!');
                        $('#defaultSize').modal('hide');
                        $('.pay-'+id).remove();
                    }
                }
            });
        });

        $('button.btn-cancel-pay').on('click', function (e){
            let id = $(this).data('pay');
            let url = $("body").data('base');
            $.ajax({
                method : 'POST',
                url: url+'/api/payouts/action/cancel',
                data: {
                    id: id,
                    reason: $('#basicTextarea').val(),
                },
                success: function (data) {
                    if (data.success == true) {
                        toastr.warning('Заказ успешно отменён!');
                        $('#small').modal('hide');
                        $('.pay-'+id).remove();
                    }
                }
            });
        });
    }
    init();
})(window, document, jQuery);
