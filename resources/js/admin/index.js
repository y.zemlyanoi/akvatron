(function(window, document, $) {
    'use strict';

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    let url = $("body").data('base');
    $.ajax({
        type:'POST',
        url:url+'/api/payouts/count',
        data:{'type':'unconfirmed'},
        datatype: "html"
    }).done(function(data){
        let tags = document.getElementsByClassName('tag-payouts')
        for (let tag of tags) {
            $(tag).html(data.amount)
        }

    }).fail(function(jqXHR, ajaxOptions, thrownError){
        alert('No response from server');
    });

})(window, document, jQuery);
